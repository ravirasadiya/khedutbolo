<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
        // Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::namespace('Products')->group(function () {
                Route::resource('products', 'ProductController');
                Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
                Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
                Route::post('getCategories', 'ProductController@getCategories')->name('products.getCategories');
                Route::post('getCropCategories', 'ProductController@getCropCategories')->name('products.getCropCategories');
                Route::post('getSubCropCategories', 'ProductController@getSubCropCategories')->name('products.getSubCropCategories');
                  Route::get('checkProductname', 'ProductController@checkProductname')->name('products.checkProductname');
                  Route::get('checkEditProductname', 'ProductController@checkEditProductname')->name('products.checkEditProductname');
                Route::post('getSubCategories', 'ProductController@getSubCategories')->name('products.getSubCategories');
                Route::get('checkhsncode', 'ProductController@checkHsnCode')->name('products.checkhsncode');
                Route::get('get_additional', 'ProductController@get_additional')->name('products.get_additional');
                Route::post('getTechnicalName', 'ProductController@getTechnicalName')->name('products.getTechnicalName');
                Route::post('getProductTechnical', 'ProductController@getProductTechnical')->name('products.getProductTechnical');
                Route::post('remove-product-unit', 'ProductController@removeUnit')->name('product.remove.unit');

            });
            Route::namespace('Customers')->group(function () {
                Route::resource('customers', 'CustomerController');
                Route::resource('customers.addresses', 'CustomerAddressController');
            });
            // Route::namespace('Crop')->group(function () {
            //     Route::resource('crops', 'CropController');
            // });
            Route::namespace('Crops')->group(function () {
                Route::resource('crops', 'CropController');
                Route::get('crops/create/{id?}', 'CropController@create')->name('crops.create');
                Route::get('crops/subcrop/{id}/{subcat?}', 'CropController@showSub')->name('crops.show_sub');
                Route::get('crops/subcrop/create/{id?}/{subcat?}', 'CropController@subCreate')->name('crops.sub_create');
                Route::get('crops/getsubcat/{id}', 'CropController@getSubcat')->name('crops.getsubcat');
                Route::post('crops/substore', 'CropController@subStore')->name('crops.subcatstore');
                Route::get('crops/subcrop/edit/{id}/{cat}/{subcat}', 'CropController@subEdit')->name('crops.sub_edit');
                Route::get('checkSubsolutionname', 'CropController@checkSubsolutionname')->name('crops.checkSubsolutionname');
              Route::get('checkEditSubsolutionname', 'CropController@checkEditSubsolutionname')->name('crops.checkEditSubsolutionname');
                
            });
            
            Route::namespace('CustomerNotifications')->group(function () {
                Route::post('getValue', 'NotificationController@getValue')->name('notifications.getValue');
                Route::resource('notifications', 'NotificationController');
            });
            
            Route::namespace('TechnicalCategoties')->group(function () {
                Route::resource('technical', 'TechnicalController');
            });

            Route::namespace('UpdateStocks')->group(function () {
                Route::resource('updateStock', 'UpdateStockController');
            });
            
            
            Route::namespace('CountriesData')->group(function () {
                Route::resource('country_data', 'CountryDataController');
                Route::get('country_data/show-tehsil/{id?}', 'CountryDataController@showTehsil')->name('country_data.showTehsil');
                Route::get('country_data/show-village/{id?}', 'CountryDataController@showVillage')->name('country_data.showVillage');
                //Route::get('country_data/destroy-village/{id?}', 'CountryDataController@destroyVillage')->name('country_data.destroyVillage');
                Route::get('checkDistrictname', 'CountryDataController@checkDistrictname')->name('country_data.checkDistrictname');
                Route::get('checkEditDistrictname', 'CountryDataController@checkEditDistrictname')->name('country_data.checkEditDistrictname');
                Route::get('checkTehsilname', 'CountryDataController@checkTehsilname')->name('country_data.checkTehsilname');
                Route::get('checkEditTehsilname', 'CountryDataController@checkEditTehsilname')->name('country_data.checkEditTehsilname');
                Route::get('checkVillagename', 'CountryDataController@checkVillagename')->name('country_data.checkVillagename');
                Route::get('checkEditVillagename', 'CountryDataController@checkEditVillagename')->name('country_data.checkEditVillagename');
                Route::post('destroy/{id}', 'CountryDataController@destroy')->name('country_data.destroy');
                Route::post('destroydistrict/{id}', 'CountryDataController@destroydistrict')->name('country_data.destroydistrict');
                Route::post('destroytehsil/{id}', 'CountryDataController@destroytehsil')->name('country_data.destroytehsil');
                Route::post('country_data/destroyvillage/{id}', 'CountryDataController@destroyvillage')->name('country_data.destroyvillage');
                Route::post('getCity', 'CountryDataController@getCity')->name('country_data.getCity');
                Route::post('getTehsil', 'CountryDataController@getTehsil')->name('country_data.getTehsil');
                Route::get('createVillage','CountryDataController@createVillage')->name('country_data.createVillage');
                Route::get('createCity','CountryDataController@createCity')->name('country_data.createCity');
                Route::post('state-store','CountryDataController@stateStore')->name('country_data.state_store');
                
                // Disritct
                Route::get('disritct-edit/{id}','CountryDataController@disritctEdit')->name('country_data.disritct_edit');
                Route::post('disritct-update','CountryDataController@disritctUpdate')->name('country_data.disritct_update');
                
                // Edit Tehsil
                Route::get('tehsil-edit/{id}','CountryDataController@tehsilEdit')->name('country_data.tehsil_edit');
                Route::post('tehsil-store','CountryDataController@tehsilStore')->name('country_data.update_tehsil');
                
                // Village
                Route::get('village-edit/{id}','CountryDataController@villageEdit')->name('country_data.village_edit');
                Route::post('village-update','CountryDataController@villageUpdate')->name('country_data.village_update');
                
                
            });

            Route::namespace('Priorities')->group(function () {
                Route::resource('pro_priority', 'PriorityController');
                // Route::post('update', 'PriorityController@update');
                Route::post('pro_priority/sort', 'PriorityController@cat_sort')->name('priority.sort');
                Route::post('pro_priority/subcat_sort', 'PriorityController@subcat_sort')->name('priority.subcat_sort');
                Route::post('pro_priority/getProduct', 'PriorityController@getProduct')->name('priority.getProduct');
                Route::post('pro_priority/getCategoryProduct', 'PriorityController@getCategoryProduct')->name('priority.getCategoryProduct');
                Route::post('pro_priority/getAllProduct', 'PriorityController@getAllProduct')->name('priority.getAllProduct');
            });
            Route::namespace('Users')->group(function () {
                Route::get('users', 'UsersController@index')->name('users.index');
                Route::get('users/create', 'UsersController@create')->name('users.create');
                Route::get('users/edit/{id}', 'UsersController@edit')->name('users.edit');
                Route::post('users/store', 'UsersController@store')->name('users.store');
                Route::post('users/destroy/{id}', 'UsersController@destroy')->name('users.destroy');
            });
            Route::namespace('Offers')->group(function () {
                Route::resource('offers', 'OffersController');
                Route::get('offers_product','OffersController@getProduct')->name('offers.product.attr');
                Route::post('offers/{id}', 'OffersController@update')->name('offers.update');
                Route::get('remove-image-offer', 'OffersController@removeImage')->name('offers.remove.image');
            });
            Route::namespace('Categories')->group(function () {
                Route::resource('categories', 'CategoryController');
                Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            });
            Route::namespace('Orders')->group(function () {
                Route::get('orders/printorder','OrderController@printInvoice')->name('orders.print_invoice');
                Route::resource('orders', 'OrderController');
                Route::resource('order-statuses', 'OrderStatusController');
                Route::get('orders/{id}/invoice', 'OrderController@generateInvoice')->name('orders.invoice.generate');
                Route::post('orders/print_order', 'OrderController@orderPrint')->name('orders.print_order');
            });
            Route::resource('addresses', 'Addresses\AddressController');
            Route::resource('countries', 'Countries\CountryController');
            Route::resource('countries.provinces', 'Provinces\ProvinceController');
            Route::resource('countries.provinces.cities', 'Cities\CityController');
            Route::resource('couriers', 'Couriers\CourierController');
            Route::resource('attributes', 'Attributes\AttributeController');
            Route::resource('attributes.values', 'Attributes\AttributeValueController');
            Route::resource('brands', 'Brands\BrandController');
            
            Route::get('list-customer', 'Wallet\WalletController@index')->name('wallet.index');
            Route::get('wallet-show/{id}', 'Wallet\WalletController@walletSummery')->name('wallet.show');

        // });
        //Changed the employees to companies
        // Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
            Route::resource('companies', 'CompaniesController');
            Route::get('companies/{id}/profile', 'CompaniesController@getProfile')->name('companies.profile');
            Route::get('remove-logo', 'CompaniesController@removeLogo')->name('company.remove.logo');
            
             Route::get('checksearch','CompaniesController@checkSearch')->name('companies.checksearch');
            Route::get('changeStatus', 'CompaniesController@changeStatus')->name('companies.status');
            Route::get('checkCompanyname', 'CompaniesController@checkCompanyname')->name('companies.checkCompanyname');
            Route::get('checkEditCompanyname', 'CompaniesController@checkEditCompanyname')->name('companies.checkEditCompanyname');
            Route::put('companies/{id}/profile', 'CompaniesController@updateProfile')->name('companies.profile.update');
           
            Route::resource('roles', 'Roles\RoleController');
            Route::resource('permissions', 'Permissions\PermissionController');
        // });
    });
    Route::get('translate','Admin\TranslateController@translate')->name('translate');
});

/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');
});

Route::namespace('Front')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['middleware' => ['auth', 'web']], function () {
        
        //contact
        Route::post('contact', 'HomeController@contactFormSubmit')->name('contact.contactFormSubmit');

        Route::namespace('Payments')->group(function () {
            Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
        });

        Route::namespace('Addresses')->group(function () {
            Route::resource('country.state', 'CountryStateController');
            Route::resource('state.city', 'StateCityController');
        });

        Route::get('accounts', 'AccountsController@index')->name('accounts');
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
    });
    Route::resource('cart', 'CartController');
    Route::get("category/{slug}", 'CategoryController@getCategory')->name('front.category.slug');
    Route::get("search", 'ProductController@search')->name('search.product');
    Route::get("{product}", 'ProductController@show')->name('front.get.product');
});