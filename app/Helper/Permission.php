<?php

namespace App\Helper;

use Illuminate\Http\Request;
use Auth;
use App\Shop\Modules\Module;
use App\Shop\ModulePermissions\ModulePermission;
use App\Shop\Roles\Role;
use App\Shop\RoleUsers\RoleUser;

class Permission 
{
	public static function permission($module="") 
	{
	    try {
			$admin = Auth::guard('employee')->user();
        	$permission = RoleUser::join('module_permission', 'module_permission.user_id', 'role_user.role_id')
                                ->join('module', 'module.id', 'module_permission.module_id')
                                ->Select('module_permission.view','module_permission.add','module_permission.edit','module_permission.delete')
                                ->where('module.name', $module)  
                                ->where('role_user.user_id', $admin->id)
                                ->first();
			return $permission;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}

}