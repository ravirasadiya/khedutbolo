<?php

namespace App\Helper;

use Illuminate\Http\Request;
use App\Shop\Notifications\NotificationResponse;
use App\Http\Controllers\Controller;
use App\Shop\Customers\Customer;
use App\User;
use Validator;
use Hash;

class NotificationHelper extends Controller
{
	public static function notification($message, $server_api_key, $token, $title) 
	{
	    
	    $msg = array
	           (
					'body' 	=> $message,
					'title'	=> $title,
					'click_action' => ".Notification_Activity",
		         	'icon'	=> 'myicon',/*Default Icon*/
		          	'sound' => 'mySound'/*Default sound*/
	           );
		$fields = array
				(
					'to'		=> $token,
					'notification'	=> $msg,
				);
		$headers = array
				(
					'Authorization: key='.$server_api_key,
					'Content-Type: application/json'
				);

		#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		if($token){
		    $customer = Customer::where('token',$token)->first();
    		$notification_response = new NotificationResponse();
    	    $notification_response->customer_id = $customer->id;
    	    $notification_response->token = $token;
    	    $notification_response->response = $result;
    	    $notification_response->save();
    		return $result;    
		}
		
	}
}