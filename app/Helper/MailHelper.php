<?php

namespace App\Helper;

use Illuminate\Http\Request;
use Mail;

class MailHelper 
{
	public static function productNotification($data="") 
	{
	    // dd($product);
	    try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.company.productNotification',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Khedut Bolo product details');
			        $message->from('noreplay@khedutbolo.com','Khedut bolo');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}

	public static function invoiceNotification($data="") 
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.customer.invoice',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Khedut Bolo order invoice');
			        $message->from('noreplay@khedutbolo.com','Khedut bolo');
			        $message->attach(public_path('invoices/'.$data['order']->id.'.pdf'));
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	
	public static function customerOrder($data="") 
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.costomerOrderDetails',['data'=>$data] , function($message) use ($data) {
			          $message->to($data['email'])->subject('Khedut Bolo Order');
			        $message->from('noreplay@khedutbolo.com','Khedut bolo');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
	
	public static function customerOrderToAdmin($data="") 
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.admin.costomerOrderDetails',['data'=>$data] , function($message) use ($data) {
			     //   $message->to('ashishpatel104122@gmail.com')->subject('Khedut Bolo Order');
			        $message->to(env('MAIL_ADMIN'))->subject('Khedut Bolo Order');
			        $message->cc($data['email'], $data['customer']['name'])->subject('Khedut Bolo order');
			        $message->from('noreplay@khedutbolo.com','Khedut Bolo');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
    
    public static function errorMail($data="") 
	{
	    try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.company.error',['data'=>$data] , function($message) use ($data) {
			        $message->to('noreplay@khedutbolo.com', 'Error')->subject('Khedut Bolo error message');
			        $message->from('noreplay@khedutbolo.com','Khedut bolo');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
}