<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use Log;
use Redirect,Response;
use Mail;

class HomeController
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('front.index');
    }
    
    public static function contactFormSubmit(Request $request){
        try {
            $data = array(
            'fullname' => $request['firstName'].''.$request['lastName'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'message' => $request['message'],
            );
            if($data == '') {
                return "false";
            }else{
                $data['mailsend'] = Mail::send('layouts.email.email',['data'=>$data] , function($message) {
     
                    $message->to('shineinfosoft24@gmail.com', 'Khedutbolo')
                        ->from('noreplay@khedutbolo.com','KhedutBolo')
                        ->subject('KhedutBolo Contact Detail');
                });
             
                $data['mailsend'] = Mail::send('layouts.email.emailuserthanks',['data'=>$data], function($msg) use ($data){
                        $msg->to($data['email'],$data['fullname'])
                        ->from('noreplay@khedutbolo.com','Khedutbolo')
                        ->subject('Khedutbolo');
                    });
                $request->session()->flash('msg', 'Thank you for contact and we will get back to you within 24 hours.');
                return redirect('/');
                exit;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
    }
    
}
