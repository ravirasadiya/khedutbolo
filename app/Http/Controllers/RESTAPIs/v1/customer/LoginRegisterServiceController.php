<?php

namespace App\Http\Controllers\RESTAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Customers\Customer;
use Log;
use Validator;
use App\Helper\MailHelper;
use App\Helper\ResponseMessage;
use App\Helper\SMSHelper;
use App\Shop\CountriesData\CountryData;

class LoginRegisterServiceController extends Controller
{
    public function register(Request $request) {
    	try {
    		$rules = [
    			// image, name, mobileno, address, language
                'name' => 'required|string|max:255',
                'mobile' => 'required|numeric|digits_between:8,15|unique:customers',
                'sid' => 'required',
                'cid' => 'required',
                'tid' => 'required',
                'vid' => 'required',
                'language' =>'required',
                'image' => 'image'
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
                'mobile.required' => 'Please enter contact number',
                'mobile.numeric' => 'Please enter number',
                'mobile.unique' => 'Mobile number already exist.',
                'mobile.digits_between' => 'Please enter only digits and maximum 15 digits.',
                'mobile.unique' => 'Contact number is already exists.',
                'address' => 'Please enter address.',
                'language' => 'Please enter language',
                'image.image' => 'Profile image type invalid.',
                'sid.required' => 'Please enter state id',
                'cid.required' => 'Please enter city id',
                'tid.required' => 'Please enter tehsil id',
                'vid.required' => 'Please enter village id'
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else{
	        	$name = trim(strip_tags($request->name));
                $mobile = trim(strip_tags($request->mobile));
                $language = trim(strip_tags($request->language));
                $sid = trim(strip_tags($request->sid));
                $cid = trim(strip_tags($request->cid));
                $tid = trim(strip_tags($request->tid));
                $vid = trim(strip_tags($request->vid));
                $token = trim(strip_tags($request->token));

                $customer = new Customer();
                $customer->name = $name;
                $customer->mobile = $mobile;
                $customer->language = $language;
                $customer->token = $token;
                $customer->state_id = $sid;
                $customer->city_id = $cid;
                $customer->tehsil_id = $tid;
                $customer->village_id = $vid;
                if($language=="gu"){
                    $state = CountryData::select('DTName_gu as DTName')->where('STCode', 'like', '%' .$sid)->where('DTCode',000)->first();  
                    $city = CountryData::select('DTName_gu as DTName')->where('DTCode', 'like', '%' .$cid)->first();
                    $tehsil = CountryData::select('SDTName_gu as SDTName')->where('SDTCode','like', '%' .$tid)->first();
                    $village = CountryData::select('Name_gu as Name')->where('TVCode','like', '%' .$vid)->first();
                }elseif($language=="hi"){
                    $state = CountryData::select('DTName_hi as DTName')->where('STCode', 'like', '%' .$sid)->where('DTCode',000)->first();
                    $city = CountryData::select('DTName_hi as DTName')->where('DTCode', 'like', '%' .$cid)->first();
                    $tehsil = CountryData::select('SDTName_hi as SDTName')->where('SDTCode','like', '%' .$tid)->first();
                    $village = CountryData::select('Name_hi as Name')->where('TVCode','like', '%' .$vid)->first();
                }else{
                    $state = CountryData::select('DTName')->where('STCode', 'like', '%' .$sid)->where('DTCode',000)->first();    
                    $city = CountryData::select('DTName')->where('DTCode', 'like', '%' .$cid)->first();
                    $tehsil = CountryData::select('SDTName')->where('SDTCode','like', '%' .$tid)->first();
                    $village = CountryData::select('Name')->where('TVCode','like', '%' .$vid)->first();
                }

                if($customer->save()) {
                	if ($request->has('image')) {
                        $file    = $request->image;
                        $time    = md5(time());
                        $profile = $file->getClientOriginalExtension();
                        $name = $file->getClientOriginalName();
                        $path    = "images/profile/" . $customer->id . "/file";
                        $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                        $profile_update          = Customer::find($customer->id);
                        $url = env('APP_URL');
                        $profile_update->image = $url."/". $path."/" .$time.'.'.$profile;
                        $profile_update->save();
                    }
                    $user = Customer::where('id',$customer->id)->select('id','name','mobile','state_id','city_id','tehsil_id','village_id','language','image')->get()->first();
                    $user->state = $state->DTName;
                    $user->city = $city->DTName;
                    $user->tehsil = $tehsil->SDTName;
                    $user->village =$village->Name;
                	ResponseMessage::success("Register Successfully", $user);
                } else {
                	ResponseMessage::error('Somethig was wrong please try again.');
                }
            }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function updateProfile(Request $request){
    	try {
    		$rules = [
    			// image, name, mobileno, address, language
                'name' => 'required|string|max:255',
                'mobile' => 'required|numeric|digits_between:8,15',
                'sid' => 'required',
                'cid' => 'required',
                'tid' => 'required',
                'vid' => 'required',
                'language' =>'required',
                'image' => 'image'
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
                'mobile.required' => 'Please enter contact number',
                'mobile.numeric' => 'Please enter number',
                'mobile.unique' => 'Mobile number already exist.',
                'mobile.digits_between' => 'Please enter only digits and maximum 15 digits.',
                'mobile.unique' => 'Contact number is already exists.',
                'address' => 'Please enter address.',
                'language' => 'Please enter language',
                'image.image' => 'Profile image type invalid.',
                'sid.required' => 'Please enter state id',
                'cid.required' => 'Please enter city id',
                'tid.required' => 'Please enter tehsil id',
                'vid.required' => 'Please enter village id',
            ];

            $customer_id = trim(strip_tags($request->id));
			if(Customer::where('id',$customer_id)->exists()){
	        	$customer = Customer::where('id',$customer_id)->get()->first();
	        	if($customer->mobile!=trim(strip_tags($request->mobile))){
	        		$rules['mobile'] = 'required|numeric|digits_between:8,15';
	        	}
	        }
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else{
	        	$customer_id = trim(strip_tags($request->id));
	        	$name = trim(strip_tags($request->name));
                $mobile = trim(strip_tags($request->mobile));
                $sid = trim(strip_tags($request->sid));
                $cid = trim(strip_tags($request->cid));
                $tid = trim(strip_tags($request->tid));
                $vid = trim(strip_tags($request->vid));
                $language = trim(strip_tags($request->language));

                if(Customer::where('id',$customer_id)->exists()){
	        		$customer = Customer::find($customer_id);
	        		if($customer){
	        			$customer->name = $name;
	        			$customer->mobile = $mobile;
		                $customer->language = $language;
                        $customer->state_id = $sid;
                        $customer->city_id = $cid;
                        $customer->tehsil_id = $tid;
                        $customer->village_id = $vid;
    				   if($language=="gu"){
                            $state = CountryData::select('DTName_gu as DTName')->where('STCode', 'like', '%' .$sid)->where('DTCode',000)->first();  
                            $city = CountryData::select('DTName_gu as DTName')->where('DTCode', 'like', '%' .$cid)->where('SDTCode',00000)->first();
                            $tehsil = CountryData::select('SDTName_gu as SDTName')->where('SDTCode','like', '%' .$tid)->first();
                            $village = CountryData::select('Name_gu as Name')->where('TVCode','like', '%' .$vid)->first();
                        }elseif($language=="hi"){
                            $state = CountryData::select('DTName_hi as DTName')->where('STCode', 'like', '%' .$sid)->where('DTCode',000)->first();
                            $city = CountryData::select('DTName_hi as DTName')->where('DTCode', 'like', '%' .$cid)->where('SDTCode',00000)->first();
                            $tehsil = CountryData::select('SDTName_hi as SDTName')->where('SDTCode','like', '%' .$tid)->first();
                            $village = CountryData::select('Name_hi as Name')->where('TVCode','like', '%' .$vid)->first();
                        }else{
                            $state = CountryData::select('DTName')->where('STCode', 'like', '%' .$sid)->where('DTCode',000)->first();    
                            $city = CountryData::select('DTName')->where('DTCode', 'like', '%' .$cid)->where('SDTCode',00000)->first();
                            $tehsil = CountryData::select('SDTName')->where('SDTCode','like', '%' .$tid)->first();
                            $village = CountryData::select('Name')->where('TVCode','like', '%' .$vid)->first();
                        }
			        	if($request->hasFile('image')) {
			        		$file    = $request->image;
	                        $time    = md5(time());
	                        $profile = $file->getClientOriginalExtension();
	                        $name = $file->getClientOriginalName();
	                        $path    = "images/profile/" . $customer->id . "/file";
	                       // $file->move(public_path($path), $name);
                            $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
	                        $profile_update          = Customer::find($customer->id);
	                        $url = env('APP_URL');
	                        $profile_update->image = $url. "/". $path."/" .$time.'.'.$profile;
	                        $profile_update->save();
			        	}
			        	if($customer->save()){
			        		$response = Customer::select('id','name','mobile','state_id','city_id','tehsil_id','village_id','language','image')->where('id',$customer->id)->get()->first();
			        		$response->state = $state->DTName;
                            $response->city = $city->DTName;
                            $response->tehsil = $tehsil->SDTName;
                            $response->village =$village->Name;
			        		ResponseMessage::success("Profile Update successfully.",$response);
			        	} else{
			        		ResponseMessage::error("Something went wrong please try again.");	
			        	}
	        		} else{
	        			ResponseMessage::error("Customer not found");	
	        		}
	        	} else{
	        		ResponseMessage::error("Customer not found");
	        	}
            }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function sendOTP(Request $request){
    	try{
    		$rules = [
    			'mobile' => 'required|numeric|digits_between:8,15'
    		];
    		$customeMessage = [
    			'mobile.required' => 'Please enter contact number',
                'mobile.numeric' => 'Please enter number',
                'mobile.digits_between' => 'Please enter only digits and maximum 15 digits.'
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

    		if($validator->fails()) {
    			$errors = $validator->errors();
    			ResponseMessage::error($errors->first());
    		} else {
    			$mobile = trim(strip_tags($request->mobile));
    			$customer = Customer::withTrashed()->where('mobile', $mobile)->first();
    			if($customer!='') {
    			    if(!isset($customer->deleted_at)){
    			        if($customer->status==1){
        				    $state = CountryData::select('DTName')->where('STCode', 'like', '%' .$customer->state_id)->where('DTCode',000)->first();
        				    $city = CountryData::select('DTName')->where('DTCode', 'like', '%' .$customer->city_id)->first();
        				    $tehsil = CountryData::select('SDTName')->where('SDTCode','like', '%' .$customer->tehsil_id)->first();
        				    $village = CountryData::select('Name')->where('TVCode','like', '%' .$customer->village_id)->first();
        				    
        					$data['is_register'] = "true";
                            $data['id'] = $customer->id;
                            $data['name'] = $customer->name;
                            $data['address'] = $customer->address;
                            $data['state_id'] = $customer->state_id;
                            $data['state'] = $state->DTName;
                            $data['city_id'] = $customer->city_id;
                            $data['city'] = $city->DTName;
                            $data['tehsil_id'] = $customer->tehsil_id;
                            $data['tehsil'] = $tehsil->SDTName;
                            $data['village_id'] = $customer->village_id;
                            $data['village'] = $village->Name;
                            $data['language'] = $customer->language;
                            $data['image'] = $customer->image;
    			        }else{
    			            ResponseMessage::error("Your account is disable so contact to Customer care.");  
    			        }
    			    }else{
    			        ResponseMessage::error("Deleted customer");
    			    }
    			} else {
    				$data['is_register'] = "false";
    			}
    			$country_code = 91;
    			$SMSresponse = SMSHelper::sendOTP($country_code, $mobile);
    			$contact_no = trim(strip_tags($SMSresponse['phone']));
                $otp = $SMSresponse['otp'];
    			if($SMSresponse){
    				if(Customer::where('mobile',$contact_no)->exists()){
        				$customer = Customer::where('mobile', $contact_no)->first();
    	        		if($customer){
    		                $customer->otp = $otp;
    			        	$customer->save();
    	        		} else{
    	        			ResponseMessage::error("Customer not found");	
    	        		}
    	        	}
    			}
                $data['mobile'] = $mobile;
                $data['otp'] = $otp;
                ResponseMessage::success("Success", $data);
    		}
    	} catch(Exception $e) {
    		log:: error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	} 
    }

    public function verify(Request $request) {
    	try {
    		$rules = [
    			'mobile' => 'required|numeric|digits_between:8,15',
    			'otp' => 'required|numeric'
    		];
    		$customeMessage = [
    			'mobile.required' => 'Please enter contact number',
                'mobile.numeric' => 'Please enter number',
                'mobile.digits_between' => 'Please enter only digits and maximum 15 digits.',
                'otp.required' => 'Please enter otp',
                'otp.numeric' => 'Please enter number'
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

    		if($validator->fails()) {
    			$errors = $validator->errors();
    			ResponseMessage::error($errors->first());
    		} else {
	    		$mobile = trim(strip_tags($request->mobile));
	    		$otp = trim(strip_tags($request->otp));
	    		if($mobile!='' && $otp!=''){
	    			if(Customer::where('mobile',$mobile)->exists()) {
	    				$customer = Customer::where('mobile', $mobile)->first();
		        		if($customer) {
		        			if($mobile==$customer->mobile && $otp==$customer->otp) {
		        				ResponseMessage::success("Success");
		        			} else {
		        				ResponseMessage::error("Details are not match");
		        			}
		        		} else {
		        			ResponseMessage::error("Customer not found");
		        		}
		        	} else{
		        		ResponseMessage::error("Customer not found");
		        	}
	    		} else {
	    			ResponseMessage::error("Detail not found");
	    		}
	    	}
    	} catch (Exception $e) {
    		log:: error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }
    
    public function log_in_out(Request $request) {
        try {
            $rules = [
                // 'token' => 'required',
                'customer_id' => 'required',
            ];
            $customeMessage = [
                // 'token.required' => 'Please enter status',
                'customer_id.required' => 'Please enter customer id',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $token = trim(strip_tags($request->token));
                $customer_id = trim(strip_tags($request->customer_id));
                if(Customer::withTrashed()->find($customer_id)->exists()){
                    $customer = Customer::withTrashed()->find($customer_id);
                    if($token) {
                        $customer->token = $token;
                    } else {
                        $customer->token = null;
                    }
                    if($request->lang){
                        $customer->language = $request->lang;
                    }
                    $customer->deleted_at=null;
                    $customer->save();
                    ResponseMessage::success("Success",$request->lang);
                } else {
                    ResponseMessage::error("Customer not found");
                }
            }
        } catch (Exception $e) {
            log:: error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }
    
    
    public function langChange(Request $request){
        try {
            $rules = [
                'user_id' => 'required',
                'lang' => 'required',
            ];
            $customeMessage = [
                'user_id.required' => 'Please enter customer id',
                'lang.required' => 'Please enter language',
            ];
            $validator = Validator::make($request->all(),$rules,$customeMessage);

            if($validator->fails()) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                if(Customer::find($request->user_id)->exists()){
                    $customer = Customer::find($request->user_id);
                    $customer->language = $request->lang;
                    if($customer->save()){
                        ResponseMessage::success("Success");
                    }else{
                        ResponseMessage::error("Something went wrong!!");
                    }
                } else {
                    ResponseMessage::error("Customer not found");
                }
            }
        } catch (Exception $e) {
            log:: error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }
}
