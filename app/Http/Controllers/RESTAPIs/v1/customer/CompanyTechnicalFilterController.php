<?php
namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use Validator;
use App\Helper\MailHelper;
use App\Helper\ResponseMessage;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use \stdClass;
use App\Shop\Crops\Crop;
use App\Shop\TechnicalCategories\Technical;
class CompanyTechnicalFilterController extends Controller
{
    public function companyTechnicalFilter(Request $request)
    {
        try
        {

            $data = array();
            $crop_id = $request->crop_id;
            $subcropcategory_id = $request->subUser_id;
            $getCntCrop = Crop::where('id', $crop_id)->count();
            if ($getCntCrop > 0)
            {
                $crop = Crop::where('id', $crop_id)->first();
                if ($crop->product_id != null)
                {

                    // echo count(explode(",",$crop->product_id));
                    //  exit;
                    if (count(explode(",", $crop->product_id)) == 1)
                    {
                        $product_id = explode(",", $crop->product_id);

                        /*     $query = Product::with(['employees' => function($query){
                        $query->groupBy('employees.id');
                        }])->where('id',$product_id[0])->get();*/
                        $cntquery = Product::with('employees', 'technicalname')->where('id', $product_id[0])->count();
                        $query = Product::with('employees', 'technicalname')->where('id', $product_id[0])->get();

                    }
                    else
                    {

                        $getarra = str_replace("[", "", $crop->product_id);
                        $finalrar = str_replace("]", "", $getarra);

                        $query = Product::with('employees', 'technicalname')->whereIn('id', explode(",", $finalrar))->get();
                        $cntquery = Product::with('employees', 'technicalname')->whereIn('id', explode(",", $finalrar))->count();
                    }
                    if ($cntquery > 0)
                    {
                        $products = array();
                        foreach ($query as $q)
                        {

                            if ($request->get('userdetail'))
                            {
                                if ($request->get('userdetail')->language == "hi")
                                {

                                    $companies[] = ['company_id' => $q
                                        ->employees->id, 'name' => $q
                                        ->employees->name_hi];
                                }
                                elseif ($request->get('userdetail')->language == "gu")
                                {
                                    $companies[] = ['company_id' => $q
                                        ->employees->id, 'name' => $q
                                        ->employees->name_gu];
                                }
                                else
                                {
                                    $companies[] = ['company_id' => $q
                                        ->employees->id, 'name' => $q
                                        ->employees->name];
                                }
                            }

                        }
                        $company = new stdClass();
                        $company->type = 'companies';
                        $company->data = array_values(array_unique($companies, SORT_REGULAR));
                        //$company->data = $companies;
                        array_push($data, $company);
                        
                        if (Crop::where('id', $crop_id)->exists())
                        {

                            $crop = Crop::where('id', $crop_id)->first();
                            if ($crop->product_id != 'null')
                            {
                                $product_ids = json_decode($crop->product_id);

                                //echo "<pre>";
                                $tech = array();
                                $solutions = array();
                                if (count(explode(",", $crop->product_id)) == 1)
                                {
                                    
                                    if ($product_ids != '' || $product_ids != null)
                                    {

                                        //  foreach ($product_ids as $key => $product_id) {
                                        $query = Product::query();
                                        $query->join('categories', 'categories.id', 'products.category_id')
                                            ->join('employees', 'employees.id', 'products.company_id')
                                            ->where('products.id', $product_ids)->leftjoin('technicals', function ($join)
                                        {
                                            $join->on('technicals.name', '=', 'products.technical_name');
                                        });

                                        if ($request->get('userdetail'))
                                        {
                                            if ($request->get('userdetail')->language == "hi")
                                            {
                                                $query->select('technicals.id', 'technicals.name_hi as technical_name');
                                            }
                                            elseif ($request->get('userdetail')->language == "gu")
                                            {
                                                $query->select('technicals.id', 'technicals.name_gu as technical_name');
                                            }
                                            else
                                            {
                                                $query->select('technicals.id', 'products.technical_name');
                                            }
                                        }

                                        $techicals = $query->where('products.status', 1)
                                            ->first();

                                        array_push($tech, $techicals);
                                        //}
                                        $techical = new stdClass();
                                        $techical->type = 'techicals';
                                        $techical->data = $tech;
                                        array_push($data, $techical);
                                        ResponseMessage::success('Success', $data);
                                    }
                                }
                                else
                                {

                                    $product_ids = json_decode($crop->product_id);

                                    $techicals = array();
                                    if ($product_ids != '' || $product_ids != null)
                                    {

                                        $getarra = str_replace("[", "", $crop->product_id);
                                        $finalrar = str_replace("]", "", $getarra);

                                        $techicals = Product::with('technicalname')->whereIn('id', explode(",", $finalrar))->where('status', 1)
                                            ->groupBy('technical_name')
                                            ->get();
                                        $newtechnical = array();
                                        foreach ($techicals as $product)
                                        {
                                            if ($request->get('userdetail'))
                                            {
                                                if ($request->get('userdetail')->language == "hi")
                                                {
                                                    $newtechnical[] = ['id' => $product->technical_id, 'technical_name' => $product
                                                        ->technicalname->name_hi];
                                                }
                                                elseif ($request->get('userdetail')->language == "gu")
                                                {
                                                    $newtechnical[] = ['id' => $product->technical_id, 'technical_name' => $product
                                                        ->technicalname->name_gu];
                                                }
                                                else
                                                {
                                                    $newtechnical[] = ['id' => $product->technical_id, 'technical_name' => $product->technical_name];
                                                }
                                            }

                                        }
                                        array_push($tech, $newtechnical);
                                        //	}
                                        if ($cntquery > 0)
                                        {

                                            $techical = new stdClass();
                                            $techical->type = 'techicals';
                                            $techical->data = $newtechnical;
                                            array_push($data, $techical);
                                            ResponseMessage::success('Success', $data);
                                        }
                                        else
                                        {

                                            $data = array();
                                            ResponseMessage::success('Success', $data);
                                        }

                                    }
                                }

                            }

                        }
                    }
                    else
                    {
                        $data = array();
                        ResponseMessage::success('Success', $data);
                    }

                } else{
                    ResponseMessage::error('Something went wrong');
                }
            }
        }
        catch(Exception $e)
        {
            Log::error($e);
        }
    }
}

