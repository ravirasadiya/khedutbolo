<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\Shop\States\State;
use App\Shop\Cities\City;
use Log;
use App\Shop\CountriesData\CountryData;
use DB;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Google\Cloud\Translate\V2\TranslateClient;



class AddressController extends Controller
{
   public function state(Request $request) {
    	try {
    	    $states=[];
    	    if($request->customer_id!=""){
    	        $customer_language = DB::table('customers')->select('language')->where('id',$request->customer_id)->first();
    	        $customer_language = $customer_language->language;
    	        if($request->code!=""){
        	        if(CountryData::where('STCode', 'like', '%' .$request->code)->where('DTCode',000)->exists()){
        	            if($customer_language=="gu"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_gu` as `DTName` from `countryData`  WHERE `status`=1 AND  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));
        	            }elseif($customer_language=="hi"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_hi` as `DTName` from `countryData` WHERE `status`=1 AND `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }else{
        	                $states = DB::select(DB::raw("select `STCode`,`DTName` from `countryData`  WHERE `status`=1 AND  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }
        	        } else {
        	            if($customer_language=="gu"){
        	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
        	            }elseif($customer_language=="hi"){
        	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
        	            }else{
        	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
        	            }
        	        }
        	    }else{
        	        if($customer_language=="gu"){
    	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
    	            }elseif($customer_language=="hi"){
    	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
    	            }else{
    	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
    	            }
        	    }
    	    }else{
    	        if($request->code!=""){
        	        if(CountryData::where('STCode', 'like', '%' .$request->code)->where('DTCode',000)->exists()){
        	            if($request->language=="gu"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_gu` as `DTName` from `countryData` WHERE `status`=1 AND  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));
        	            }elseif($request->language=="hi"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_hi` as `DTName` from `countryData`  WHERE `status`=1 AND  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }else{
        	                $states = DB::select(DB::raw("select `STCode`,`DTName` from `countryData`  WHERE `status`=1 AND  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }
        	        } else {
        	            if($request->language=="gu"){
        	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
        	            }elseif($request->language=="hi"){
        	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
        	            }else{
        	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
        	            }
        	        }
        	    }else{
        	        if($request->language=="gu"){
    	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
        	        }elseif($request->language=="hi"){
    	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
    	            }else{
    	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->where('status',1)->orderBy('DTName', 'ASC')->get();    
    	            }
        	    }    
    	    }
    	    
    		ResponseMessage::success('Success', $states);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function getstate(Request $request) {
    	try {
    	    $states=[];
    	    if($request->customer_id!=""){
    	        $customer_language = DB::table('customers')->select('language')->where('id',$request->customer_id)->first();
    	        $customer_language = $customer_language->language;
    	        if($request->code!=""){
        	        if(CountryData::where('STCode', 'like', '%' .$request->code)->where('DTCode',000)->exists()){
        	            if($customer_language=="gu"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_gu` as `DTName` from `countryData`  WHERE   `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));
        	            }elseif($customer_language=="hi"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_hi` as `DTName` from `countryData` WHERE  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }else{
        	                $states = DB::select(DB::raw("select `STCode`,`DTName` from `countryData`  WHERE   `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }
        	        } else {
        	            if($customer_language=="gu"){
        	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
        	            }elseif($customer_language=="hi"){
        	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
        	            }else{
        	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
        	            }
        	        }
        	    }else{
        	        if($customer_language=="gu"){
    	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
    	            }elseif($customer_language=="hi"){
    	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
    	            }else{
    	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
    	            }
        	    }
    	    }else{
    	        if($request->code!=""){
        	        if(CountryData::where('STCode', 'like', '%' .$request->code)->where('DTCode',000)->exists()){
        	            if($request->language=="gu"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_gu` as `DTName` from `countryData` WHERE  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));
        	            }elseif($request->language=="hi"){
        	                $states = DB::select(DB::raw("select `STCode`,`DTName_hi` as `DTName` from `countryData`  WHERE  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }else{
        	                $states = DB::select(DB::raw("select `STCode`,`DTName` from `countryData`  WHERE  `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));    
        	            }
        	        } else {
        	            if($request->language=="gu"){
        	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
        	            }elseif($request->language=="hi"){
        	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
        	            }else{
        	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
        	            }
        	        }
        	    }else{
        	        if($request->language=="gu"){
    	                $states = CountryData::select('STCode', 'DTName_gu as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
        	        }elseif($request->language=="hi"){
    	                $states = CountryData::select('STCode', 'DTName_hi as DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
    	            }else{
    	                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();    
    	            }
        	    }    
    	    }
    	    
    		ResponseMessage::success('Success', $states);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    
    public function city(Request $request) {
    	try {
    		$id = trim(strip_tags($request->state_id));
    		$cities = CountryData::select('DTCode', 'DTName','id','DTName_hi','DTName_gu')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();
    		foreach ($cities as $city) {
                $city_data = CountryData::where('id', $city->id)->first();
                $apiKey = 'AIzaSyAu6nKX700afPHYN-MnlsGWoSrlw9Egfgg';
                
                if($city_data->DTName_gu==NULL){
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = trim($city_data->DTName);
                    $target = 'gu';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $city_data->DTName_gu = $translation['text'];
                    $city_data->save();
                }
                if($city_data->DTName_hi==NULL){
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = trim($city_data->DTName);
                    $target = 'hi';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $city_data->DTName_hi = $translation['text'];
                    $city_data->save();
                }
    		}
    		if($request->customer_id!=""){
    		    $customer_language = DB::table('customers')->select('language')->where('id',$request->customer_id)->first();
    	        $customer_language = $customer_language->language;
    		    $cities=[];
        	    if($request->code!=""){
        	        if(CountryData::where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' .$id)->exists()){
        	            if($customer_language=="gu"){
        	                $cities = DB::select(DB::raw("select `DTCode`,`DTName_gu` as `DTName`  from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
        	            }elseif($customer_language=="hi"){
        	                $cities = DB::select(DB::raw("select `DTCode`,`DTName_hi` as `DTName` from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
        	            }else{
        	                $cities = DB::select(DB::raw("select `DTCode`,`DTName` from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
        	            }
        	        } else {
        	            if($customer_language=="gu"){
        	                $cities = CountryData::select('DTCode', 'DTName_gu as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
        	            }elseif($customer_language=="hi"){
        	                $cities = CountryData::select('DTCode', 'DTName_hi as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
        	            }else{
        	                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
        	            }
        	        }
        	    }
        	    else{
                    if($customer_language=="gu"){
    	                $cities = CountryData::select('DTCode', 'DTName_gu as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
    	            }elseif($customer_language=="hi"){
    	                $cities = CountryData::select('DTCode', 'DTName_hi as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
    	            }else{
    	                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
    	            }
        	    }
    		}else{
    		    $cities=[];
        	    if($request->code!=""){
        	        if(CountryData::where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' .$id)->exists()){
        	            if($request->language=="gu"){
        	                $cities = DB::select(DB::raw("select `DTCode`,`DTName_gu` as `DTName`  from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
        	            }elseif($request->language=="hi"){
        	                $cities = DB::select(DB::raw("select `DTCode`,`DTName_hi` as `DTName` from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
        	            }else{
        	                $cities = DB::select(DB::raw("select `DTCode`,`DTName` from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
        	            }
        	        } else {
        	            if($request->language=="gu"){
        	                $cities = CountryData::select('DTCode', 'DTName_gu as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
        	            }elseif($request->language=="hi"){
        	                $cities = CountryData::select('DTCode', 'DTName_hi as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
        	            }else{
        	                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
        	            }
        	        }
        	    }
        	    else{
                    if($request->language=="gu"){
    	                $cities = CountryData::select('DTCode', 'DTName_gu as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
                    }elseif($request->language=="hi"){
    	                $cities = CountryData::select('DTCode', 'DTName_hi as DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
    	            }else{
    	                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();    
    	            }
        	    }
    		}
    		
    		ResponseMessage::success('Success', $cities);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    public function tehsil(Request $request) {
        try {
            $id = trim(strip_tags($request->city_id));
            $tehsils = countrydata::select('SDTCode', 'SDTName','SDTName_hi','SDTName_gu','id')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
            // dd($tehsils);
            foreach ($tehsils as $tehsil) {
                $tehsil_data = CountryData::where('id', $tehsil->id)->first();
                $apiKey = 'AIzaSyAu6nKX700afPHYN-MnlsGWoSrlw9Egfgg';
               
                if($tehsil_data->SDTName_gu==NULL){
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = trim($tehsil_data->SDTName);
                    $target = 'gu';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $tehsil_data->SDTName_gu = $translation['text'];
                    $tehsil_data->save();
                }
                if($tehsil_data->SDTName_hi==NULL){
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = trim($tehsil_data->SDTName);
                    $target = 'hi';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $tehsil_data->SDTName_hi = $translation['text'];
                    $tehsil_data->save();
                }
    		}
            if($request->customer_id!=""){
                $tehsils=[];
                $customer_language = DB::table('customers')->select('language')->where('id',$request->customer_id)->first();
    	        $customer_language = $customer_language->language;
                if($request->code!=""){
        	        if(CountryData::where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->exists()){
        	            if($customer_language=="gu"){
        	                $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName_gu` as `SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
        	            }elseif($customer_language=="hi"){
        	                $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName_hi` as `SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
        	            }else{
        	                $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
        	            }    
                        
        	        } else {
        	            if($customer_language=="gu"){
        	                $tehsils = countrydata::select('SDTCode', 'SDTName_gu as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
        	            }elseif($customer_language=="hi"){
        	                $tehsils = countrydata::select('SDTCode', 'SDTName_hi as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
        	            }else{
        	                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();    
        	            }
        	        }
        	    }else{
                    if($customer_language=="gu"){
    	                $tehsils = countrydata::select('SDTCode', 'SDTName_gu as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
    	            }elseif($customer_language=="hi"){
    	                $tehsils = countrydata::select('SDTCode', 'SDTName_hi as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
    	            }else{
    	                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();    
    	            }
        	    } 
            }else{
                $tehsils=[];
                if($request->code!=""){
        	        if(CountryData::where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->exists()){
                        if($request->language=="gu"){
        	                $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName_gu` as `SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
        	            }elseif($request->language=="hi"){
        	                $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName_hi` as `SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
        	            }else{
        	                $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
        	            }
        	        } else {
        	            if($request->language=="gu"){
        	                $tehsils = countrydata::select('SDTCode', 'SDTName_gu as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
        	            }elseif($request->language=="hi"){
        	                $tehsils = countrydata::select('SDTCode', 'SDTName_hi as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
        	            }else{
        	                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();    
        	            }
        	        }
        	    }else{
        	        if($request->language=="gu"){
    	                $tehsils = countrydata::select('SDTCode', 'SDTName_gu as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
        	        }elseif($request->language=="hi"){
    	                $tehsils = countrydata::select('SDTCode', 'SDTName_hi as SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
    	            }else{
    	                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();    
    	            }
        	    }    
            }    
            
            ResponseMessage::success('Success', $tehsils);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function village(Request $request) {
        try {
            $id = trim(strip_tags($request->tehsil_id));
            $villages = countrydata::select('TVCode', 'Name','Name_hi','Name_gu','id')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
            foreach ($villages as $village) {
                $village_data = CountryData::where('id', $village->id)->first();
                $apiKey = 'AIzaSyAu6nKX700afPHYN-MnlsGWoSrlw9Egfgg';
                if($village_data->Name_gu==NULL){
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = trim($village_data->Name);
                    $target = 'gu';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $village_data->Name_gu = $translation['text'];
                    $village_data->save();
                }
                if($village_data->Name_hi==NULL){
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = trim($village_data->Name);
                    $target = 'hi';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $village_data->Name_hi = $translation['text'];
                    $village_data->save();
                }
    		}
            if($request->customer_id!=""){
                $customer_language = DB::table('customers')->select('language')->where('id',$request->customer_id)->first();
                $customer_language = $customer_language->language;
                if($request->code!=""){
        	        if(CountryData::where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->exists()){
                        if($customer_language=="gu"){
                            $villages = DB::select(DB::raw("select `TVCode`,`Name_gu` as `Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
                        }elseif ($customer_language=="hi") {
                            $villages = DB::select(DB::raw("select `TVCode`,`Name_hi` as `Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
                        }else{
                            $villages = DB::select(DB::raw("select `TVCode`,`Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
                        }
        	        } else {
                        if($customer_language=="gu"){
                            $villages = countrydata::select('TVCode', 'Name_gu as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                        }elseif ($customer_language=="hi") {
                            $villages = countrydata::select('TVCode', 'Name_hi as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                        }else{
                            $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                        }
        	        }
        	    }else{
                    if($customer_language=="gu"){
                        $villages = countrydata::select('TVCode', 'Name_gu as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                    }elseif ($customer_language=="hi") {
                        $villages = countrydata::select('TVCode', 'Name_hi as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                    }else{
                        $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                    }
        	    }
            }else{
                if($request->code!=""){
        	        if(CountryData::where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->exists()){
                        if($request->language=="gu"){
                            $villages = DB::select(DB::raw("select `TVCode`,`Name_gu` as `Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
                        }elseif ($request->language=="hi") {
                            $villages = DB::select(DB::raw("select `TVCode`,`Name_hi` as `Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
                        }else{
                            $villages = DB::select(DB::raw("select `TVCode`,`Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
                        }
        	        } else {
                        if($request->language=="gu"){
                            $villages = countrydata::select('TVCode', 'Name_gu as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                        }elseif ($request->language=="hi") {
                            $villages = countrydata::select('TVCode', 'Name_hi as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                        }else{
                            $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                        }
        	        }
        	    }else{
                    if($request->language=="gu"){
                        $villages = countrydata::select('TVCode', 'Name_gu as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                    }elseif ($request->language=="hi") {
                        $villages = countrydata::select('TVCode', 'Name_hi as Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                    }else{
                        $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                    }
        	    }    
            }
            ResponseMessage::success('Success', $villages);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function translate() {
        try{
            $apiKey = 'AIzaSyAu6nKX700afPHYN-MnlsGWoSrlw9Egfgg';
           
            // $translate = new TranslateClient([
            //     'key' => $apiKey
            // ]);
            // $text = 'Hello, world!';
            // $target = 'gu';
            // $translation = $translate->translate($text, [
            //     'target' => $target
            // ]);
           
            $list = CountryData::select('STCode', 'DTName as name')->where('DTCode',000)->where('status','1')->get();
            $villages = countrydata::select('TVCode', 'Name','id')->where('STCode','24')->where('SDTCode','!=', 00000)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->where('Name_gu',null)->get();
            // dd($villages);
            foreach($villages as $village){
                $village_data = countrydata::where('id',$village->id)->first();
                if($village_data->Name_gu==NULL){
                    
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = $village_data->Name;
                    $target = 'gu';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $village_data->Name_gu = $translation['text'];
                   
                }
                if($village_data->Name_hi==NULL){
                    $translate = new TranslateClient([
                        'key' => $apiKey
                    ]);
                    $text = $village_data->Name;
                    $target = 'hi';
                    $translation = $translate->translate($text, [
                        'target' => $target
                    ]);
                    $village_data->Name_hi = $translation['text'];
                    //  dd($village_data);
                    
                }
                $village_data->save();
                
            }
            // foreach($list as $state){
         
            //     $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $state->STCode)->orderBy('DTName', 'ASC')->get();
            //     // dd($cities);
            //     foreach ($cities as $city) {
            //         $city_data = CountryData::where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $state->STCode)->where('DTCode', 'like', '%' . $city->DTCode)->first();
            //         $last_data = $city_data->id;
            //         // if($city_data->DTName_gu==NULL){
            //         //     dd($city_data);
            //         //     $translate = new TranslateClient([
            //         //         'key' => $apiKey
            //         //     ]);
            //         //     $text = $city_data->DTName;
            //         //     $target = 'gu';
            //         //     $translation = $translate->translate($text, [
            //         //         'target' => $target
            //         //     ]);
            //         //     $city_data->DTName_gu = $translation['text'];
            //         // }
            //         // if($city_data->DTName_hi==NULL){
            //         //     dd($city_data);
            //         //     $translate = new TranslateClient([
            //         //         'key' => $apiKey
            //         //     ]);
            //         //     $text = $city_data->DTName;
            //         //     $target = 'hi';
            //         //     $translation = $translate->translate($text, [
            //         //         'target' => $target
            //         //     ]);
            //         //     $city_data->DTName_hi = $translation['text'];
            //         // }
                    
            //         $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$city->DTCode)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
            //         foreach ($tehsils as $tehsil) {
            //             $tehsil_data = countrydata::where('DTCode', 'like', '%' .$city->DTCode)->where('SDTCode', '!=',00000)->where('TVCode',000000)->where('SDTCode', 'like', '%' .$tehsil->SDTCode)->first();
            //             $last_data = $tehsil_data->id;
                        
            //             // if($tehsil_data->SDTName_gu==NULL){
            //             //     dd('dd');
            //             //     $translate = new TranslateClient([
            //             //         'key' => $apiKey
            //             //     ]);
            //             //     $text = $tehsil_data->SDTName;
            //             //     $target = 'gu';
            //             //     $translation = $translate->translate($text, [
            //             //         'target' => $target
            //             //     ]);
            //             //     $tehsil_data->SDTName_gu = $translation['text'];
            //             // }
            //             // if($tehsil_data->SDTName_hi==NULL){
            //             //     dd('dd');
            //             //     $translate = new TranslateClient([
            //             //         'key' => $apiKey
            //             //     ]);
            //             //     $text = $tehsil_data->SDTName;
            //             //     $target = 'hi';
            //             //     $translation = $translate->translate($text, [
            //             //         'target' => $target
            //             //     ]);
            //             //     $tehsil_data->SDTName_hi = $translation['text'];
            //             // }
            //             // $tehsil_data->save();
            //             // $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$tehsil->SDTCode)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
            //             // foreach($villages as $village){
            //             //     $village_data = countrydata::where('SDTCode', 'like', '%' .$tehsil->SDTCode)->where('TVCode', 'like', '%' .$village->TVCode)->where('TVCode', '!=',000000)->first();
            //             //     if($village_data->Name_gu==NULL){
            //             //         // dd($village_data);
            //             //         $translate = new TranslateClient([
            //             //             'key' => $apiKey
            //             //         ]);
            //             //         $text = $village_data->Name;
            //             //         $target = 'gu';
            //             //         $translation = $translate->translate($text, [
            //             //             'target' => $target
            //             //         ]);
            //             //         $village_data->Name_gu = $translation['text'];
            //             //     }
            //             //     if($village_data->Name_hi==NULL){
            //             //         $translate = new TranslateClient([
            //             //             'key' => $apiKey
            //             //         ]);
            //             //         $text = $village_data->Name;
            //             //         $target = 'hi';
            //             //         $translation = $translate->translate($text, [
            //             //             'target' => $target
            //             //         ]);
            //             //         $village_data->Name_hi = $translation['text'];
            //             //     }
                            
            //             //     $village_data->save();
            //             // }
            //         }
            //     }
            // }
            ResponseMessage::success('Success', "Success");
        } catch (Exception $e) {
            log::error($last_data." ".$e);
        }
    }
}
