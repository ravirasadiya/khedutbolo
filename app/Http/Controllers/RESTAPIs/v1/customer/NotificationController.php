<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Notifications\Notification;
use App\Shop\CustomerNotifications\CustomerNotification;
use App\Shop\Employees\Employee;
use App\Shop\Addresses\Address;
use App\Shop\States\State;
use App\Shop\Cities\City;
use App\Helper\ResponseMessage;
use App\Shop\Customers\Customer;
use App\Shop\Products\Product;
use App\Shop\Countries\Country;
use App\Shop\Orders\Order;
use App\Shop\Carts\Cart;
use \stdClass;
use Validator;
use Log;
use App\Helper\NotificationHelper;
use App\Helper\MailHelper;
use App\Helper\SMSHelper;
use PDF;
use File;

class NotificationController extends Controller
{
    public function notify(Request $request) {
    	try {
    		$rules = [
    			'user_id' => 'required',
    		];
    		$customeMessage = [
    			'user_id.required' => 'Please enter user id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$admin_id = trim(strip_tags($request->user_id));
		    	if(Customer::where('id', $admin_id)->where('status',1)->exists()) {
		    	    if(Notification::where('customer_id', $admin_id)->first()){
    		    		$notify = Notification::Select('notify_detail', 'created_at as date', 'time')
    		    								->where('customer_id', $admin_id)
    								    		->orderBy('id', 'DESC')
    								    		->get();
		    	} else{
		    	    ResponseMessage::error('Customer notification not found!!');
		    	}
		    		if($notify){
		    			ResponseMessage::success('Success', $notify);
		    		}
		    	}
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }


    public function sendNotification(Request $request) {
    	try {
    		$rules = [
				'userid' => 'required',
				'title' => 'required',
				'message' => 'required',
				];
			$customeMessage = [
					"userid.required"=>"Please enter user id.",
					"title.required"=>"Please enter title.",
					"message.required"=>"Please enter message.",
				];
			$validator = Validator::make($request->all(), $rules,$customeMessage);

			if($validator->fails()){
				return back()->withErrors($validator);
			}else{
				// $server_api_key = 'AAAAr2mz3qw:APA91bHreE9QcwXtIMa221zDANf-Nt1lpFHwNB7u1T3E2qVqFVPYbGt_NRBQT-khwCiZsHQcgaRND4uNNhRVDswAhH9SRorI19s0e-uLpsywUL1YCU0_E7Rm1HFkJ54iWwwcNiOmn9mS';
				 $server_api_key = 'AAAABWo6xZM:APA91bFjaUiBMsm9HfzFioojNYA8_14Fr4mqcCKtZXlWWNGMVnuynYTCuwu9kphKIfPec3GuJv-Qx2HatvX9bR4PrM0YUTHvrrApKR9RYzflk7ogMjH3LaTuNYvJ3T05tHKMA5CHMiuY';
				$title = trim($request->title);;
				$userid = trim($request->userid);
				$message = trim($request->message);
				$token = Customer::select('token')->where('id', $userid)->first();
				$token = $token->token;
				$result = NotificationHelper::notification($message, $server_api_key, $token, $title);
				$result = json_decode($result);
				if($result->results[0]->message_id!=""){
					ResponseMessage::success('Success', 'Notification Successfully Send.');
				}else{
					ResponseMessage::error('Something was wrong please try again.');
				}
			}
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }


    public function productNotify() {
    	try {
    		$products = Product::join('employees', 'employees.id', 'products.company_id')
    							->Select('products.id', 'products.name', 'products.available_quantity', 'employees.name as company_name', 'employees.email', 'products.mail_status')
    							->where('available_quantity','<',10)
    							->where('employees.status',1)
    							->where('products.status',1)
    							->get();
    		foreach ($products as $product) {
    			if($product->mail_status == 0) {
    				$mail = MailHelper::productNotification($product);
    				$value = Product::find($product->id);
    				$value->mail_status=1;
    				$value->save();
    			}
    		}
    		ResponseMessage::success('Success', 'Notification send successfully.');
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

    public function sendInvoice() {
    	try {
    		$orders = Order::where('order_status_id', 3)
    						->where('mail_status', 1)
    						->get();
    		if($orders) {
	    		foreach ($orders as $order) {		
		    		$customer = Customer::find($order->customer_id);
		    		$address = Address::find($order->address_id);
		    		$data['email'] = $customer->email;
		    		$data['name'] = $customer->name;
		    		$data['order'] = $order;
		    		$data['address'] = $address;
			        $state = State::find($order->address->state_code);
			        $order->address->state_code = $state->state;
			        $country = Country::find($order->address->country_id);
			        $order->address->country_id = $country->name;
			        if(!$order->invoice_path) {
				        $data1 = [
				            'order' => $order,
				            'products' => $order->products,
				            'customer' => $order->customer,
				            'courier' => $order->courier,
				            'address' => $order->address,
				            'status' => $order->orderStatus,
				            'payment' => $order->paymentMethod
				        ];
				        $pdf = PDF::loadView('invoices.orders', $data1);

			    		$content = $pdf->download()->getOriginalContent();
			    		$file = File::put(public_path('invoices/'.$data['order']->id.'.pdf'),$content);
			    	}
		    		$result = MailHelper::invoiceNotification($data);
		    		$order->mail_status=0;
		    		$order->invoice_path='invoices/'.$data['order']->id.'.pdf';
		    		$order->update();
		    	}
		    	ResponseMessage::success('Success', 'Invoice send successfully to customer.');
		    } else {
		    	ResponseMessage::error('Orders not found!!!');
		    }
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }
    
    public function customerNotify(Request $request){
    	try {
    		$rules = [
				'company_id' => 'required',
				];
			$customeMessage = [
					"company_id.required"=>"Please enter company id.",
				];
			$validator = Validator::make($request->all(), $rules,$customeMessage);

			if($validator->fails()){
				return back()->withErrors($validator);
			}else{
				$company_id = trim(strip_tags($request->company_id));
				$notify_types = CustomerNotification::where('company_id', $company_id)
													->where('status', 1)
													->first();
				$notify_type = $notify_types->customer_type;
				$value=$notify_types->value;
				$country_code="+91";
				$msg=$notify_types->msg;
				if($notify_type=='all_customer'){
					$customers = Employee::join('products', 'products.company_id', 'employees.id')
                            ->join('order_product', 'order_product.product_id', 'products.id')
                            ->join('orders', 'orders.id', 'order_product.order_id')
                            ->join('customers', 'customers.id', 'orders.customer_id')
                            ->select('customers.id', 'customers.name', 'customers.mobile')
                            ->where('employees.id', $request->company_id)
                            ->distinct()
                            ->get();
                    if($customers) {
	                    foreach ($customers as $customer) {
	                    	$SMSresponse = SMSHelper::customerNotification($country_code, $customer->mobile, $msg);
	                    	if($SMSresponse=='false') {
	                    		ResponseMessage::error('Something was wrong please try again.');
	                    	}
	                    }
	                }
				}
				if($notify_type=='single_customer'){
					$customer=Customer::find($value);
					if($customer) {
	                    $SMSresponse = SMSHelper::customerNotification($country_code, $customer->mobile, $msg);
	                    // dd($SMSresponse);
	                    if($SMSresponse=='false') {
                    		ResponseMessage::error('Something was wrong please try again.');
                    	}
					}
				}
				if($notify_type=='state_customer'){
					$customers = Employee::join('products', 'products.company_id', 'employees.id')
                            ->join('order_product', 'order_product.product_id', 'products.id')
                            ->join('orders','orders.id','order_product.order_id')
                            ->join('addresses', 'addresses.id', 'orders.address_id')
                            ->join('customers', 'customers.id', 'addresses.customer_id')
                            ->select('customers.id', 'customers.name', 'customers.mobile')
                            ->where('employees.id', $request->company_id)
                            ->where('addresses.state_code', $value)
                            ->distinct()
                            ->get();
                    if($customers) {
	                    foreach ($customers as $customer) {
	                    	$SMSresponse = SMSHelper::customerNotification($country_code, $customer->mobile, $msg);
	                    	if($SMSresponse=='false') {
	                    		ResponseMessage::error('Something was wrong please try again.');
	                    	}
	                    }
	                }
				}
				if($notify_type=='city_customer'){
					$customers = Employee::join('products', 'products.company_id', 'employees.id')
                            ->join('order_product', 'order_product.product_id', 'products.id')
                            ->join('orders','orders.id','order_product.order_id')
                            ->join('addresses', 'addresses.id', 'orders.address_id')
                            ->join('customers', 'customers.id', 'addresses.customer_id')
                            ->select('customers.id', 'customers.name', 'customers.mobile')
                            ->where('employees.id', $request->company_id)
                            ->where('addresses.city', $value)
                            ->distinct()
                            ->get();
                    if($customers) {
	                    foreach ($customers as $customer) {
	                    	$SMSresponse = SMSHelper::customerNotification($country_code, $customer->mobile, $msg);
	                    	if($SMSresponse=='false') {
	                    		ResponseMessage::error('Something was wrong please try again.');
	                    	}
	                    }
	                }
				}
				if($notify_type=='tehsil_customer'){
					$customers = Employee::join('products', 'products.company_id', 'employees.id')
                            ->join('order_product', 'order_product.product_id', 'products.id')
                            ->join('orders','orders.id','order_product.order_id')
                            ->join('addresses', 'addresses.id', 'orders.address_id')
                            ->join('customers', 'customers.id', 'addresses.customer_id')
                            ->select('customers.id', 'customers.name', 'customers.mobile')
                            ->where('employees.id', $request->company_id)
                            ->where('addresses.tehsil_id', $value)
                            ->distinct()
                            ->get();
                    if($customers) {
	                    foreach ($customers as $customer) {
	                    	$SMSresponse = SMSHelper::customerNotification($country_code, $customer->mobile, $msg);
	                    	if($SMSresponse=='false') {
	                    		ResponseMessage::error('Something was wrong please try again.');
	                    	}
	                    }
	                }
				}
				if($notify_type=='village_customer'){
					$customers = Employee::join('products', 'products.company_id', 'employees.id')
                            ->join('order_product', 'order_product.product_id', 'products.id')
                            ->join('orders','orders.id','order_product.order_id')
                            ->join('addresses', 'addresses.id', 'orders.address_id')
                            ->join('customers', 'customers.id', 'addresses.customer_id')
                            ->select('customers.id', 'customers.name', 'customers.mobile')
                            ->where('employees.id', $request->company_id)
                            ->where('addresses.village_id', $value)
                            ->distinct()
                            ->get();
                    if($customers) {
	                    foreach ($customers as $customer) {
	                    	$SMSresponse = SMSHelper::customerNotification($country_code, $customer->mobile, $msg);
	                    	if($SMSresponse=='false') {
	                    		ResponseMessage::error('Something was wrong please try again.');
	                    	}
	                    }
	                }
				}
				// $notify_types->status=0;
				// $notify_types->update();
	            ResponseMessage::success('Success', 'Notification send successfully to customer.');
			}
    	} catch (Exception $e) {
    		log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
    	}
    }

}
