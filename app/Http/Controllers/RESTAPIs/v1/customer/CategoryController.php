<?php

namespace App\Http\Controllers\RESTAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use App\Shop\Priorities\Priority;
use \stdClass;
use App\Helper\MailHelper;
use App\Shop\Priorities\TrendingProduct;
use App\Shop\Products\Product_attributes;
use App\Shop\TechnicalCategories\Technical;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
	public function productCategories(Request $request) {
		try {
			$data = array();
			$comp = array();
			$cate = array();
			$sub_cat = array();
			if(Category::where('parent_id', null)->exists()) {

				$categories = Category::Select('id', 'name', 'name_hi', 'name_gu', 'cover')->where('parent_id', null)->where('status',1)->get();
				foreach ($categories as $category) {
					$cate_add = new stdClass();
					$cate_add->id=$category->id;
					if($request->get('userdetail')){
                       	if($request->get('userdetail')->language=="hi"){
                       		$cate_add->name=$category->name_hi;
                       	} else if($request->get('userdetail')->language=="gu"){
                       		$cate_add->name=$category->name_gu;
                       	} else{
                       		$cate_add->name=$category->name;
                       	}
                   	} else{
						$cate_add->name=$category->name;
                   	}
					$cate_add->cover=$category->cover;
					$cate_add->price=null;
					$cate_add->sale_price=null;
					$cate_add->company_name=null;
					
					array_push($cate, $cate_add);
				}
				$companies = Employee::Select('id', 'name','name_hi','name_gu', 'logo as cover','deleted_at')->where('status',1)->where('deleted_at','=',null)->get();
				foreach ($companies as $company) {
					$company_add = new stdClass();
					$company_add->id=$company->id;
					if($request->get('userdetail')){
                       	if($request->get('userdetail')->language=="hi"){
                       		$company_add->name=$company->name_hi;
                       	} else if($request->get('userdetail')->language=="gu"){
                       		$company_add->name=$company->name_gu;
                       	} else{
                       		$company_add->name=$company->name;
                       	}
                   	} else{
						$company_add->name=$company->name;
                   	}
					$company_add->cover=$company->cover;
					$company_add->price=null;
					$company_add->sale_price=null;
					$company_add->company_name=null;
				    $company_add->deleted_at=$company->deleted_at;
					array_push($comp, $company_add);
				}

				$category = new stdClass();
				$category->type = 'Product Categories / Manage Products';
				$category->data = $cate;
				$priority = Priority::where('name', $category->type)->first();
				$category->priority = $priority->priority;

				array_push($data, $category);

				$company = new stdClass();
				$company->type = 'Company List';
				$company->data = $comp;
				$priority = Priority::where('name', $company->type)->first();
				$company->priority = $priority->priority;
				array_push($data, $company);

				$product = new stdClass();
				$product->type = 'Trending Products';
				$priority = Priority::where('name', $product->type)->first();
				$subcategories = TrendingProduct::select('priority','product_id')->get();
				foreach ($subcategories as $subcategory) {
					$productquery = Product::query();
					$productquery->join('employees', 'employees.id', 'products.company_id')
								->join('product_attribute','product_attribute.product_id','products.id');
					if($request->get('userdetail')){
                        if($request->get('userdetail')->language=="hi"){
                        	$productquery->Select('products.id', 'products.name_hi as name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name_hi as company_name');
                        } elseif($request->get('userdetail')->language=="gu"){
                        	$productquery->Select('products.id', 'products.name_gu as name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name_gu as company_name');
                        } else{
                        	$productquery->Select('products.id', 'products.name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name as company_name');
                        }
                    } else{
                    	$productquery->Select('products.id', 'products.name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name as company_name');
                    }
					$sub_category_data = $productquery->where('products.status',1)
									->where('employees.status',1)
									->where('products.id', $subcategory->product_id)
									->first();
									
					if($sub_category_data){
				        $sub_category_data->price = str_replace(",","",$sub_category_data->price);
				        $sub_category_data->sale_price = str_replace(",","",$sub_category_data->sale_price);
						array_push($sub_cat, $sub_category_data);
					}
				}
				$product->data = $sub_cat;
				$product->priority = $priority->priority;
			
				array_push($data, $product);
				ResponseMessage::success('Success', $data);
			} else {
				ResponseMessage::error('Category not found.');
			}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}

	public function selectedCategory(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter sub-category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
	        	$data=[];
				if(Category::where('id', $id)->where('status',1)->exists()) {
	        		$category = array();
	                $about_product = array();
	                $i=1;
	                $technicals = Technical::get();
	                if(Product::where('subcategory_id', $id)->orwhere('category_id',$id)->where('status',1)->exists()){
	                    $technicalquery = Technical::query();
	                    $productquery = Product::query();
	                    $productquery->join('categories',function($query){
				                    	$query->on('categories.id','=','products.category_id')->where('subcategory_id',null);
				                    	$query->oron('categories.id','=','products.subcategory_id')->where('subcategory_id','!=',null);
				                    })
	                                ->join('employees', 'employees.id', 'products.company_id')
	                                ->leftjoin('technicals',function($join){
	                                    $join->on('technicals.name','=','products.technical_name');
	                                });
	                        if($request->get('userdetail')){
	                            if($request->get('userdetail')->language=="hi"){
	                                $technicalquery->select('*','name_hi as name');
	                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
	                            } elseif($request->get('userdetail')->language=="gu"){
	                                $technicalquery->select('*','name_gu as name');
	                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
	                            } else{
	                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                            }
	                        } else{
	                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                        }
	                    $solutions = $productquery->where('products.subcategory_id', $id)->where('products.status',1)->where('employees.status',1)->where('categories.status',1)
	                                ->orwhere('products.category_id', $id)
	                                ->get();
	                    $technicals = $technicalquery->get();
                        $technical = array();
                        if(count($solutions)>0) {
                            foreach($solutions as $solution) {
                            	$attr = Product_attributes::where('product_id',$solution->id)->first();
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $solution->price = $price;
                                    $solution->sale_price = $sale_price;
                                }
                                if($solution->additional_info) {
                                    $decode = json_decode($solution->additional_info);
                                    $solution->additional_info = $decode;
                                }else {
                                    $solution->additional_info = [];
                                }
                                // array_push($technical, $solution);
                            }
    		                for($j=0; $j<count($technicals);$j++) {
    		                    for($i=0; $i<count($solutions);$i++) {
    		                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    		                            array_push($technical, $solutions[$i]);
    		                        }
    		                    }  
    		                }
	                        $data = $technical;
                        }
		        		ResponseMessage::success('Success', $data);
		        	}else {
						ResponseMessage::error('Product not found!!');
					}
				} else {
					ResponseMessage::error('Category id not found!!');
				}
	        }
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}

	public function get_subCategory(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
	        	
	        
				if(Category::where('id', $id)->exists()) {
				    
				// 	$categoryquery = Category::query();
				// 	dd($categoryquery);
				
				$categoryquery = DB::table('categories');
                
					if($request->get('userdetail'))
					{
                    	if($request->get('userdetail')->language == "hi"){
                        $categoryquery->select('id','name_hi as name','cover');
                    	}else if($request->get('userdetail')->language == "gu"){
                    	 $categoryquery->select('id','name_gu as name','cover');
                    	} else{
                    		$categoryquery->select('id','name','cover');
                    	}
                    } else{
                    	$categoryquery->select('id','name','cover');
                    }
                    // dd($categoryquery);
		        	$sub_categories = $categoryquery->where('parent_id', $id)->where('status',1)->get();
                // dd($sub_categories);
		        	$extra=[];
                    $data=[];
                    $extra['subcategory_status']=0;
		        	if(count($sub_categories)>0){
		        		$extra['subcategory_status']=0;
		        		$data=$sub_categories;
		        	} else {
		        		$category = array();
		                $about_product = array();
		                $i=1;
                    	// $technicals = Technical::get();
		                if(Product::where('category_id', $id)->where('status',1)->exists()){
		        			$extra['subcategory_status']=1;
		        			$technicalquery = Technical::query();
		        			$productquery = Product::query();
		        			
		                    $productquery->join('categories', 'categories.id', 'products.category_id')
	                                ->join('employees', 'employees.id', 'products.company_id')
	                                ->leftjoin('technicals',function($join){
	                                    $join->on('technicals.name','=','products.technical_name');
	                                });
	                        if($request->get('userdetail')){
	                       
	                          
	                            if($request->get('userdetail')->language=="hi"){
	                            /*    echo "first";
	                               exit;*/
	                                $technicalquery->select('*','name_hi as name');
	                                $productquery->select('products.id', 'products.name_hi as name', 'products.price','technicals.id','technicals.id as technical_id', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
	                            } elseif($request->get('userdetail')->language=="gu"){
	                            /* echo "second";
	                               exit;*/
	                                $technicalquery->select('*','name_gu as name');
	                                $productquery->select('products.id', 'products.name_gu as name', 'products.price','technicals.id','technicals.id as technical_id', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
	                            } else{
	                            /*  echo "third";
	                               exit;*/
	                              $productquery =  $productquery->select('products.id', 'products.name', 'products.technical_name','technicals.id','technicals.id as technical_id', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                          
	                            }
	                        } else{
	                           
	                            $productquery->select('products.id', 'products.name', 'products.technical_name','technicals.id','technicals.id as technical_id', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
	                        }
	                       // exit;
	                   $solutions = $productquery->where('products.category_id', $id)->where('products.status',1)->get();
	                    $productcnt= Product::where('category_id', $id)->where('status',1)->count();
	                    
	                    $technicals = $technicalquery->get();
	           //   dd($technicals);
	           //   dd($solutions);
				// 		echo "====>>>>>".$productcnt;
				// 		exit;
	                        $technical = array();
	                        if($productcnt>0) {
	                           $solutions = Product::with('categoryname','employees','technicalname')
	                           ->leftjoin('technicals', 'technicals.name', 'products.technical_name')
	                           ->where('category_id', $id)->where('status',1)
	                           ->select('products.*', 'technicals.name_hi as techmical_hindi','technicals.name_gu as techmical_gujrati' )
	                           ->get();
	                       // dd($solutions);
	                            foreach($solutions as $solution) {
    	                           
    	                         //  print_r($solution->categoryname) ;
    	                        	$attr = Product_attributes::where('product_id',$solution->id)->first();
    	                       // 	dd($attr);
    	                       
    	                            if($attr){
                                        $price= str_replace(",","",$attr->price);
                                        $sale_price= str_replace(",","",$attr->sale_price);
    	                                $solution->price = $price;
    	                                $solution->sale_price = $sale_price;
    	                            }
    	                            if($solution->additional_info) {
    	                                $decode = json_decode($solution->additional_info);
    	                                $solution->additional_info = $decode;
    	                            }else {
    	                                $solution->additional_info = [];
    	                            }
    	                        }
    	                    //  exit;
    	                     $final=array();
    	                   //  dd($request->get('userdetail')->language);
    	                         for($j=0; $j<count($technicals);$j++) {
    	                           //  dd('helo');
    			                    for($i=0; $i<count($solutions);$i++) {
    			                            	                            //  dd('helo');

    			                        //$solutions[$i]->categoryname=$solutions[$i]->categoryname[0]->name;
    			                 
    			                       
    			                         if($request->get('userdetail')->language=="en"){
    			                            if($technicals[$j]->name==$solutions[$i]->technical_name){
    			                                $final=['id'=>$solutions[$i]->id,'name'=>$solutions[$i]->name,'technical_name'=>$solutions[$i]->technical_name,'price'=>$solutions[$i]->price,'additional_info'=>$solutions[$i]->additional_info,'cover'=>$solutions[$i]->cover,'sale_price'=>$solutions[$i]->sale_price,'technical_id'=>$solutions[$i]->technical_id,'category_name'=>$solutions[$i]->categoryname->name,'dealer_name'=>$solutions[$i]->employees->name,'mobile'=>$solutions[$i]->employees->mobile];
    			                                array_push($technical, $final);
    			                            }
    			                         }
    			                         else{
    			                              if($request->get('userdetail')->language=="hi"){
    			                               if($technicals[$j]->name==$solutions[$i]->techmical_hindi){
    			                                        $final=['id'=>$solutions[$i]->id,'name'=>$solutions[$i]->name_hi,'technical_name'=>$solutions[$i]->techmical_hindi,'price'=>$solutions[$i]->price,'additional_info'=>$solutions[$i]->additional_info_hi,'cover'=>$solutions[$i]->cover,'sale_price'=>$solutions[$i]->sale_price,'technical_id'=>$solutions[$i]->technical_id,'category_name'=>$solutions[$i]->categoryname->name_hi,'dealer_name'=>$solutions[$i]->employees->name_hi,'mobile'=>$solutions[$i]->employees->mobile];
            			                                array_push($technical, $final);
    			                              }
    			                            }
    			                            if($request->get('userdetail')->language=="gu"){
    			                             //   dd($solutions[$i]->techmical_gujrati);
    			                                if($technicals[$j]->name==$solutions[$i]->techmical_gujrati){
    			                                        $final=['id'=>$solutions[$i]->id,'name'=>$solutions[$i]->name_gu,'technical_name'=>$solutions[$i]->techmical_gujrati,'price'=>$solutions[$i]->price,'additional_info'=>$solutions[$i]->additional_info_gu,'cover'=>$solutions[$i]->cover,'sale_price'=>$solutions[$i]->sale_price,'technical_id'=>$solutions[$i]->technical_id,'category_name'=>$solutions[$i]->categoryname->name_gu,'dealer_name'=>$solutions[$i]->employees->name_gu,'mobile'=>$solutions[$i]->employees->mobile];
            			                                array_push($technical, $final);
    			                                }
    			                            }
    			                             if($request->get('userdetail')->language==null){
        			                            if($technicals[$j]->name==$solutions[$i]->technical_name){
        			                                $final=['id'=>$solutions[$i]->id,'name'=>$solutions[$i]->name,'technical_name'=>$solutions[$i]->technical_name,'price'=>$solutions[$i]->price,'additional_info'=>$solutions[$i]->additional_info,'cover'=>$solutions[$i]->cover,'sale_price'=>$solutions[$i]->sale_price,'technical_id'=>$solutions[$i]->technical_id,'category_name'=>$solutions[$i]->categoryname->name,'dealer_name'=>$solutions[$i]->employees->name,'mobile'=>$solutions[$i]->employees->mobile];
        			                                array_push($technical, $final);
        			                            }
    			                            }
    			                            
    			                         }
    			                        
    			                    } 
    			                 //   dd($final);
    			                }
    			             
    	                       /*  foreach($solutions as $solution) {
    	                           
    	                            
    	                        	$attr = Product_attributes::where('product_id',$solution->id)->first();
    	                        	
    	                        
    	                            if($attr){
                                        $price= str_replace(",","",$attr->price);
                                        $sale_price= str_replace(",","",$attr->sale_price);
    	                                $solution->price = $price;
    	                                $solution->sale_price = $sale_price;
    	                            }
    	                            if($solution->additional_info) {
    	                                $decode = json_decode($solution->additional_info);
    	                                $solution->additional_info = $decode;
    	                            }else {
    	                                $solution->additional_info = [];
    	                            }
    	                        }
    	                       
    			               for($j=0; $j<count($technicals);$j++) {
    			                    for($i=0; $i<count($solutions);$i++) {
    			                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    			                            array_push($technical, $solutions[$i]);
    			                        }
    			                    }  
    			                }*/
    			                $data = $technical;
	                        }
	                       //  $data = $solutions;
			        	}
		        	}
		      //  	dd($data);
		        	ResponseMessage::successor('Success', $data, $extra);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
	

	public function get_subCategory_product(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
				if(Category::where('id', $id)->where('status',1)->exists()) {
	        		$category = array();
	                $about_product = array();
	                $i=1;
	                $data=[];
                    
	                if(Product::where('subcategory_id', $id)->where('status',1)->exists()){

                		$technicalquery = Technical::query();
	        			$productquery = Product::query();
	                    $productquery->join('categories', 'categories.id', 'products.subcategory_id')
                                ->join('employees', 'employees.id', 'products.company_id')
                                ->leftjoin('technicals',function($join){
                                    $join->on('technicals.name','=','products.technical_name');
                                });
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $technicalquery->select('*','name_hi as name');
                                $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_hi as additional_info', 'employees.name_hi as dealer_name', 'employees.mobile', 'categories.name_hi as category_name','technicals.name_hi as technical_name');
                            } elseif($request->get('userdetail')->language=="gu"){
                                $technicalquery->select('*','name_gu as name');
                                $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info_gu as additional_info', 'employees.name_gu as dealer_name', 'employees.mobile', 'categories.name_gu as category_name','technicals.name_gu as technical_name');
                            } else{
                                $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                            }
                        } else{
                            $productquery->select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name');
                        }
                    $solutions = $productquery->where('products.subcategory_id', $id)->where('products.status',1)->where('employees.status',1)->where('categories.status',1)->get();
                    $technicals = $technicalquery->get();
						

	                    /*$solutions = Product::join('categories', 'categories.id', 'products.subcategory_id')
	                                        ->join('employees', 'employees.id', 'products.company_id')
	                                        ->Select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name')
	                                        ->where('products.subcategory_id', $id)
	                                        ->where('products.status',1)
	                                        ->where('employees.status',1)
	                                        ->where('categories.status',1)
	                                        ->get();*/
                        $technical = array();
                        if(count($solutions)>0){
                            foreach($solutions as $solution) {
                            	$attr = Product_attributes::where('product_id',$solution->id)->first();
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $solution->price = $price;
                                    $solution->sale_price = $sale_price;
                                }
                                if($solution->additional_info) {
                                    $decode = json_decode($solution->additional_info);
                                    $solution->additional_info = $decode;
                                }else {
                                    $solution->additional_info = [];
                                }
                            }
    		                for($j=0; $j<count($technicals);$j++) {
    		                    for($i=0; $i<count($solutions);$i++) {
    		                        if($technicals[$j]->name==$solutions[$i]->technical_name){
    		                            array_push($technical, $solutions[$i]);
    		                        }
    		                    }  
    		                }
    	                    $data = $technical;
	                    }
		        	}
		        	ResponseMessage::success('Success', $data);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
		} catch (Exception $e) {
			log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
		}
	}
}