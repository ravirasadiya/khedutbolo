<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RestAPIs\v1\customer\Controllers;
use Log;
use Validator;
use DB;
use App\Helper\ResponseMessage;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\OrderProducts\OrderProduct;
use App\Shop\Carts\Cart;
use App\Shop\Products\Product_attributes;
use App\Shop\Employees\Employee;
use App\Shop\TechnicalCategories\Technical;

class ProductController extends Controller
{
	Public function productDetail(Request $request){
		try {
    		$rules = [
    			'id' => 'required',
    			'customer_id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter product id.',
    			'customer_id.required' => 'Please enter customer id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
	        	$customer_id = trim(strip_tags($request->customer_id));
	        	if(Product::where('id', $id)->where('status',1)->exists()){

                        $productquery = Product::query();
                        $productquery->join('employees', 'employees.id', 'products.company_id');
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $productquery->select('products.id', 'products.name_hi as name', 'employees.name_hi as company_name', 'employees.mobile', 'products.status', 'products.weight', 'products.shipping_price', 'products.cover', 'products.additional_info_hi as additional_info');
                            } elseif($request->get('userdetail')->language=="gu"){
                                $productquery->select('products.id', 'products.name_gu as name', 'employees.name_gu as company_name', 'employees.mobile', 'products.status', 'products.weight', 'products.shipping_price', 'products.cover', 'products.additional_info_gu as additional_info');
                            } else{
                                $productquery->select('products.id', 'products.name', 'employees.name as company_name', 'employees.mobile', 'products.status', 'products.shipping_price', 'products.shipping_price', 'products.weight', 'products.cover', 'products.additional_info');
                            }
                        } else{
                            $productquery->select('products.id', 'products.name', 'employees.name as company_name', 'employees.mobile', 'products.status', 'products.shipping_price', 'products.weight', 'products.cover', 'products.additional_info');
                        }
                        $product = $productquery->where('products.id', $id)->where('products.status',1)->where('employees.status',1)->first();
                    $finoprofd=array();
                    if($product){
                        if(Product_attributes::where('product_id',$id)->exists()){
                            $infos = Product_attributes::select('key','value','quantity','gst_price', 'price','sale_price', 'available_quantity','id')->where('product_id',$id)->get();
                             foreach($infos as $key=>$inf){
                                 $finoprofd[$key]=['key'=>$inf->key,'value'=>$inf->value,'quantity'=>$inf->quantity,'price'=>str_replace(",","",$inf->price),'sale_price'=>str_replace(",","",$inf->sale_price),'available_quantity'=>$inf->available_quantity,'id'=>$inf->id];
                             }
                          //  $product->unit = $finoprofd;
                            $user_quantity=0;
                            foreach ($infos as $key=> $info) {
                                $i=0;
                               // $infos[$i]->price= str_replace(",","",$info->price);
                               // $infos->price= str_replace(",","",$info->price);
                                $infos->price= str_replace(",","",$info->price);
                                $sale_price = str_replace(",","",$info->sale_price);
                                $gst_price = $sale_price*$infos[$i]->gst_price/100;
                                
                                $infos->sale_price= $sale_price+$gst_price;
                                
                                if(Cart::where('customer_id', $customer_id)->where('product_id', $id)->exists()){
                                    $carts = Cart::where('customer_id', $customer_id)
                                                ->where('product_id', $id)
                                                ->get();
                                    foreach ($carts as $cart) {
                                        if($info->id == $cart->attribute_id){
                                            $user_quantity = $cart->quantity;
                                        } else {
                                            $user_quantity = 0;
                                        }
                                    }
                                } else {
                                    $user_quantity = 0;
                                }
                                $finoprofd[$key]=['key'=>$info->key,'value'=>$info->value,'quantity'=>$info->quantity,'price'=>str_replace(",","",$info->price),'sale_price'=>str_replace(",","",$info->sale_price),'available_quantity'=>$info->available_quantity,'id'=>$info->id,'user_quantity'=>$user_quantity];
                              $product->unit = $finoprofd;
                               //$solution->unit=$testarr;
                                $info->user_quantity=$user_quantity;
                                $i++;
                            }
                        } else {
                            ResponseMessage::error('Product attribute not found.');
                        }
                        if($product->additional_info) {
        	        		$decode = json_decode($product->additional_info);
        	        		$product->additional_info = $decode;
        	        		foreach($product->additional_info as $info){
            	        		if(!$info->key){
            	        		    $product->additional_info = [];
            	        		}
        	        		}
                        }else {
                            $product->additional_info = [];
                        }
                        if(DB::table('customers')->where('id',$customer_id)->where('language','hi')->exists()){
                            $offer_product = Product::select('offer_title_hi as offer_title','offer_product_hi as offer_product','offer_quantity','offer_product_image')->where('id', $id)->first();
                            if($offer_product->offer_title) {
                                $product['offer'] = $offer_product;
                            } else {
                                $product['offer'] = null;
                            }
                        } elseif(DB::table('customers')->where('id',$customer_id)->where('language','gu')->exists()){
                            $offer_product = Product::select('offer_title_gu as offer_title','offer_product_gu as offer_product','offer_quantity','offer_product_image')->where('id', $id)->first();
                            if($offer_product->offer_title) {
                                $product['offer'] = $offer_product;
                            } else {
                                $product['offer'] = null;
                            }
                        }else{
                            $offer_product = Product::select('offer_title','offer_product','offer_quantity','offer_product_image')->where('id', $id)->first();    
                            if($offer_product->offer_title) {
                                $product['offer'] = $offer_product;
                            } else {
                                $product['offer'] = null;
                            }
                        }
                        
                        
                    } else {
                        $product=[];
                    }
	        	//	ResponseMessage::success('Success', $product);
	        	 $data=array();
    		   $data['status']=200;
    		   $data['message']='Success';
    		   $data['data']=$product;
    		   $data['mobile']=env('COMPANY_MOBNO');
	return json_encode($data);
	        	} else {
	        		ResponseMessage::error('Product not found.');
	        	}
	        }
    	} catch (Exception $e) {
    		log::error($e);
    	}
	}

//     Public function productDetail(Request $request){
// 		try {
//     		$rules = [
//     			'id' => 'required',
//     			'customer_id' => 'required',
//     		];
//     		$customeMessage = [
//     			'id.required' => 'Please enter product id.',
//     			'customer_id.required' => 'Please enter customer id.',
//     		];
//     		$validator = Validator::make($request->all(),$rules, $customeMessage);

//             if( $validator->fails() ) {
// 	            $errors = $validator->errors();
// 				ResponseMessage::error($errors->first());
// 	        } else {
// 	        	$id = trim(strip_tags($request->id));
// 	        	$customer_id = trim(strip_tags($request->customer_id));
// 	        	if(Product::where('id', $id)->exists()){
// 	        		$product = Product::join('employees', 'employees.id', 'products.company_id')
//                                         ->Select('products.id', 'products.name', 'employees.name as company_name', 'employees.mobile as mobile', 'products.status', 'products.weight', 'products.shipping_price', 'products.cover', 'products.additional_info')
//                                         ->where('products.id', $id)
//                                         ->where('products.status',1)
//                                         ->first();
//                     $infos = Product_attributes::select('key','value','quantity', 'price','sale_price', 'available_quantity','id')->where('product_id',$id)->get();
//                     $product->unit = $infos;
//                     $user_quantity=0;
//                     foreach ($infos as $info) {
//                         $i=0;
//                         if(Cart::where('customer_id', $customer_id)->where('product_id', $id)->exists()){
//                             $carts = Cart::where('customer_id', $customer_id)
//                                         ->where('product_id', $id)
//                                         ->get();
//                             foreach ($carts as $cart) {
//                                 if($info->id == $cart->attribute_id){
//                                     $user_quantity = $cart->quantity;
//                                 } else {
//                                     $user_quantity = 0;
//                                 }
//                             }
//                         } else {
//                             $user_quantity = 0;
//                         }
//                         $info->user_quantity=$user_quantity;
//                         $i++;
//                     }
//                     if($product->additional_info) {
//     	        		$decode = json_decode($product->additional_info);
//     	        		$product->additional_info = $decode;
//     	        		foreach($product->additional_info as $info){
//         	        		if(!$info->key){
//         	        		    $product->additional_info = [];
//         	        		}
//     	        		}
//                     }else {
//                         $product->additional_info = [];
//                     }
                    
//                     $offer_product = Product::select('offer_title','offer_product','offer_quantity','offer_product_image')->where('id', $id)->first();
//                     if($offer_product->offer_title) {
//                         $product['offer'] = $offer_product;
//                     } else {
//                         $product['offer'] = null;
//                     }
// 	        		ResponseMessage::success('Success', $product);
// 	        	} else {
// 	        		ResponseMessage::error('Product not found.');
// 	        	}
// 	        }
//     	} catch (Exception $e) {
//     		log::error($e);
//     	}
// 	}
	
	public function productAttribute(Request $request){
        try {
            $rules = [
                'product_id' => 'required',
                'attribute_id' => 'required',
                'customer_id' => 'required',
            ];
            $customeMessage = [
                'product_id.required' => 'Please enter product id.',
                'attribute_id.required' => 'Please enter attribute id.',
                'customer_id.required' => 'Please enter customer id.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $product_id = trim(strip_tags($request->product_id));
                $attribute_id = trim(strip_tags($request->attribute_id));
                $customer_id = trim(strip_tags($request->customer_id));
                if(Product::where('id', $product_id)->where('status',1)->exists()){
                    if (Product_attributes::where('product_id',$product_id)->where('id',$attribute_id)->exists()){
                        $attribute = Product_attributes::where('product_id',$product_id)->where('id',$attribute_id)->first();
                        $attribute->price = str_replace(",","",$attribute->price);
                        $attribute->sale_price = str_replace(",","",$attribute->sale_price);
                        if($attribute){
                            if(Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->exists()) 
                            {
                                $cart = Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->first();
                                $attribute->user_quantity = $cart->quantity;
                            } else {
                                $attribute->user_quantity = 0;
                            }
                        } else {
                            $attribute=[];
                        }
                        ResponseMessage::success('Success', $attribute);
                    } else {
                        ResponseMessage::error('Product attribute not found.');
                    }
                } else {
                    ResponseMessage::error('Product not found.');
                }    
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function similarProducts(Request $request){
    	try {
    		$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter product id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$products = array();
	        	$i=1;
	        	$id = trim(strip_tags($request->id));
	        	if(Product::where('id', $id)->where('status',1)->exists()){
	        		$product = Product::where('id', $id)->where('status',1)->first();
	        		if(Product::where('subcategory_id', $product->subcategory_id)->where('status',1)->exists()){

                        $technicalquery = Technical::query();
                        $productquery = Product::query();
                        $productquery->join('categories', 'categories.id', 'products.subcategory_id')
                                ->join('employees', 'employees.id', 'products.company_id')
                                ->leftjoin('technicals',function($join){
                                    $join->on('technicals.name','=','products.technical_name');
                                });
                               
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                $technicalquery->select('*','name_hi as name');
                               
                                $productquery->select('products.id','products.company_id', 'products.name_hi as name','technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_hi as product_dealer');
                            } elseif($request->get('userdetail')->language=="gu"){
                                //  dd($technicalquery);
                                $technicalquery->select('*','name_gu as name');
                                $productquery->select('products.id','products.company_id', 'products.name_gu as name', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_gu as product_dealer');
                            } else{
                                $productquery->select('products.id','products.company_id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as product_dealer');
                            }
                        } else{
                            $productquery->select('products.id','products.company_id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as product_dealer');
                        }
                        $similarProducts = $productquery->where('products.subcategory_id', $product->subcategory_id)->where('products.id', '!=', $product->id)->where('products.technical_id',$request->technical_id)->where('products.status', 1)->where('employees.status', 1)->get();
                        $technicals = $technicalquery->get();
                        if(count($similarProducts)>0){
    	        			foreach ($similarProducts as $similarProduct) {
    	        			    $attr = Product_attributes::where('product_id',$similarProduct->id)->first();
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $similarProduct->price = $price;
                                    $similarProduct->sale_price = $sale_price;
                                }
    	        				array_push($products, $similarProduct);
    	        			}
                        }
	        			ResponseMessage::success('Success', $products);
	        		} else {
	        			ResponseMessage::success('Similar product not found.');
	        		}
	        	} else {
	        		ResponseMessage::error('Product not found.');
	        	}
	        }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }


    public function companyProducts(Request $request){
    	try {
    		$rules = [
    // 			'company_id' => 'required',
    		];
    		$customeMessage = [
    // 			'company_id.required' => 'Please enter company id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$company_id = trim(strip_tags($request->company_id));
                $data=[];
		  //  	if(Employee::where('id', $company_id)->where('status',1)->exists()){
    	        	if(Product::where('status',1)->where('products.company_id',$company_id)->exists()){
    	        							
                        $technicalquery = Category::query();
                        $technicalquery->where('parent_id',null)->where('status',1);
                        $productquery = Product::query();
                        $productquery->join('categories', 'categories.id', 'products.category_id')
                                    ->join('employees', 'employees.id', 'products.company_id');
                            if($request->get('userdetail')){
                                if($request->get('userdetail')->language=="hi"){
                                    $technicalquery->select('*','name_hi as name');
                                    $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover',  'categories.name_hi as category_name' ,'employees.name_gu as company_name');
                                } elseif($request->get('userdetail')->language=="gu"){
                                    $technicalquery->select('*','name_gu as name');
                                    $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_gu as company_name', 'categories.name_gu as category_name');
                                } else{
                                    $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as company_name', 'categories.name as category_name');
                                }
                            } else{
                                $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as company_name', 'employees.mobile', 'categories.name as category_name');
                            }
                        $products = $productquery->where('products.company_id',$company_id)->where('products.status',1)
                                            ->where('categories.status',1)
                                            ->get();
                                            // dd($products);
                        $technicals = $technicalquery->get();
    	        		if(count($products)>0){
        	        		foreach ($products as $prod) {
                                $attr = Product_attributes::where('product_id',$prod->id)->first();
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $prod->price = $price;
                                    $prod->sale_price = $sale_price;
                                }
                            }
                            for($j=0; $j<count($technicals);$j++) {
                                for($i=0; $i<count($products);$i++) {
                                    if($technicals[$j]->name==$products[$i]->category_name){
                                        array_push($data, $products[$i]);
                                    }
                                }  
                            }
    	        		}
    	        		ResponseMessage::success('Success', $data);
    	        	} else {
    	        		ResponseMessage::error('Product not found.');
    	        	}
		  //  	} else {
    //                 ResponseMessage::error('Company not found.');
    //             }
	        }
	    } catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function traddingProducts(Request $request){
    	try {
        	if(OrderProduct::exists()){
        		$product_detail = array();
        		$product_ids = OrderProduct::select('product_id')->distinct()->get();
        		foreach ($product_ids as $product_id) {
        			$count = OrderProduct::where('product_id', $product_id->product_id)->count();
        			$quantities = OrderProduct::select('quantity')->where('product_id', $product_id->product_id)->get();
        			$sale_quantity=0;
        			foreach ($quantities as $quantity) {
        				$sale_quantity = $sale_quantity+$quantity->quantity;
        			}
        			$data['sale_quantity'] = $sale_quantity;
        			$data['id ']= $product_id->product_id;
        			$data['order_product'] = $count;

                    $productquery = Product::query();
                    $productquery->leftjoin('categories', 'categories.id', 'products.subcategory_id')
                                    ->join('employees', 'employees.id', 'products.company_id');
                            if($request->get('userdetail')){
                                if($request->get('userdetail')->language=="hi"){
                                    $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_hi as company_name', 'categories.name_hi as category_name');
                                } elseif($request->get('userdetail')->language=="gu"){
                                    $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_gu as company_name','categories.name_gu as category_name');
                                } else{
                                    $productquery->select('products.id', 'products.name',  'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as company_name', 'employees.mobile', 'categories.name as category_name');
                                }
                            } else{
                                $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as company_name', 'categories.name as category_name');
                            }
                        $data['product'] = $productquery->where('products.id', $product_id->product_id)
                                    ->where('products.status',1)
                                    ->where('employees.status',1)
                                    ->get();
        			array_push($product_detail, $data);
        		 }
        		rsort($product_detail);
        		ResponseMessage::success('Success', $product_detail);
        	} else {
        		ResponseMessage::error('Product not found.');
        	}
	    } catch (Exception $e) {
    		log::error($e);
    	}
    }

}
