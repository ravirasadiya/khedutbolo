<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use Log;
use Validator;
use App\Helper\MailHelper;
use App\Helper\ResponseMessage;
use \stdClass;
use App\Shop\Products\Product_attributes;
use App\Shop\TechnicalCategories\Technical;
use App\Shop\Crops\Crop;

class FilterController extends Controller
{
    // filter API get products from technical id and company id 
    // date: 15/05/2020
    // developer name: Shikha
    public function filter(Request $request) 
    {
        try {
            $data = array();
            $prod=[];
            $technicals = $request->technicals;
            $companies = $request->companies;
            $solution_id = ($request->solution_id)?$request->solution_id:array();
            
            $subsubSolution = $request->subsubSolution_id;
            if(count($companies)==0){
                $companies = array(
                    "0" => null ,
                );
            }
            if(!empty($subsubSolution)){
                if(count($subsubSolution)==0){
                    $subsubSolution = array(
                        "0" => null ,
                    );
                }
          
                 $crop = Crop::where('id',$subsubSolution[0])->where('crop_sub',$solution_id)->first();
                // print_r($crop);
                // exit;
            }
            else{
               
                 $crop = Crop::where('id',$solution_id)->first();
            }
           // echo "<br>=---->>".$subsubSolution;
             //   exit;
           /* if(count($subsubSolution)==0){
                $subsubSolution = array(
                    "0" => null ,
                );
            }*/
            if($companies && $technicals) {
               
                for($i=0;$i<count($companies);$i++){
                    for($j=0;$j<count($technicals);$j++){
                        $id=$companies[$i];
                       
                        
                       
                        $technical = $technicals[$j];
                       
                        $product_ids = json_decode($crop->product_id);
                      
                        foreach ($product_ids as $key => $product_id) {
                        
                            $productquery = Product::query();
                            $productquery->join('employees','employees.id','products.company_id')
                                                ->join('categories','categories.id','products.category_id')
                                                ->join('technicals','technicals.name','products.technical_name');
                            if($request->get('userdetail')){
                                if($request->get('userdetail')->language=="hi"){
                                    //$productquery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi  as additional_info', 'products.cover', 'categories.name_hi as category_name');
                                
                                    $productquery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name_hi as category_name');
                                } elseif($request->get('userdetail')->language=="gu"){
                                    $productquery->select('products.id', 'products.name_gu as name', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name_gu as category_name');
                                    //$productquery->select('products.id', 'products.name_gu as name', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_gu  as additional_info', 'products.cover', 'categories.name_gu as category_name');
                                } else{
                                    $productquery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name');
                                }
                            } else{
                                $productquery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name');
                            }
                            // dd($id);
                            // dd($productquery->orWhere('products.company_id', $id)->get());
                       // echo count($companies);
                        

                                $technical_prod = $productquery->Where(['products.company_id'=>$id,'products.technical_id'=>$technical])
                                                //->orWhere('products.technical_id',$technical)
                                                ->orwhere('products.id',$product_id);
                                                // ->whereIn('crop_id',$solution_id)
                   
                            $technical_prod =   $technical_prod->where('products.status',1)
                                                ->where('employees.status',1)
                                                ->where('categories.status',1)
                                                ->get(); 

                            foreach ($technical_prod as $com) {
                                $attr = Product_attributes::where('product_id',$com->id)->first();
                                if($attr){
                                    $price= str_replace(",","",$attr->price);
                                    $sale_price= str_replace(",","",$attr->sale_price);
                                    $com->price = $price;
                                    $com->sale_price = $sale_price;
                                }
                                array_push($prod, $com);
                            }
                           
                        }
                    }
                   
                }
                for($j=0; $j<count($technicals);$j++) {
                    for($i=0; $i<count($prod);$i++) {
                        $technicalquery = Technical::query();
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="gu"){
                                $technicalquery->where("name_gu",$prod[$i]->technical_name);
                            }else if($request->get('userdetail')->language=="hi"){
                                $technicalquery->where("name_hi",$prod[$i]->technical_name);
                            }else{
                                $technicalquery->where("name",$prod[$i]->technical_name);
                            }
                        }
                        $tech = $technicalquery->first();
                        if($technicals[$j]==$tech->id){
                            array_push($data, $prod[$i]);
                        }
                    }  
                }
               
                
            } else {
                if($companies) {
                 
                   $getarra= str_replace("[","",$crop->product_id);
				$finalrar= str_replace("]","",$getarra);
		
                    foreach($companies as $company){
                        
                        
                        $id = $company;
                        $employeequery = Product::query();
                        $employeequery->join('employees','employees.id','products.company_id')
                                            ->join('categories','categories.id','products.category_id')
                                            ->join('technicals','technicals.name','products.technical_name');
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                                //$employeequery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name_hi as category_name');
                                $employeequery->select('products.id', 'products.name_hi as name','products.company_id', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name_hi as category_name');
                            } elseif($request->get('userdetail')->language=="gu"){
                               // $employeequery->select('products.id', 'products.name_gu as name', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name_gu as category_name');
                            $employeequery->select('products.id', 'products.name_gu as name','products.company_id', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name_gu as category_name');
                                // $company_prod = $employeequery->where('products.company_id', $id)
                                //         ->whereIn('crop_id',$solution_id)
                                //         ->where('products.status',1)
                                //         ->where('employees.status',1)
                                //         ->where('categories.status',1)
                                //         ->get();
                            } else{
                                //$employeequery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name as category_name');
                            
                                 $employeequery->select('products.id', 'products.name','products.company_id', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name');
                            }
                        } else{
                           // $employeequery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi', 'products.cover', 'categories.name as category_name');
                        
                             $employeequery->select('products.id', 'products.name','products.company_id', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name');
                        }
                      
                        $company_prod = $employeequery->where('products.company_id', $id)
                                        ->where('products.status',1)
                                        ->where('employees.status',1)
                                        ->where('categories.status',1)
                                        ->whereIn('products.id',explode(",",$finalrar))
                                        ->get();
                      
                        foreach ($company_prod as $com) {
                           // if($com->company_id==$id){
                                 $attr = Product_attributes::where('product_id',$com->id)->first();
                            if($attr){
                                $price= str_replace(",","",$attr->price);
                                $sale_price= str_replace(",","",$attr->sale_price);
                                $com->price = $price;
                                $com->sale_price = $sale_price;
                            }
                            array_push($data, $com);
                           // }
                           
                        }
                    }
                } else {
                    
                    foreach($technicals as $technical){
                        $technical = $technical;
                        $productquery = Product::query();
                        $productquery->join('employees', 'employees.id', 'products.company_id')
                                     ->join('categories', 'categories.id', 'products.category_id')
                                     ->join('technicals', 'technicals.name','products.technical_name');
                        if($request->get('userdetail')){
                            if($request->get('userdetail')->language=="hi"){
                               // $productquery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi as additional_info', 'products.cover', 'categories.name_hi as category_name');
                          $productquery->select('products.id', 'products.name_hi as name', 'employees.name_hi as dealer_name', 'employees.mobile', 'technicals.name_hi as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name_hi as category_name');
                            }else if($request->get('userdetail')->language=="gu"){
                                //$productquery->select('products.id', 'products.name_gu as name', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info_hi as additional_info', 'products.cover', 'categories.name_gu as category_name');
                           $productquery->select('products.id', 'products.name_gu as name', 'employees.name_gu as dealer_name', 'employees.mobile', 'technicals.name_gu as technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name_gu as category_name');
                            }else{
                                $productquery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info as additional_info', 'products.cover', 'categories.name as category_name');
                            }
                        } else{
                            $productquery->select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'technicals.name as technical_name', 'products.price', 'products.sale_price', 'products.additional_info as additional_info', 'products.cover', 'categories.name as category_name');
                        }

                        $technical_prod = $productquery->where('products.status',1)
                                        ->whereIn('crop_id',$solution_id)
                                        ->where('employees.status',1)
                                        ->where('categories.status',1)
                                        ->where('technicals.id',$technical)
                                        ->get();
                        $price=null; 
                        $sale_price =null;             
                        foreach ($technical_prod as $com) {
                            $attr = Product_attributes::where('product_id',$com->id)->first();
                            if($attr){
                                $price= str_replace(",","",$attr->price);
                                $sale_price= str_replace(",","",$attr->sale_price);
                                $com->price = $price;
                                $com->sale_price = $sale_price;
                            }
                            array_push($data, $com);
                        }
                    }
                }
            }
            // $filter = array();
            $filter = array_unique($data);
            $filter = array_merge($filter);
            // foreach ($data as $key => $item) {
            //     dd($filter[$item['id']][$key]);
            //     $filter[]
            //   $filter[$item['id']][$key] = $item;
            // }
            // ksort($filter, SORT_NUMERIC);
            ResponseMessage::success('Success', $filter);
        } catch (Exception $e) {
            log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }

    public function search(Request $request){
        try {
            $rules = [
                'value' => 'required',
            ];
            $customeMessage = [
                'value.required' => 'Please enter search value.',
            ];$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $value = trim(strip_tags($request->value));
                $productquery = Product::query();
              //  $productquery->join('categories', 'categories.id', 'products.subcategory_id')
              //$productquery->join('categories', 'categories.id', 'products.subcategory_id')
                                    $productquery->join('employees', 'employees.id', 'products.company_id');
                                    // ->leftjoin('technicals',function($join){
                                    //     $join->on('technicals.name','=','products.technical_name');
                                    // });
                if($request->get('userdetail')){
                    if($request->get('userdetail')->language=="hi"){
                        $productquery->select('products.id', 'products.name_hi as name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_hi as dealer_name');
                        
                        $products = $productquery->where('products.name_hi', 'like','%'.$value.'%')
                                    ->orWhere('products.technical_name', 'like','%'.$value.'%')
                                    ->orWhere('employees.name', 'like','%'.$value.'%')
                                    ->where('employees.status',1)->where('products.status',1)->get();
                    } elseif($request->get('userdetail')->language=="gu"){
                        
                        $productquery->select('products.id', 'products.name_gu as name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name_gu as dealer_name');
                    
                        $products = $productquery->where('products.name_gu', 'like','%'.$value.'%')
                                    ->orWhere('products.technical_name', 'like','%'.$value.'%')
                                    ->orWhere('employees.name', 'like','%'.$value.'%')
                                    ->where('employees.status',1)->where('products.status',1)->get();
                        
                    } else{
                        $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as dealer_name');
                    
                        
                        $products = $productquery->where('products.name', 'like','%'.$value.'%')
                                    ->orWhere('products.technical_name', 'like','%'.$value.'%')
                                    ->orWhere('employees.name', 'like','%'.$value.'%')
                                    ->where('employees.status',1)->where('products.status',1)->get();
                    }
                } else{
                    $productquery->select('products.id', 'products.name', 'products.price', 'products.sale_price', 'products.cover', 'employees.name as dealer_name');
                
                    $products = $productquery->where('products.name', 'like','%'.$value.'%')
                                    ->orWhere('products.technical_name', 'like','%'.$value.'%')
                                    ->orWhere('employees.name', 'like','%'.$value.'%')
                                    ->where('employees.status',1)->where('products.status',1)->get();
                }
                // dd($products);
                // $products = $productquery->where('products.name', 'like','%'.$value.'%')
                //                     ->orWhere('products.technical_name', 'like','%'.$value.'%')
                //                     ->orWhere('employees.name', 'like','%'.$value.'%')
                //                     ->where('employees.status',1)->where('products.status',1)->get();

                if(count($products)>0){
                    foreach($products as $solution) {
                        $attr = Product_attributes::where('product_id',$solution->id)->first();
                        if($attr){
                            $price= str_replace(",","",$attr->price);
                            $sale_price= str_replace(",","",$attr->sale_price);
                            $solution->price = $price;
                            $solution->sale_price = $sale_price;
                        }
                    }
                    ResponseMessage::success('Success', $products);
                } else {
                    ResponseMessage::error('Product not found');
                }
            }
        } catch (Exception $e) {
            log::error($e);
			$data['error']=$e;
			$error_mail = MailHelper::errorMail($data);
        }
    }
}
