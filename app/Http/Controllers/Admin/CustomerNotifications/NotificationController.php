<?php

namespace App\Http\Controllers\Admin\CustomerNotifications;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Employees\Employee;
use App\Shop\Addresses\Address;
use App\Shop\Customers\Customer;
use App\Shop\States\State;
use App\Shop\Cities\City;
use App\Shop\CustomerNotifications\CustomerNotification;
use Log;
use Validator;
use App\Helper\Permission;
use App\Shop\CountriesData\CountryData;
use App\Helper\NotificationHelper;
use App\Shop\Notifications\Notification;

class NotificationController extends Controller
{
    public function index() {
        $permission = Permission::permission('notification');
        if($permission->view==1) {
        	$notifications = CustomerNotification::get();
        	return view('admin.customer-notifications.list', ['notifications' => $notifications, 'permission' => $permission]);
        } else {
            return view('layouts.errors.403');
        }
    }

    public function create() {
        $permission = Permission::permission('notification');
        if($permission->add==1) {
        	$companies = Employee::where('status',1)->get();
        	return view('admin.customer-notifications.create', ['companies' => $companies]);
        } else {
            return view('layouts.errors.403');
        }
    }

    public function edit($id)
    {
        $permission = Permission::permission('notification');
        if($permission->edit==1) {
    		$notification = CustomerNotification::find($id);
    		$companies = Employee::get();
            $data=[];
            $value=[];
            $state=[];
            $city=[];
            if($notification->customer_type=='single_customer') {
                $customers = Employee::join('products', 'products.company_id', 'employees.id')
                                ->join('order_product', 'order_product.product_id', 'products.id')
                                ->join('orders', 'orders.id', 'order_product.order_id')
                                ->join('customers', 'customers.id', 'orders.customer_id')
                                ->select('customers.id', 'customers.name')
                                ->where('employees.id', $notification->company_id)
                                ->where('employees.status',1)
                                ->distinct()
                                ->get();
                foreach( $customers as $customer )
                {
                    $data[$customer->id] = $customer->name;
                }
            }
            if($notification->customer_type=='state_customer' || $notification->customer_type=='city_customer' || $notification->customer_type=='tehsil_customer' || $notification->customer_type=='village_customer') {
                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();
                foreach( $states as $state )
                {
                    $data[$state->STCode] = $state->DTName;
                }
                if($notification->customer_type!='state_customer'){
                    $value['state'] = $data;
                }
            }
            if($notification->customer_type=='city_customer' || $notification->customer_type=='tehsil_customer' || $notification->customer_type=='village_customer') {
                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $notification->state_id)->orderBy('DTName', 'ASC')->get();
                $data=[];
                foreach( $cities as $city )
                {
                    $data[$city->DTCode] = $city->DTName;
                }
                if($notification->customer_type!='city_customer'){
                    $value['city'] = $data;
                }
            }
            if($notification->customer_type=='tehsil_customer' || $notification->customer_type=='village_customer') {
                $data=[];
                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$notification->city_id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
                foreach( $tehsils as $tehsil )
                {
                    $data[$tehsil->SDTCode] = $tehsil->SDTName;
                }
                if($notification->customer_type!='tehsil_customer'){
                    $value['tehsil'] = $data;
                }
            }
            if($notification->customer_type=='village_customer') {
                $data=[];
                $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                foreach( $villages as $village )
                {
                    $data[$village->TVCode] = $village->Name;
                }
            }
    		return view('admin.customer-notifications.create', [
    			'companies' => $companies,
    			'notification' => $notification,
                'data' => $data, 
                'values' => $value
    		]);
        } else {
            return view('layouts.errors.403');
        }
    }

    public function store(Request $request) {
        $rules = [
            'customer_type' => 'required',
            'msg'  => 'required',
        ];
        $customeMessage = [
            'customer_type.required' => 'Please enter customer type.',
            'msg.required' => 'Please enter message',
        ];
        $validator = Validator::make($request->all(),$rules, $customeMessage);
        if( $validator->fails() ) {
             return back()->withInput()->withErrors($validator->errors());
        } else {
            $customer_type = $request->customer_type;
            if($request->lang=="Hindi"){
                $msg = $request->msg_hi;
            } else if($request->lang=="Gujarati"){
                $msg = $request->msg_gu;
            } else{
                $msg = $request->msg;
            }
            if($customer_type=='single_customer' || $customer_type=='all_customer'){
                $query = Customer::select('customers.token','customers.id')->where('status',1);
                if($customer_type=='single_customer') {
                   $query = $query->where('id',$request->states);
                }
            } else {
                $query =Customer::join('addresses','addresses.customer_id','customers.id')
                                ->select('customers.token','customers.id')
                                ->where('customers.status',1);
                if($customer_type=='state_customer' || $customer_type=='city_customer' || $customer_type=='tehsil_customer' || $customer_type=='village_customer') {
                    $query = $query->where('state_code',$request->states);
                } else if($customer_type=='city_customer' || $customer_type=='tehsil_customer' || $customer_type=='village_customer') {
                    $query = $query->where('city',$request->cities);
                } else if($customer_type=='tehsil_customer' || $customer_type=='village_customer') {
                    $query = $query->where('tehsil_id',$request->tehsil);
                } else {
                    $query = $query->where('village_id', $request->village);
                }
            }
            $customers = $query->distinct()->get();
            if(count($customers)>0){
                foreach($customers as $customer){
                    $server_api_key = 'AAAABWo6xZM:APA91bFjaUiBMsm9HfzFioojNYA8_14Fr4mqcCKtZXlWWNGMVnuynYTCuwu9kphKIfPec3GuJv-Qx2HatvX9bR4PrM0YUTHvrrApKR9RYzflk7ogMjH3LaTuNYvJ3T05tHKMA5CHMiuY';
                    // $server_api_key = 'AAAAr2mz3qw:APA91bHreE9QcwXtIMa221zDANf-Nt1lpFHwNB7u1T3E2qVqFVPYbGt_NRBQT-khwCiZsHQcgaRND4uNNhRVDswAhH9SRorI19s0e-uLpsywUL1YCU0_E7Rm1HFkJ54iWwwcNiOmn9mS';
                    $title = 'Notification ';
                    $token = $customer->token;
                    $message = $msg;
                    $notify = new Notification();
                    $notify->notify_detail = $message;
                    $notify->customer_id = $customer->id;
                    date_default_timezone_set('Asia/Kolkata');
                    $notify->time = date('h:i:s');
                    $notify->save();
                    $result = NotificationHelper::notification($message, $server_api_key, $token, $title);
                    $result = json_decode($result);
                }
                if(isset($result->results[0]->message_id)){
                    return redirect()->route('admin.notifications.create')->with('message', 'Notification send successfully');
                }else{
                    return redirect()->route('admin.notifications.create')->with('error', 'Notification not send');
                }
            } else {
                return redirect()->route('admin.notifications.create')->with('message', 'Customer not found!!');
            }
        }
    }


    public function destroy($id) {
        $permission = Permission::permission('notification');
        if($permission->delete==1) {
        	if(CustomerNotification::where('id', $id)->exists()) {
    	    	$notification = CustomerNotification::find($id);
    	    	if($notification->delete()) {
    	    		return redirect()->route('admin.notifications.index')->with('message', 'Notification delete successfully.');
    	    	}
    	    }
        } else {
            return view('layouts.errors.403');
        }
    }

    public function getValue(Request $request) {
        $data = [];
        if($request->value=='single_customer') {
           $customers = Customer::select('customers.id', 'customers.name')->where('status',1)->get();
            foreach( $customers as $customer )
            {
                $data[$customer->id] = $customer->name;
            }
        }else if($request->value=='state_customer') {
            $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->get();
            foreach( $states as $state )
            {
                $data[$state->STCode] = $state->DTName;
            }
        }else if($request->value=='city_customer') {
            $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->orderBy('DTName', 'ASC')->where('STCode', 'like', '%' . $request->state)->get();
            foreach( $cities as $city )
            {
                $data[$city->DTCode] = $city->DTName;
            }
        }else if($request->value=='tehsil_customer') {
            $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$request->city)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
            foreach( $tehsils as $tehsil )
            {
                $data[$tehsil->SDTCode] = $tehsil->SDTName;
            }
        }else if($request->value=='village_customer') {
            $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$request->tehsil)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
            foreach( $villages as $village )
            {
                $data[$village->TVCode] = $village->Name;
            }
        }
        return $data;
    }
}
