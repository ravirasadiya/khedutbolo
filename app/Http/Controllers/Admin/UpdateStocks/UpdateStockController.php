<?php

namespace App\Http\Controllers\Admin\UpdateStocks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Attributes\Repositories\AttributeRepositoryInterface;
use App\Shop\AttributeValues\Repositories\AttributeValueRepositoryInterface;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\Products\Exceptions\ProductInvalidArgumentException;
use App\Shop\Products\Exceptions\ProductNotFoundException;
use App\Shop\Products\Product;
use App\Shop\Crops\Crop;
use App\Shop\TechnicalCategories\Technical;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Repositories\ProductRepository;
use App\Shop\Products\Requests\CreateProductRequest;
use App\Shop\Products\Requests\UpdateProductRequest;
use App\Shop\Categories\Category;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Shop\Employees\Repositories\EmployeeRepository;
use App\Helper\Permission;
use App\Shop\Products\Product_attributes;


class UpdateStockController extends Controller
{
    use ProductTransformable, UploadableTrait;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
    private $companiesRepo;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepo;

    /**
     * @var AttributeValueRepositoryInterface
     */
    private $attributeValueRepository;

    /**
     * @var ProductAttribute
     */
    private $productAttribute;

    /**
     * @var BrandRepositoryInterface
     */
    private $brandRepo;

    /**
     * ProductController constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param CategoryRepositoryInterface $categoryRepository
     * @param AttributeRepositoryInterface $attributeRepository
     * @param AttributeValueRepositoryInterface $attributeValueRepository
     * @param ProductAttribute $productAttribute
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        CategoryRepositoryInterface $categoryRepository,
        AttributeRepositoryInterface $attributeRepository,
        AttributeValueRepositoryInterface $attributeValueRepository,
        ProductAttribute $productAttribute,
        BrandRepositoryInterface $brandRepository,
        EmployeeRepositoryInterface $employeeRepository
    ) {
        $this->productRepo = $productRepository;
        $this->categoryRepo = $categoryRepository;
        $this->attributeRepo = $attributeRepository;
        $this->attributeValueRepository = $attributeValueRepository;
        $this->productAttribute = $productAttribute;
        $this->brandRepo = $brandRepository;
        $this->companiesRepo = $employeeRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permission = Permission::permission('update_stock');
        if($permission->view==1) {  
            $products = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->get();
            return view('admin.updateStock.list', [
                'products' => $products,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }


    public function edit(int $id)
    {
        $permission = Permission::permission('update_stock');
        if($permission->edit==1) {
            $product = $this->productRepo->findProductById($id);
            // dd($product);
            $technicals = Technical::get();
            $productAttributes = $product->attributes()->get();
            // $product['unit'] = json_decode($product->unit);
            $unit = Product_attributes::where('product_id',$id)->get();
            $product['unit'] = $unit;
            $product['additional_info'] = json_decode($product->additional_info);
            $qty = $productAttributes->map(function ($item) {
                return $item->quantity;
            })->sum();

            if (request()->has('delete') && request()->has('pa')) {
                $pa = $productAttributes->where('id', request()->input('pa'))->first();
                $pa->attributesValues()->detach();
                $pa->delete();

                request()->session()->flash('message', 'Delete successful');
                return redirect()->route('admin.products.edit', [$product->id, 'combination' => 1]);
            }
            $categories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', null)
                ->where('company_id', $product->company_id);
            $subcategories = $this->categoryRepo->listCategories('name', 'asc')->where('parent_id', $product->category_id);
            return view('admin.updateStock.edit', [
                'product' => $product,
                'images' => $product->images()->get(['src']),
                'categories' => $categories,
                'selectedIds' => $product->categories()->pluck('category_id')->all(),
                'attributes' => $this->attributeRepo->listAttributes(),
                'productAttributes' => $productAttributes,
                'qty' => $qty,
                'brands' => $this->brandRepo->listBrands(['*'], 'name', 'asc'),
                'weight' => $product->weight,
                'default_weight' => $product->mass_unit,
                'weight_units' => Product::MASS_UNIT,
                'companies' => $this->companiesRepo->listEmployees('name', 'asc'),
                'crops' => Crop::where('crop_category', null)->get(),
                'subcategories' => $subcategories,
                'cropscategories' => Crop::where('crop_category', $product->crop_category)->get(),
                'technicals' => $technicals
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProductRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \App\Shop\Products\Exceptions\ProductUpdateErrorException
     */

    public function update(Request $request, int $id)
	{	
        try {
            $permission = Permission::permission('update_stock');
            if($permission->edit==1) {
                $rules = [
                    'stock_status' => 'required',
                ];
                $customeMessage = [
                    'stock_status.required' => 'Please select stock status',
                ];
                $validator = Validator::make($request->all(),$rules, $customeMessage);

                if( $validator->fails() ) {
                     return back()->withInput()->withErrors($validator->errors());
                } else {
                    if(Product::where('id', $id)->exists()){
                        $attribute = Product_attributes::where('product_id',$id)->get();
                        $add_quantities = $request->add_quantities;
                        for ($i=0; $i < count($attribute); $i++) { 
                            $availavle_quantity = $attribute[$i]->available_quantity;
                            $availavle_quantity = $availavle_quantity +$add_quantities[$i];
                            $quantity = $attribute[$i]->quantity;
                            $quantity = $quantity +$add_quantities[$i];
                            $attribute[$i]->available_quantity = $availavle_quantity;
                            $attribute[$i]->quantity = $quantity;
                            $attribute[$i]->save();
                        }
                        $product = Product::where('id', $id)->first();
                        $quantity = $product->quantity + $request->add_quantity;
                        $available_quantity = $product->available_quantity + $request->add_quantity;
                        if($request->add_quantity){
                            $product->mail_status = 0;
                        }
                        $product->status = $request->status;
                        $product->stock_status=$request->stock_status;
                        if($product->save()) {
                            return redirect()->route('admin.updateStock.index')
                                    ->with('message', 'Product stock details updated successfully.');
                        } else {
                            return redirect()->route('admin.updateStock.edit')
                                    ->with('message', 'Product not found!!.');
                        }
                    }
                }
            } else {
                return view('layouts.errors.403');
            }
        } catch (Exception $e) {
            log::error($e);
        }
	}

}
