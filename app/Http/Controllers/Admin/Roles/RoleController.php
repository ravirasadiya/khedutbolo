<?php

namespace App\Http\Controllers\Admin\Roles;

use App\Http\Controllers\Controller;
use App\Shop\Permissions\Repositories\Interfaces\PermissionRepositoryInterface;
use App\Shop\Roles\Repositories\RoleRepository;
use App\Shop\Roles\Repositories\RoleRepositoryInterface;
use App\Shop\Roles\Requests\CreateRoleRequest;
use App\Shop\Roles\Requests\UpdateRoleRequest;
use App\Shop\Modules\Module;
use App\Shop\ModulePermissions\ModulePermission;
use App\Shop\Roles\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helper\Permission;
use App\Shop\RoleUsers\RoleUser;
use Auth;

// use \stdClass;

class RoleController extends Controller
{
    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepo;

    /**
     * @var PermissionRepositoryInterface
     */
    private $permissionRepository;

    /**
     * RoleController constructor.
     *
     * @param RoleRepositoryInterface $roleRepository
     * @param PermissionRepositoryInterface $permissionRepository
     */
    public function __construct(
        RoleRepositoryInterface $roleRepository,
        PermissionRepositoryInterface $permissionRepository
    ) {
        $this->roleRepo = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $permission = Permission::permission('role');
        if($permission->view==1) {
            $list = $this->roleRepo->listRoles('name', 'asc')->all();
            $admin = Auth::guard('employee')->user()->id;
            $admin_role = RoleUser::join('roles','roles.id','role_user.role_id')->select('roles.name')->where('role_user.user_id',$admin)->first();
            $admin_role=$admin_role->name;
            $roles = $this->roleRepo->paginateArrayResults($list);
            return view('admin.roles.list', ['roles' => $roles, 'permission' => $permission, 'admin_role' => $admin_role]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $permission = Permission::permission('role');
        if($permission->add==1) {
            $permissions = $this->permissionRepository->listPermissions(['*'], 'name', 'asc');
            $modules = Module::get();
            // dd($modules);
            return view('admin.roles.create', compact('permissions', 'modules'));
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * @param CreateRoleRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $permission = Permission::permission('role');
        if($permission->add==1) {
            $rules = [
                'name' => 'required|unique:roles',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name',
                'name.unique' => 'Please enter unique name',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);
    
            if( $validator->fails() ) {
                 return back()->withInput()->withErrors($validator->errors());
            } else {
                $role = $this->roleRepo->createRole($request->except('_method', '_token'));
                $lastRole = Role::Select('id')->orderBy('id', 'DESC')->first();
                // dd($lastRole->id);
                $models = Module::get();
                foreach ($models as $model) {
                    $permission = new ModulePermission();
                    $permission->view=0;
                    $permission->add=0;
                    $permission->edit=0;
                    $permission->delete=0;
                    $permission->user_id=$lastRole->id;
                    $permission->module_id=$model->id;
                    $permission->save();
                }
                $role_id = $lastRole->id;
                if($request->input('view')) {
                    $views = $request->view;
                    foreach ($views as $view) {
                        $firstchar = substr($view, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$view);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        // dd('enter');
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->view = $firstchar;
                            $permission->update();
                        }
                    }
                }
                if($request->input('add')) {
                    $adds = $request->add;
                    foreach ($adds as $add) {
                        $firstchar = substr($add, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$add);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->add = $firstchar;
                            $permission->update();
                        }
                    }
                }
                if($request->input('edit')) {
                    $edits = $request->edit;
                    foreach ($edits as $edit) {
                        $firstchar = substr($edit, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$edit);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->edit = $firstchar;
                            $permission->update();
                        }
                    }
                }
                if($request->input('delete')) {
                    $deletes = $request->delete;
                    foreach ($deletes as $delete) {
                        $firstchar = substr($delete, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$delete);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->delete = $firstchar;
                            $permission->update();
                        }
                    }
                }
    
                return redirect()->route('admin.roles.index')
                    ->with('message', 'Role added successfully');
            }
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $permission = Permission::permission('role');
        if($permission->edit==1) {
            $role = $this->roleRepo->findRoleById($id);
    
            $roleRepo = new RoleRepository($role);
            $attachedPermissionsArrayIds = $roleRepo->listPermissions()->pluck('id')->all();
            // $permissions = $this->permissionRepository->listPermissions(['*'], 'name', 'asc');
            $permissions = ModulePermission::where('user_id', $role->id)->get();
            // dd($permissions[0]);
            $add=$edit=$delete=$view=0;
            foreach($permissions as $permission) {
                if($permission->add==1){
                    $add=$add+1;
                }
                if($permission->edit==1){
                    $edit=$edit+1;
                }
                if($permission->delete==1){
                    $delete=$delete+1;
                }
                if($permission->view==1){
                    $view=$view+1;
                }
            }
            $modules = Module::get();
            return view('admin.roles.edit', compact(
                'role',
                'permissions',
                'attachedPermissionsArrayIds',
                'modules',
                'delete',
                'add',
                'edit',
                'view'
            ));
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * @param UpdateRoleRequest $request
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::permission('role');
        if($permission->edit==1) {
            // dd($request);
            $rules=array();
            $customeMessage=array();
            if($request!=""){
                $old_data = $this->roleRepo->findRoleById($id);
                $old_id = $id;
                $old_name = $old_data->name;
                if( $old_id != $id ){
                   $rules['id'] = 'required';
                    $customeMessage['id.required']  = 'Please enter id.';
                } 
                if( $old_name != $request->name ){
                    $rules['name'] = 'unique:roles';   
                    $customeMessage['name.unique']  = 'Please enter unique name.';
                }
            } else{
                 $rules = [
                    'name' => 'required|unique:permissions',
                    'display_name' => 'required',
                ];
                $customeMessage = [
                    'name.required' => 'Please enter name.',
                    'name.unique' => 'Please enter unique name.',
                    'display_name.required' => 'Please enter display name',
                ];
            }
            $validator = Validator::make($request->all(),$rules, $customeMessage);
            if( $validator->fails() ){
                // dd('enter');
                return back()->withInput()->withErrors($validator->errors());
            } else{
                $role = $this->roleRepo->findRoleById($id);
                $role->name = $request->name;
                $role->display_name = $request->display_name;
                $description = $request->description;
                $description = str_replace("<p>","",$description);
                $description = str_replace("</p>","",$description);
                $role->description = $description;
                $role->update();
                $models = Module::get();
                foreach ($models as $model) {
                    $permission = ModulePermission::where('module_id', $model->id)->where('user_id', $role->id)->first();
                    $permission->view=0;
                    $permission->add=0;
                    $permission->edit=0;
                    $permission->delete=0;
                    $permission->user_id=$role->id;
                    $permission->module_id=$model->id;
                    $permission->save();
                }
                $role_id = $role->id;
                if($request->input('view')) {
                    $views = $request->view;
                    foreach ($views as $view) {
                        $firstchar = substr($view, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$view);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->view = $firstchar;
                            $permission->update();
                        }
                    }
                }
                if($request->input('add')) {
                    $adds = $request->add;
                    foreach ($adds as $add) {
                        $firstchar = substr($add, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$add);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->add = $firstchar;
                            $permission->save();
                        }
                    }
                }
                if($request->input('edit')) {
                    $edits = $request->edit;
                    foreach ($edits as $edit) {
                        $firstchar = substr($edit, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$edit);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->edit = $firstchar;
                            $permission->save();
                        }
                    }
                }
                if($request->input('delete')) {
                    $deletes = $request->delete;
                    foreach ($deletes as $delete) {
                        $firstchar = substr($delete, 0, 1);
                        $replace = $firstchar."-";
                        $module_name = str_replace($replace,"",$delete);
                        $module_id = Module::Select('id')->where('name', $module_name)->first();
                        $id = $module_id->id;
                        if(ModulePermission::where('module_id', $id)->where('user_id', $role_id)->exists()) {
                            $permission = ModulePermission::where('module_id', $id)->where('user_id', $role_id)->first();
                            $permission->delete = $firstchar;
                            $permission->save();
                        }
                    }
                }
                return redirect()->route('admin.roles.index')
                    ->with('message', 'Role updated successfully');
            }
        } else {
            return view('layouts.errors.403');
        }
    }
    
    public function destroy($id) {
        $permission = Permission::permission('role');
        if($permission->delete==1) {
            $role = $this->roleRepo->findRoleById($id);
            $role->delete();
            return back()->with('message', 'Role deleted successfully.');
        } else {
            return view('layouts.errors.403');
        }
    }
}
