<?php

namespace App\Http\Controllers\Admin\Priorities;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Priorities\Priority;
use Log;
use App\Helper\Permission;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Priorities\TrendingProduct;

class PriorityController extends Controller
{
    public function index() {
        $permission = Permission::permission('priority');
        if($permission->view==1) {
        	$priorities = Priority::orderBy('priority', 'ASC')->get();
        	return view('admin.priorities.list', [
                'priorities' => $priorities,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    } 

    public function edit($id) {
    	try {
            $permission = Permission::permission('priority');
            if($permission->edit==1) {
        		if(Priority::where('id', $id)->exists()) {
    	    		$priority = Priority::find($id);
                    $categories = Category::where('parent_id',null)->get();
                   // category_id
                    $subcategory = Category::where('parent_id',$priority->category_id)->get();
                   //$products = Product::where('subcategory_id', $priority->subcategory_id)->get();
                   $products = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->distinct('products.id')
                            ->where('employees.status',1)
                            ->get();
                    $trend_product = TrendingProduct::select('product_id')->get()->toArray();
                  /*  $products = TrendingProduct::join('products','products.id','trending_category.product_id')
                            ->join('categories','categories.id','products.category_id')
                            ->join('employees','employees.id','products.company_id')
                            ->select('products.id','products.name', 'trending_category.priority','employees.name as company_name','products.technical_name')
                            ->get();*/
                    $treding = array();
                    foreach($trend_product as $tp)
                    {
                        array_push($treding ,$tp['product_id']);
                    }
                    // echo "<pre>";
                    // print_r($treding);exit;
    		    	return view('admin.priorities.edit', [
    		            'priority' => $priority,
                        'categories' => $categories,
                        'subcategories' => $subcategory,
                        'products' => $products,
                        'trending_product'=>$treding
    		        ]);
    		    } else {
    		    	return redirect()->route('admin.pro_priority.index')->with('message', 'Priority not found!!');
    		    }
            } else {
                return view('layouts.errors.403');
            }
	    } catch (Exception $e) {
			log::error($e);
    	}
    }

    public function update(Request $request, $id) {
    	try {
    	 
    	  //  exit;
            $permission = Permission::permission('priority');
            if($permission->edit==1) {
                if($request->view_priority=="1"){
                    $product_all = array();
                    if($request->check!=""){
                   	
                        foreach($request->check as $product){
                            $product =  Product::where('products.id',$product)->join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company_name', 'categories.name as category')
                            ->first();
                            
                            array_push($product_all , $product);
                        }
                        $data['products'] = $product_all;
                       // echo "<pre>";
                        //print_r( $data['products']);
                       
                        $data['priority'] = Priority::find($id);
                        // print_r( $data['priority']);
                        //  exit;
                        return view('admin.priorities.view')->with($data);    
                    }else{
                        return back()->with('error','Please select product.');
                    }
                    
                }else{
                
                    if(Priority::where('id', $id)->exists()) {
        	    		$priority = Priority::find($id);
        	    		if($request->category_id!="all"){
        	    		    $priority->category_id = $request->category_id;    
        	    		}
                        $priority->subcategory_id = $request->subcategory_id;
        	    		$priority->save();
        	    	return redirect()->route('admin.pro_priority.index')->with('message', 'Priority update successfully.');
        	    	} else {
return redirect()->route('admin.pro_priority.index')->with('message', 'Priority not found!!');
        	    	}
                }
        		
            } else {
            return view('layouts.errors.403');
        }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    	
    }
    
    public function view(Request $request, $id) {
    	try {
    	    
            $permission = Permission::permission('priority');
            if($permission->edit==1) {
        		if(Priority::where('id', $id)->exists()) {
    	    		$priority = Priority::find($id);
    	    		if($request->category_id!="all"){
    	    		    $priority->category_id = $request->category_id;    
    	    		}
                    $priority->subcategory_id = $request->subcategory_id;
    	    		$priority->save();
    	    	return redirect()->route('admin.pro_priority.index')->with('message', 'Priority update successfully.');
    	    	} else {
    	    	return redirect()->route('admin.pro_priority.index')->with('message', 'Priority not found!!');
    	    	}
            } else {
            return view('layouts.errors.403');
        }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    	
    }

    public function cat_sort(Request $request)
    {
        try{
            $priorities = $request->id;
            if(count($priorities)!=0){
                foreach ($priorities as $key => $value) {
                    if(!empty($value)){
                        $priority = Priority::find($value);
                        $priority->priority = $key;
                        $priority->save();
                    }
                }
            }
            return "true";
        } catch (\Exception $e) {
            Exceptions::exception($e);
            return "false";
        }
    }

    public function subcat_sort(Request $request)
    {
        try{
            $sub_priorities = $request->id;
               // return($sub_priorities) ;
                
                
            if(count($sub_priorities)!=0){
                $trendingscnt = TrendingProduct::count();
                if($trendingscnt>0){
                     $trendings = TrendingProduct::get();
                    if(count($trendings)!=0){
                        foreach($trendings as $trending){
                            $data = TrendingProduct::find($trending->id);
                            //return $data;
                            $data->delete();
                            // if($data->delete()){
    
                            // }
                        }
                        //exit;
                    }
                }
               
                foreach ($sub_priorities as $key => $value) {
                    
                   // echo $value;
                    
                   if(!empty($value)){
                        $data = new TrendingProduct();
                        $data->priority = $key;
                        $data->product_id = $value;
                        if($data->save()){
                        }
                    }
                }
            }
            return "true";
          
        } catch (\Exception $e) {
            // echo $e;
            // exit;
            Exceptions::exception($e);
            return "false";
        }
    }

    public function getProduct(Request $request)
    {
        $products = Product::select('id', 'name')->where('category_id', $request->category_id)->where('subcategory_id', $request->id)->where('status', 1)->get();
        $output = [];
        foreach( $products as $product )
        {
            $output[$product->id] = $product->name;
        }
        return $output;
    }
    
    public function getCategoryProduct(Request $request)
    {
        $trend_product = TrendingProduct::select('product_id')->get()->toArray();
        $products = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->whereIn('products.id',$trend_product)
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('products.id', 'DESC')
                            ->distinct('products.id')
                            ->where('employees.status',1)
                            ->get();
        return $products;
        // $products = Product::select('id', 'name')->where('category_id', $request->id)->where('subcategory_id', null)->where('status', 1)->get();
        // $output = [];
        // foreach( $products as $product )
        // {
        //     $output[$product->id] = $product->name;
        // }
        // return $output;
    }
    public function getAllProduct(Request $request)
    {
        $products = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
        $output = [];
        foreach( $products as $product )
        {
            $output[$product->id] = $product->name;
        }
        return $products;
    }
}
