<?php

namespace App\Http\Controllers\Admin\Crop;

use App\Shop\Crop\Crop;
use App\Shop\Crop\Repositories\CropRepository;
use App\Shop\Crop\Repositories\Interfaces\CropRepositoryInterface;
use App\Shop\Crop\Requests\CreateCropRequest;
use App\Shop\Crop\Requests\UpdateCropRequest;
use App\Shop\Crop\Transformations\CropTransformable;
use App\Http\Controllers\Controller;

class CropController extends Controller
{
    use CropTransformable;

    /**
     * @var CropRepositoryInterface
     */
    private $cropRepo;

    /**
     * CropController constructor.
     * @param CropRepositoryInterface $cropRepository
     */
    public function __construct(CropRepositoryInterface $cropRepository)
    {
        $this->cropRepo = $cropRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->cropRepo->listCrops('created_at', 'desc');

        if (request()->has('q')) {
            $list = $this->cropRepo->searchCrop(request()->input('q'));
        }

        $crops = $list->map(function (Crop $crop) {
            return $this->transformCrop($crop);
        })->all();


        return view('admin.crops.list', [
            'crops' => $this->cropRepo->paginateArrayResults($crops)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.crops.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCropRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCropRequest $request)
    {
        $this->cropRepo->createCrop($request->except('_token', '_method'));
        $request->session()->flash('message', 'Crop have been created successfully!');
        return redirect()->route('admin.crops.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $crop = $this->cropRepo->findCropById($id);
        
        return view('admin.crops.show', [
            'crop' => $crop,
            'addresses' => $crop->addresses
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.crops.edit', ['crop' => $this->cropRepo->findCropById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCropRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCropRequest $request, $id)
    {
        $crop = $this->cropRepo->findCropById($id);
       
        if ($request->input('status') != $crop->status) {
            $data = $crop;
            $data['status'] = $request->input('status');
            Mail::to($request->input('email'))->send(new CropsEmail($data));
        }

        $update = new CropRepository($crop);
        $data = $request->except('_method', '_token', 'password');

        if ($request->has('password')) {
            $data['password'] = bcrypt($request->input('password'));
        }

        $update->updateCrop($data);

        // $request->session()->flash('message', 'Crop details has been updated successfully!');
        return redirect()->route('admin.crops.index')->with('message', 'Create crop successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $crop = $this->cropRepo->findCropById($id);

        $cropRepo = new CropRepository($crop);
        $cropRepo->deleteCrop();

        return redirect()->route('admin.crops.index')->with('message', 'Crop has been deleted successful');
    }
}
