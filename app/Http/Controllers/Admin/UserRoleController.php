<?php

namespace App\Http\Controllers\Admin;

use App\Shop\Admins\Requests\CreateEmployeeRequest;
use App\Shop\Admins\Requests\UpdateEmployeeRequest;
use App\Shop\Employees\Repositories\EmployeeRepository;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Shop\Roles\Repositories\RoleRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\CompaniesEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Shop\Employees\Employee;
use App\Helper\Permission;
use App\Shop\Roles\Role;

class CompaniesController extends Controller
{
    /**
     * @var EmployeeRepositoryInterface
     */
    private $employeeRepo;
    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepo;

    /**
     * CompaniesController constructor.
     *
     * @param EmployeeRepositoryInterface $employeeRepository
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(
        EmployeeRepositoryInterface $employeeRepository,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->employeeRepo = $employeeRepository;
        $this->roleRepo = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = Permission::permission('company');
        if($permission->view==1) {
            $list = $this->employeeRepo->listEmployees('id', 'desc');

            return view('admin.companies.list', [
                'employees' => $this->employeeRepo->paginateArrayResults($list->all()),
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::permission('company');
        if($permission->add==1) {
            // $roles = $this->roleRepo->listRoles();
            $roles = Role::orderBy('name', 'ASC')->get();

            return view('admin.companies.create', compact('roles'));
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateEmployeeRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        $permission = Permission::permission('company');
        if($permission->add==1) {
            $employee = $this->employeeRepo->createEmployee($request->all());
            
            if ($request->has('role')) {
                // if ($request->hasFile('logo') && $request->file('logo') instanceof UploadedFile) {
                    // $employee['logo'] = $this->uploadOne($request->file('logo'), 'logo');
                    // $employee['logo'] = $this->employeeRepo->saveLogoImage($request->file('logo'));
                // }
                $employeeRepo = new EmployeeRepository($employee);
                $employeeRepo->syncRoles([$request->input('role')]);
                if ($request->hasFile('logo')) {
                    $url = env('APP_URL');
                    $file    = $request->logo;
                    $time    = md5(time());
                    $profile = $file->getClientOriginalExtension();
                    $name = $file->getClientOriginalName();
                    $path    = "storage/logo";
                    $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                    $profile_update          = Employee::find($employee->id);
                    $profile_update->logo = $url."/".$path.'/'.$time.'.'.$profile;
                    // dd($profile_update->logo);
                    $profile_update->save();
                }
            }

            return redirect()->route('admin.companies.index')
                ->with('message', 'Company Added Successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $employee = $this->employeeRepo->findEmployeeById($id);
        return view('admin.companies.show', ['employee' => $employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $permission = Permission::permission('company');
        if($permission->edit==1) {
            $employee = $this->employeeRepo->findEmployeeById($id);
            $roles = Role::orderBy('name', 'ASC')->get();
            $isCurrentUser = $this->employeeRepo->isAuthUser($employee);
            
            return view(
                'admin.companies.edit',
                [
                    'employee' => $employee,
                    'roles' => $roles,
                    'isCurrentUser' => $isCurrentUser,
                    'selectedIds' => $employee->roles()->pluck('role_id')->all()
                ]
            );
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEmployeeRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $permission = Permission::permission('company');
        if($permission->edit==1) {
            $employee = $this->employeeRepo->findEmployeeById($id);
            $isCurrentUser = $this->employeeRepo->isAuthUser($employee);
            

            if ($request->input('status') != $employee->status) {
                $data = $employee;
                $data['status'] = $request->input('status');
                Mail::to($request->input('email'))->send(new CompaniesEmail($data));
            }

            if ($request->hasFile('logo')) {
                $employee['logo'] = $this->employeeRepo->saveLogoImage($request->file('logo'));
            }
            
            $empRepo = new EmployeeRepository($employee);
            $empRepo->updateEmployee($request->except('_token', '_method', 'password'));
            
            
            if ($request->hasFile('logo')) {
                $url = env('APP_URL');
                $file    = $request->logo;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "storage/logo";
                $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                $profile_update          = Employee::find($id);
                $profile_update->logo = $url."/".$path.'/'.$time.'.'.$profile;
                // dd($profile_update->logo);
                $profile_update->save();
            }
            if ($request->has('password') && !empty($request->input('password'))) {
                $employee->password = Hash::make($request->input('password'));
                $employee->save();
            }

            if ($request->has('roles') and !$isCurrentUser) {
                $employee->roles()->sync($request->input('roles'));
            } elseif (!$isCurrentUser) {
                $employee->roles()->detach();
            }

            return redirect()->route('admin.companies.index')
                ->with('message', 'Company Updated Successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $permission = Permission::permission('company');
        if($permission->delete==1) {
            $employee = $this->employeeRepo->findEmployeeById($id);
            $employeeRepo = new EmployeeRepository($employee);
            $employeeRepo->deleteEmployee();

            return redirect()->route('admin.companies.index')->with('message', 'Company Deleted Successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile($id)
    {
        $employee = $this->employeeRepo->findEmployeeById($id);
        return view('admin.companies.profile', ['employee' => $employee]);
    }

    /**
     * @param UpdateEmployeeRequest $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(Request $request, $id)
    {
        $employee = $this->employeeRepo->findEmployeeById($id);

        if ($request->hasFile('logo')) {
            $employee['logo'] = $this->employeeRepo->saveLogoImage($request->file('logo'));
        }
        
        $update = new EmployeeRepository($employee);
        $update->updateEmployee($request->except('_token', '_method', 'password'));

        if ($request->has('password') && $request->input('password') != '') {
            $update->updateEmployee($request->only('password'));
        }

        return redirect()->route('admin.dashboard')
            ->with('message', 'Update profile successfully');
    }

    public function removeLogo(Request $request)
    {
        $employee = $this->employeeRepo->findEmployeeById($request->employee);
        Storage::disk('public')->delete($employee->logo);
        $company = Employee::find($employee->id);
        $company->logo = null; 
        $company->save();
        request()->session()->flash('message', 'logo delete successful');
        return redirect()->route('admin.companies.edit', $request->input('employee'));
    }
}
