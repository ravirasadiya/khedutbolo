<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Categories\Requests\CreateCategoryRequest;
use App\Shop\Categories\Requests\UpdateCategoryRequest;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Shop\Employees\Repositories\EmployeeRepository;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use App\Helper\Permission;
use App\Shop\Employees\Employee;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;
    private $companiesRepo;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository,EmployeeRepositoryInterface $employeeRepository)
    {
        $this->categoryRepo = $categoryRepository;
        $this->companiesRepo = $employeeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd($_SERVER['SERVER_NAME']);
        $permission = Permission::permission('category');
        if($permission->view==1) {
             $list = Category::Select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('parent_id', null)
                            ->get();

            // $list = $this->categoryRepo->listCategories('created_at', 'desc')->where('parent_id',NULL);
            
            return view('admin.categories.list', [
                'categories' => $list,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::permission('category');
        if($permission->add==1) {
            return view('admin.categories.create', [
                'categories' => $this->categoryRepo->listCategories('name', 'asc'),
                // 'companies' => $this->companiesRepo->listEmployees('name', 'asc')
                'companies' => Employee::where('status',1)->get()
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $permission = Permission::permission('category');
            if($permission->add==1) {
                $rules = [
                    'name' => 'required',
                    // 'company_id' => 'required',
                    'cover' => 'required',
                    'status'  => 'required',
                ];
                $customeMessage = [
                    'name.required' => 'Please enter name.',
                    'company_id.required' => 'Please  select company',
                    'cover.required' => 'Please select cover image',
                    'status.required' => 'Please select status',
                ];
                $validator = Validator::make($request->all(),$rules, $customeMessage);

                if( $validator->fails() ) {
                     return back()->withInput()->withErrors($validator->errors());
                } else {
                    $category = $this->categoryRepo->createCategory($request->except('_token', '_method'));
                    $url = env('APP_URL');
                    if ($request->has('cover')) {
                        $file    = $request->cover;
                        $time    = md5(time());
                        $profile = $file->getClientOriginalExtension();
                        $name = $file->getClientOriginalName();
                        $path    = "storage/categories";
                        $file->move(public_path(env('IMAGE_URL').$path), $name);
                        $profile_update          = Category::find($category->id);
                        $profile_update->cover = $url."/storage/categories" . "/" .$name;
                        $profile_update->save();
                    }
                    if($request->parent) {
                        $request->session()->flash('message', 'Sub-category created successfully');
                        return redirect()->route('admin.categories.show', $request->parent);
                    } else {
                        $request->session()->flash('message', 'Category created successfully');
                        return redirect()->route('admin.categories.index');
                    }
                }
            } else {
            return view('layouts.errors.403');
        }
        } catch (Exception $e) {
            log::error($e);
        }
        // return redirect()->route('admin.categories.index')->with('message', 'Category created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->categoryRepo->findCategoryById($id);
        $categories = Category::select('categories.id', 'categories.name','categories.status', 'categories.cover')
                            ->where('categories.parent_id',$id)
                            ->get();

        $cat = new CategoryRepository($category);
        $permission = Permission::permission('category');
        return view('admin.categories.show', [
            'category' => $category,
            'categories' => $categories,
            'products' => $cat->findProducts(),
            'permission' => $permission
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::permission('category');
        if($permission->edit==1) {
            return view('admin.categories.edit', [
                'categories' => $this->categoryRepo->listCategories('name', 'asc', $id),
                'companies' => Employee::where('status',1)->get(),
                'category' => $this->categoryRepo->findCategoryById($id)
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCategoryRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $permission = Permission::permission('category');
            if($permission->edit==1) {
                $rules = [
                    'name' => 'required',
                    'status'  => 'required',
                ];
                $customeMessage = [
                    'name.required' => 'Please enter name.',
                    'status.required' => 'Please select status',
                ];
                $validator = Validator::make($request->all(),$rules, $customeMessage);

                if( $validator->fails() ) {
                     return back()->withInput()->withErrors($validator->errors());
                } else {
                    $category = Category::where('id', $id)->first();
                    $category->company_id = $request->company_id;
                    $category->parent_id = $request->parent;
                    $category->name = $request->name;
                    $category->name_hi = $request->name_hi;
                    $category->name_gu = $request->name_gu;
                    $description = $request->description;
                    $description = str_replace('</p>', '', $description);
                    $description = str_replace('<p>', '', $description);
                    $description_hi = $request->description_hi;
                    $description_hi = str_replace('</p>', '', $description_hi);
                    $description_hi = str_replace('<p>', '', $description_hi);
                    $description_gu = $request->description_gu;
                    $description_gu = str_replace('</p>', '', $description_gu);
                    $description_gu = str_replace('<p>', '', $description_gu);
                    $category->description = $description;
                    $category->description_hi = $description_hi;
                    $category->description_gu = $description_gu;
                    $category->status = $request->status;
                    $url = env('APP_URL');

                    if ($request->has('cover')) {
                        $file    = $request->cover;
                        $time    = md5(time());
                        $profile = $file->getClientOriginalExtension();
                        $name = $file->getClientOriginalName();
                        $path    = "storage/categories";
                        $file->move(public_path(env('IMAGE_URL').$path), $name);
                        $profile_update          = Category::find($id);
                        $profile_update->cover = $url."/storage/categories" . "/" .$name;
                        $profile_update->save();
                    }
                    if($category->update()) {
                        if($category->parent_id) {
                            $request->session()->flash('message', 'Sub-category updated successfully');
                            return redirect()->route('admin.categories.show', $request->parent);
                        } else {
                            $request->session()->flash('message', 'Category updated successfully');
                            return redirect()->route('admin.categories.index');
                        }
                    }
                }
            } else {
            return view('layouts.errors.403');
        }
        } catch (Exception $e) {
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $permission = Permission::permission('category');
        if($permission->delete==1) {
            $category = $this->categoryRepo->findCategoryById($id);
            // dd($category);
            $parent = $category->parent_id;
            $list= null;
            $products = null;
            if($category->parent_id) {
                $products=Product::where('subcategory_id', $category->id)->first();
            } else {
                // $list = $this->categoryRepo->listCategories('created_at', 'desc')->where('parent_id',$id);
                $list = Category::where('parent_id', $category->id)->first();
            }
            if($list != null) {
                 return redirect()->route('admin.categories.index')->with('message', 'Unable to delete! please delete sub-category first');
            } 
            if($products != null){
                return redirect()->route('admin.categories.show', $parent)->with('message', 'Unable to delete! please delete product first');
            }
            if($category->parent_id){
                $category->products()->sync([]);
                $category->delete();
               return redirect()->route('admin.categories.show', $parent)->with('message', 'Sub-category deleted successfully');
            } else {
                $category->products()->sync([]);
                $category->delete();
                return redirect()->route('admin.categories.index')->with('message', 'Category deleted successfully');
            }
        } else {
            return view('layouts.errors.403');
        }
        
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage(Request $request)
    {
        // $category = $this->categoryRepo->findCategoryById($request->category);
        $category = Category::find($request->category);
        Storage::disk('public')->delete($category->cover);
        $category->cover = null;
        $category->save();
        request()->session()->flash('message', 'Image delete successful');
        return redirect()->route('admin.categories.edit', $request->input('category'));
    }
}
