<?php

namespace App\Shop\Categories\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateCategoryRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:categories'],
            'company_id' => ['required'],
            'cover' => ['required','image:jpeg,png,jpg|max:2048'],
            'description' => ['required'],
        ];
    }
}
