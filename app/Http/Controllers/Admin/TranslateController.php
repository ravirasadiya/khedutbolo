<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Log;

class TranslateController extends Controller
{
    public function translate(Request $request)
    {
    	try {
    		$alldata = $request->all();
    		$result = array();
    		foreach ($alldata as $key => $value) {
	    		$mainString = trim($value);
	    		$tr = new GoogleTranslate();
				$tr->setTarget('gu');
				$data['gu'] = $tr->translate($mainString);
				$tr->setTarget('hi');
				$data['hi'] = $tr->translate($mainString);
    			$result[$key] = $data;
    		}
			return json_encode($result);
    	} catch (Exception $e) {
    		Log::error($e);
    	}
    }
}
