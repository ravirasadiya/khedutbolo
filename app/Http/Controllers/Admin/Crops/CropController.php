<?php

namespace App\Http\Controllers\Admin\Crops;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Crops\Crop;
use App\Shop\Products\Product;
use App\Shop\TechnicalCategories\Technical;
use Log;
use App\Helper\Permission;

class CropController extends Controller
{
    public function index()
    {
        $permission = Permission::permission('crop');
        if($permission->view==1) {
            $crops = Crop::orderBy('created_at', 'desc')->where('crop_category', null)->get();

            return view('admin.crop.list', [
                'crops' => $crops,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    public function create($id=null)
    {
        $permission = Permission::permission('crop');
        if($permission->add==1) {
            $crops = Crop::where('crop_category', null)->get();
            if(isset($id)){
                $crop= $id;
            } else {
                $crop= 0;
            }
        	$crops = Crop::where('crop_category', null)->get();
            // $product = Product::get();
            $product = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
            $technical = Technical::get();
            return view('admin.crop.create',[
                'categories' => $crops,
                'crop' => $crop,
                'technical' => $technical,
                'products' => $product
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    public function store(Request $request)
    {
    	try {
    	    $permission = Permission::permission('crop');
            if($permission->add==1) {
                $name = trim(strip_tags($request->name));
                $name_hi = trim(strip_tags($request->name_hi));
        		$name_gu = trim(strip_tags($request->name_gu));
                $product_ids = $request->product;
                $pro = json_encode($product_ids);
        		$crop = new Crop();
                $crop->name = $name;
                $crop->name_hi = $name_hi;
        		$crop->name_gu = $name_gu;
                $crop->product_id = $pro;
                $url = env('APP_URL');
        		if($request->core_category){
        			$id = trim(strip_tags($request->core_category));
        			$crop->crop_category = $id;
        		}

        		if($crop->save()){
        			if ($request->has('image')) {
        			    $file    = $request->image;
                        $time    = md5(time());
                        $profile = $file->getClientOriginalExtension();
                        $name = $file->getClientOriginalName();
                        $path    = "images/crop/" . $crop->id . "/file";
                        $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                        $profile_update          = Crop::find($crop->id);
                        $profile_update->image = $url."/".$path . "/" .$time.'.'.$profile;
                        $profile_update->save();
                    }
        		}
                if($request->core_category){
                    return redirect()->route('admin.crops.show', $request->core_category)->with('message', 'Sub-solution added successfully');
                } else {
        	        return redirect()->route('admin.crops.index')->with('message', 'Solution added successfully');
                }
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		
    	}
    }

    public function show($id)
    {
        try {
            $permission = Permission::permission('crop');
            if($permission->view==1) {
                $crop = Crop::where('id', $id)->first();
                $crop_sub_cat = Crop::where('crop_category', $id)->where('crop_sub',null)->get();
                return view('admin.crop.show', [
                	'values' => $crop_sub_cat,
                    'crop' => $crop,
                    'permission' => $permission
                ]);
            } else {
            return view('layouts.errors.403');
        }
        } catch (AttributeNotFoundException $e) {
            request()->session()->flash('error', 'The crop you are looking for is not found.');

            return redirect()->route('admin.crop.index');
        }
    }

    public function destroy($id)
    {
        try {
            $permission = Permission::permission('crop');
            if($permission->delete==1) {
            	$product = Crop::join('products', 'products.crop_id', 'crop.id')
            					->select('products.crop_id', 'crop.*')
            					->where('crop.id', $id);
                $crop = Crop::where('id', $id)->first();
                if(Crop::where('crop_category', $id)->exists()){
                	return redirect()->route('admin.crops.index')->with('error', 'Solution have sub-solution');
                } else if($product->exists()) {
                	return redirect()->route('admin.crops.index')->with('error', 'Sub-solution have products');
                } else {
                	if($crop->crop_category){
                	    $crop->delete();
                	    return redirect()->back()->with('message', 'Sub-solution deleted successfully');
                	} else {
                	    $crop->delete();
                	    return redirect()->back()->with('message', 'Solution deleted successfully');
                	}
                }
            } else {
                return view('layouts.errors.403');
            }
        } catch (AttributeNotFoundException $e) {
           return redirect()->route('admin.crop.index');
        }
    }


    public function edit($id)
    {
        try {
            $permission = Permission::permission('crop');
            if($permission->edit==1) {
                $crop = Crop::where('id', $id)->first();
                $categories = Crop::where('crop_category', null)->get();
                // $product = Product::get();
                $product = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
                $technical = Technical::get();
                return view('admin.crop.edit',['crop' => $crop,'id'=>$id,'technical' => $technical, 'categories' => $categories,'products' => $product]);
            } else {
                return view('layouts.errors.403');
            }
        } catch (AttributeNotFoundException $e) {
            return redirect()->route('admin.crop.index');
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $permission = Permission::permission('crop');
            if($permission->edit==1) {
            	$name = trim(strip_tags($request->name));
                $name_hi = trim(strip_tags($request->name_hi));
                $name_gu = trim(strip_tags($request->name_gu));
                $product_ids = $request->product;
                $pro = json_encode($product_ids);
                // dd($pro);
                
            	if(Crop::where('id', $id)->exists()) {
            		$crop = Crop::where('id', $id)->get()->first();
                    $url = env('APP_URL');
            		if($crop){
            			$crop->name = $name;
                        $crop->name_hi = $name_hi;
                        $crop->name_gu = $name_gu;
                        $crop->product_id = $pro;
                        
                        
            			if($crop->save()){
            				if($request->hasFile('image')){
            					$file    = $request->image;
    		                    $time    = md5(time());
    		                    $profile = $file->getClientOriginalExtension();
    		                    $name = $file->getClientOriginalName();
    		                    $path    = "images/crop/" . $crop->id . "/file";
    		                    $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
    		                    $profile_update          = Crop::find($crop->id);
    		                    $profile_update->image = $url."/".$path . "/" .$time.'.'.$profile;
    		                    $profile_update->save();
            				}
                            if($crop->crop_category) {
                                return redirect()->route('admin.crops.show', $request->core_category)->with('message', 'Sub-solution updated successfully');
                            } else {
                                return redirect()->route('admin.crops.index')->with('message', 'Solution updated successfully');
                            }
            			} else {
            				log::error('Something went wrong crop not found');
            			}
            		} else {
            			log::error('Crop not found');
            		}
            	} else {
            		log::error('Crop not found');
            	}
            } else {
                return view('layouts.errors.403');
            }
            // if($request->core_category){
            //     return redirect()->route('admin.crops.show', $request->core_category)->with('message', 'Update sub-crop successfully');
            // } else {
            //     return redirect()->route('admin.crops.index')->with('message', 'Update crop successfully');
            // }
            // return redirect()->route('admin.crops.index');
        } catch (AttributeNotFoundException $e) {
            request()->session()->flash('error', 'The crop delete successfully.');
            return redirect()->route('admin.crop.index');
        }
    }
    public function showSub($id,$subid)
    {
        try {
            $permission = Permission::permission('crop');
            if($permission->view==1) {
                $crop = Crop::where('id', $id)->first();
                $crop_sub_cat = Crop::where('crop_sub', $id)->get();
                // dd($crop);
                return view('admin.crop.show-sub', [
                	'values' => $crop_sub_cat,
                    'crop' => $crop,
                    'subid' => $subid,
                    'id' => $id,
                    'permission' => $permission
                ]);
            } else {
            return view('layouts.errors.403');
        }
        } catch (AttributeNotFoundException $e) {
            request()->session()->flash('error', 'The crop you are looking for is not found.');

            return redirect()->route('admin.crop.index');
        }
    }
    public function subCreate($id,$subid){
        try {
            $permission = Permission::permission('crop');
            if($permission->add==1) {
                $crop = Crop::where('id', $id)->first();
                $categories_selected = Crop::where('id', $subid)->first();
                $categories = Crop::where('crop_category', null)->get();
                // $product = Product::get();
                $product = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
                $technical = Technical::get();
                return view('admin.crop.create-sub',[
                    'categories' => $categories,
                    'categories_selected' => $categories_selected,
                    'subid' => $subid,
                    'technical' => $technical,
                    'products' => $product,
                    'crop' => $crop
                ]);
            } else {
                return view('layouts.errors.403');
            }
        } catch (AttributeNotFoundException $e) {
            request()->session()->flash('error', 'The crop you are looking for is not found.');

            return redirect()->route('admin.crop.index');
        }
    }
    public function subStore(Request $request)
    {
    	try {
    	   // dd($request->all());
    	    $permission = Permission::permission('crop');
    	    $id = $request->id;
    	   // dd($request->all());
            if($permission->add==1) {
                $name = trim(strip_tags($request->name));
                $name_hi = trim(strip_tags($request->name_hi));
        		$name_gu = trim(strip_tags($request->name_gu));
                $product_ids = $request->product;
                
              
               // $pro = json_encode($product_ids);
               if(count($product_ids)==1){
                   
                   $pro = $product_ids[0];
               }
               else{
                   $pro = str_replace('"', "", json_encode($product_ids, JSON_HEX_APOS));
               }
                
              
        		if($id!=""){
        		    $crop = Crop::find($id);    
        		}else{
        		    $crop = new Crop();    
        		}
                $crop->name = $name;
                $crop->name_hi = $name_hi;
        		$crop->name_gu = $name_gu;
                $crop->product_id = $pro;
                $url = env('APP_URL');
        		if($request->core_category){
        			$id = trim(strip_tags($request->core_category));
        			$crop->crop_category = $id;
        		}
        		$crop->crop_sub =$request->core_subcategory;
        		if($crop->save()){
        			if ($request->has('image')) {
        			    $file    = $request->image;
                        $time    = md5(time());
                        $profile = $file->getClientOriginalExtension();
                        $name = $file->getClientOriginalName();
                        $path    = "images/crop/" . $crop->id . "/file";
                        $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                        $profile_update          = Crop::find($crop->id);
                        $profile_update->image = $url."/".$path . "/" .$time.'.'.$profile;
                        $profile_update->save();
                    }
        		}
                if($id!=""){
                    return redirect()->route('admin.crops.show_sub', [$request->core_subcategory , $request->core_category] )->with('message', 'Sub-sub Solution update successfully');
                } else {
                    return redirect()->route('admin.crops.show_sub', $request->core_subcategory , $request->core_category)->with('message', 'Sub-sub solution added successfully');
        	        
                }
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		
    	}
    }
    public function subEdit($id,$catid,$subid){
        try {
            $permission = Permission::permission('crop');
            if($permission->add==1) {
                $subsubid = Crop::where('id', $id)->first();
                $crop = Crop::where('id', $catid)->first();
                $categories_selected = Crop::where('id', $subid)->first();
                $categories = Crop::where('crop_category', null)->get();
                // $product = Product::get();
                $product = Product::join('employees','employees.id','products.company_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*', 'employees.name as company', 'categories.name as category')
                            ->orderBy('id', 'DESC')
                            ->where('employees.status',1)
                            ->get();
                $technical = Technical::get();
                return view('admin.crop.create-sub',[
                    'categories' => $categories,
                    'categories_selected' => $categories_selected,
                    'subid' => $subid,
                    'crop' => $crop,
                    'technical' => $technical,
                    'products' => $product,
                    'subsubid'=> $subsubid
                ]);
            } else {
                return view('layouts.errors.403');
            }
        } catch (AttributeNotFoundException $e) {
            request()->session()->flash('error', 'The crop you are looking for is not found.');

            return redirect()->route('admin.crop.index');
        }
    }
      function checkSubsolutionname (Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $crop=$request->crop;
        $core_category=$request->core_category;
        $product = Crop::where(['name'=>$producntname,'crop_category'=>$crop])->count();
        if($product==0){
          echo "false";
           //echo "<span style='color: red;'>Company already exist.</span>";
        } else{
            echo "true";
        }
    }
      function checkEditSubsolutionname (Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $crop=$request->crop;
        $core_category=$request->core_category;
        $id=$request->id;
        $product = Crop::where(['name'=>$producntname,'crop_category'=>$core_category])->where('id','!=',$crop)->count();
        if($product==0){
          echo "false";
           //echo "<span style='color: red;'>Company already exist.</span>";
        } else{
            echo "true";
        }
    }
    public function getSubcat($id){
        try {
            $categories = Crop::where('crop_category', $id)->pluck('id','name');
            return $categories;
        } catch (AttributeNotFoundException $e) {
            request()->session()->flash('error', 'The crop you are looking for is not found.');

            return redirect()->route('admin.crop.index');
        }
    }


}
