<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Shop\Orders\Order;
use App\Shop\Products\Product;


class DashboardController extends Controller
{
    public function index()
    {
    	$orders = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
			            ->join('order_statuses','order_statuses.id', '=','orders.order_status_id' )
			            ->select('customers.name as customer_name', 'customers.mobile', 'order_statuses.name as status', 'order_statuses.color as color', 'customers.email', 'orders.*')
			            ->whereNull('customers.deleted_at')
			            ->orderBy('orders.id', 'DESC')
			            ->limit(5)
			            ->get();
        $stock = Product::join('employees','employees.id','products.company_id')
                        ->join('categories','categories.id','products.category_id')
                        ->select('products.*', 'employees.name as company', 'categories.name as category')
                        ->orderBy('id', 'DESC')
			            ->limit(5)
        				->get();
        return view('admin.dashboard',[
        	'orders' => $orders,
        	'products'  => $stock
        ]);
    }
}
