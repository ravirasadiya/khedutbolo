<?php

namespace App\Http\Controllers\Admin;

use App\Shop\Admins\Requests\CreateEmployeeRequest;
use App\Shop\Admins\Requests\UpdateEmployeeRequest;
use App\Shop\Employees\Repositories\EmployeeRepository;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Shop\Roles\Repositories\RoleRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\CompaniesEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Shop\Employees\Employee;
use App\Helper\Permission;
use App\Shop\Roles\Role;
use App\Shop\User;


class CompaniesController extends Controller
{
    /**
     * @var EmployeeRepositoryInterface
     */
    private $employeeRepo;
    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepo;

    /**
     * CompaniesController constructor.
     *
     * @param EmployeeRepositoryInterface $employeeRepository
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(
        EmployeeRepositoryInterface $employeeRepository,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->employeeRepo = $employeeRepository;
        $this->roleRepo = $roleRepository;
    }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = Permission::permission('company');
        if($permission->view==1) {
           // $list = $this->employeeRepo->listEmployees('id', 'desc');
$list =Employee::where('deleted_at',null)->orderBy('id','desc');
            return view('admin.companies.list', [
                //'employees' => $this->employeeRepo->paginateArrayResults($list->all()),
                'employees' => $list->paginate(10),
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }
      public function checkSearch(Request $request)
    {   
        
        if($request->ajax())
        {
            $output="";
            $i=1;
            $list =Employee::where('deleted_at',null)->where('name','LIKE','%'.$request->search."%")->orderBy('id','desc')->paginate(10);
            foreach ($list as $product) {
            $output.='<tr>'.
           // '<td>'.$i++.'</td>'.
            '<td>'.$product->name.'</td>'.
            '<td>'.$this->getStatus($product).'</td>'.
            /*if($product->status==1){
                  '<td><span style="display: none; visibility: hidden">1</span><button type="button" class="btn btn-success btn-sm status-pointer"><i class="fa fa-check"></i></button></td>'.
            }else{
                '<td><span style="display: none; visibility: hidden">0</span><button type="button" class="btn btn-danger btn-sm status-pointer" style=" width: 34px;"><i class="fa fa-times"></i></button></td>'.
            }*/
            
           
            '<td> 
             <form action="'.route("admin.companies.destroy", $product->id).'" method="post" class="form-horizontal">
           '. csrf_field().'<input type="hidden" name="_method" value="delete">  
            <div class="btn-group">
            <a href="'.route("admin.companies.edit", $product->id).'" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>'.$product->price.' <button onclick="return confirm("Are you sure?")" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button></td>'.
            '</div></form>
             </td>'.
            '</tr>';
            }
            return Response($output);
        }
    }
    
    function getStatus($product){
        if($product->status==1){
              return '<span style="display: none; visibility: hidden">1</span><button type="button" class="btn btn-success btn-sm status-pointer"><i class="fa fa-check"></i></button>';
        }
        else{
            return '<span style="display: none; visibility: hidden">0</span><button type="button" class="btn btn-danger btn-sm status-pointer" style=" width: 34px;"><i class="fa fa-times"></i></button>';
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::permission('company');
        if($permission->add==1) {
            // $roles = $this->roleRepo->listRoles();
            $roles = Role::orderBy('name', 'ASC')->get();

            return view('admin.companies.create', compact('roles'));
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateEmployeeRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        $permission = Permission::permission('company');
        if($permission->add==1) {
            $employee = $this->employeeRepo->createEmployee($request->all());
            
            // if ($request->has('role')) {
                // if ($request->hasFile('logo') && $request->file('logo') instanceof UploadedFile) {
                //     $employee['logo'] = $this->uploadOne($request->file('logo'), 'logo');
                //     $employee['logo'] = $this->employeeRepo->saveLogoImage($request->file('logo'));
                // }
                // $employeeRepo = new EmployeeRepository($employee);
                // $employeeRepo->syncRoles([$request->input('role')]);
                if ($request->hasFile('logo')) {
                    $url = env('APP_URL');
                    $file    = $request->logo;
                    $time    = md5(time());
                    $profile = $file->getClientOriginalExtension();
                    $name = $file->getClientOriginalName();
                    $path    = "storage/logo";
                    $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                    $profile_update          = Employee::find($employee->id);
                    $profile_update->logo = $url."/".$path.'/'.$time.'.'.$profile;
                    
                    $profile_update->save();
                    // dd($profile_update);
                }
               
            // }

            return redirect()->route('admin.companies.index')
                ->with('message', 'Company Added Successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $employee = $this->employeeRepo->findEmployeeById($id);
        return view('admin.companies.show', ['employee' => $employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $permission = Permission::permission('company');
        if($permission->edit==1) {
            $employee = $this->employeeRepo->findEmployeeById($id);
            $roles = Role::orderBy('name', 'ASC')->get();
            $isCurrentUser = $this->employeeRepo->isAuthUser($employee);
            
            return view(
                'admin.companies.edit',
                [
                    'employee' => $employee,
                    'roles' => $roles,
                    'isCurrentUser' => $isCurrentUser,
                    'selectedIds' => $employee->roles()->pluck('role_id')->all()
                ]
            );
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEmployeeRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $permission = Permission::permission('company');
        if($permission->edit==1) {
            $employee = $this->employeeRepo->findEmployeeById($id);
            $isCurrentUser = $this->employeeRepo->isAuthUser($employee);
            

            // if ($request->input('status') != $employee->status) {
            //     $data = $employee;
            //     $data['status'] = $request->input('status');
            //     Mail::to($request->input('email'))->send(new CompaniesEmail($data));
            // }

            // if ($request->hasFile('logo')) {
            //     $employee['logo'] = $this->employeeRepo->saveLogoImage($request->file('logo'));
            // }
            
            $empRepo = new EmployeeRepository($employee);
            $empRepo->updateEmployee($request->except('_token', '_method', 'password','logo'));
            
            
            if ($request->hasFile('logo')) {
                $url = env('APP_URL');
                $file    = $request->logo;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "storage/logo";
                $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                $profile_update          = Employee::find($id);
                $profile_update->logo = $url."/".$path.'/'.$time.'.'.$profile;
                
                $profile_update->save();
                // dd($profile_update->logo);
            }
            if ($request->has('password') && !empty($request->input('password'))) {
                $employee->password = Hash::make($request->input('password'));
                $employee->save();
            }

            if ($request->has('roles') and !$isCurrentUser) {
                $employee->roles()->sync($request->input('roles'));
            } elseif (!$isCurrentUser) {
                $employee->roles()->detach();
            }

            return redirect()->route('admin.companies.index')
                ->with('message', 'Company Updated Successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $permission = Permission::permission('company');
        if($permission->delete==1) {
            $employee = $this->employeeRepo->findEmployeeById($id);
            $employeeRepo = new EmployeeRepository($employee);
            $employeeRepo->deleteEmployee();

            return redirect()->route('admin.companies.index')->with('message', 'Company Deleted Successfully');
        } else {
            return view('layouts.errors.403');
        }
    }
    public function updateProfile(Request $request, $id)
    {
        $employee = User::where('id',$id)->first();

        // if ($request->hasFile('logo')) {
        //     $employee['logo'] = $this->employeeRepo->saveLogoImage($request->file('logo'));
        // }
        $user =  User::find($request->id);
        $user->name = $request->name;
        $user->name_hi = $request->name_hi;
        $user->name_gu = $request->name_gu;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->status = $user->status;
        if ($user->save()) {
            if ($request->hasFile('logo')) {
                $url = env('APP_URL');
                $file    = $request->logo;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "storage/logo";
                $file->move(public_path(env('IMAGE_URL').$path), $time.'.'.$profile);
                $profile_update          = User::find($user->id);
                $profile_update->logo = $url."/".$path.'/'.$time.'.'.$profile;
                $profile_update->save();
            }
        }
        // $update = new EmployeeRepository($employee);
        // $update->updateEmployee($request->except('_token', '_method', 'password'));

        // if ($request->has('password') && $request->input('password') != '') {
        //     $update->updateEmployee($request->only('password'));
        // }

        return redirect()->route('admin.dashboard')
            ->with('message', 'Update profile successfully');
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    // public function getProfile($id)
    // {
    //     $employee = $this->employeeRepo->findEmployeeById($id);
    //     return view('admin.companies.profile', ['employee' => $employee]);
    // }

    /**
     * @param UpdateEmployeeRequest $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
     public function getProfile($id)
    {
        $employee = User::where('id',$id)->first();
        return view('admin.companies.profile', ['employee' => $employee]);
    }


     function checkCompanyname(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $product = Employee::where('name',$producntname)->count();
        if($product==0){
          //  echo "false";
           echo "<span style='color: red;'>Company already exist.</span>";
        } else{
            echo "true";
        }
    }
    
     function checkEditCompanyname(Request $request)
    {
        $producntname = trim(strip_tags($request->name));
        $productid=$request->productid;
        $product = Employee::where('name',$producntname)->where('id','!=',$productid)->count();
        if($product==0){
          echo "false";
           //echo "<span style='color: red;'>Company already exist.</span>";
        } else{
            echo "true";
        }
    }
    public function removeLogo(Request $request)
    {
        $employee = $this->employeeRepo->findEmployeeById($request->employee);
        Storage::disk('public')->delete($employee->logo);
        $company = Employee::find($employee->id);
        $company->logo = null; 
        $company->save();
        request()->session()->flash('message', 'logo delete successful');
        return redirect()->route('admin.companies.edit', $request->input('employee'));
    }
}
