<?php

namespace App\Http\Middleware;

use Closure;
use App\Shop\Employees\Employee;
use App\Shop\Customers\Customer;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public $userdetails;
    public function handle($request, Closure $next)
    {
        if(isset($request->user_id))
        {
            $customer = Customer::where('id',$request->user_id)->where('status','1')->first();
            if($customer==null){
                echo json_encode(array("status" => 111,"message"=>"unauthorised user"));  
                exit();
            } else{
                $request->attributes->add(['userdetail'=>$customer]);
                return $next($request);
            }
        } else{
            echo json_encode(array("status" => 111,"message"=>"unauthorised user"));
            exit();
        }
    }
}
