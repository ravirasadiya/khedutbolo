<?php

namespace App\Shop\Products;

use App\Shop\Brands\Brand;
use App\Shop\Categories\Category;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\ProductImages\ProductImage;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model implements Buyable
{
    use SearchableTrait;

    public const MASS_UNIT = [
        // 'OUNCES' => 'oz',
        // 'GRAMS' => 'gms',
        // 'POUNDS' => 'lbs'
        'RUPEES' => 'RS'
    ];

    public const DISTANCE_UNIT = [
        'CENTIMETER' => 'cm',
        'METER' => 'mtr',
        'INCH' => 'in',
        'MILIMETER' => 'mm',
        'FOOT' => 'ft',
        'YARD' => 'yd'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'products.name' => 10,
            'products.description' => 5
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'id',
        'sku',
        'name',
        'name_hi',
        'name_gu',
        'description',
        'description_hi',
        'description_gu',
        'cover',
        'quantity',
        'price',
        'category_id',
        'subcategory_id',
        'crop_id',
        'crop_category',
        'subcropcategory_id',
        'status',
        'stock_status',
        'weight',
        'mass_unit',
        'status',
        'sale_price',
        'length',
        'width',
        'height',
        'distance_unit',
        'slug',
        'unit',
        'company_id',
        'additional_info',
        'additional_info_hi',
        'additional_info_gu',
        'technical_id',
        'technical_name',
        'available_quantity',
        'mail_status',
        'shipping_price',
        'hsn_code',
        'offer_title',
        'offer_product',
        'offer_product_image',
        'offer_quantity',
        'offer_title_hi',
        'offer_title_gu',
        'offer_product_hi',
        'offer_product_gu'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Get the identifier of the Buyable item.
     *
     * @param null $options
     * @return int|string
     */
    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @param null $options
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->name;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @param null $options
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @param string $term
     * @return Collection
     */
    public function searchProduct(string $term) : Collection
    {
        return self::search($term)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function employees()
    {
        return $this->hasOne('App\Shop\Employees\Employee','id','company_id');
    }
    
    public function emplysgrp()
    {
            return $this->employees()->selectRaw('id')->groupBy('id');
    }
     public function technicalname()
    {
      
        return $this->hasOne('App\Shop\TechnicalCategories\Technical','id','technical_id');
    }
     public function categoryname()
    {
        return $this->hasOne('App\Shop\Categories\Category','id','category_id')->select(['id','name']);
    }

    public function product_attributes()
    {
        return $this->hasMany('App\Shop\Products\Product_attributes','product_id','id');
    }
}
