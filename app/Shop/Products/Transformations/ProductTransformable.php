<?php

namespace App\Shop\Products\Transformations;

use App\Shop\Products\Product;
use Illuminate\Support\Facades\Storage;

trait ProductTransformable
{
    /**
     * Transform the product
     *
     * @param Product $product
     * @return Product
     */
    protected function transformProduct(Product $product)
    {
        $prod = new Product;
        $prod->id = (int) $product->id;
        $prod->name = $product->name;
        $prod->sku = $product->sku;
        $prod->slug = $product->slug;
        $prod->crop_id = $product->crop_id;
        $prod->crop_category = $product->crop_category;
        $prod->category_id = $product->category_id;
        $prod->subcategory_id = $product->subcategory_id;
        $prod->slug = $product->slug;
        $prod->description = $product->description;
        $prod->cover = $product->cover;
        $prod->quantity = $product->quantity;
        $prod->available_quantity = $product->available_quantity;
        $prod->price = $product->price;
        $prod->status = $product->status;
        $prod->stock_status = $product->stock_status;
        $prod->weight = (float) $product->weight;
        $prod->mass_unit = $product->mass_unit;
        $prod->sale_price = $product->sale_price;
        $prod->brand_id = (int) $product->brand_id;
        $prod->company_id = (int) $product->company_id;
        $prod->mail_status = (int)$product->mail_status;
        $prod->unit = $product->unit;
        $prod->technical_name = $product->technical_name;
        $prod->additional_info = $product->additional_info;
        $prod->shipping_price = $product->shipping_price;
        $prod->hsn_code = $product->hsn_code;
        $prod->offer_title = $product->offer_title;
        $prod->offer_product = $product->offer_product;
        $prod->offer_quantity = (int)$product->offer_quantity;
        $prod->offer_product_image = asset("storage/$product->offer_product_image");

        return $prod;
    }
}
