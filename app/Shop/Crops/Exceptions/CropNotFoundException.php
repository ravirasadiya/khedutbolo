<?php

namespace App\Shop\Crops\Exceptions;

class CropNotFoundException extends \Exception
{
}
