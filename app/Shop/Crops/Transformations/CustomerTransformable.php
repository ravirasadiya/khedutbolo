<?php

namespace App\Shop\Crops\Transformations;

use App\Shop\Crops\Crop;

trait CropTransformable
{
    protected function transformCrop(Crop $crop)
    {
        $prop = new Crop;
        $prop->id = (int) $crop->id;
        $prop->crop_category = $crop->crop_category;
        $prop->name = $crop->name;

        return $prop;
    }
}
