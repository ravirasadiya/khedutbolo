<?php

namespace App\Shop\Crops\Repositories;

use App\Shop\Addresses\Address;
use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Crops\Crop;
use App\Shop\Crops\Exceptions\CreateCropInvalidArgumentException;
use App\Shop\Crops\Exceptions\CropNotFoundException;
use App\Shop\Crops\Exceptions\CropPaymentChargingErrorException;
use App\Shop\Crops\Exceptions\UpdateCropInvalidArgumentException;
use App\Shop\Crops\Repositories\Interfaces\CropRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection as Support;

class CropRepository extends BaseRepository implements CropRepositoryInterface
{
    /**
     * CropRepository constructor.
     * @param Crop $crop
     */
    public function __construct(Crop $crop)
    {
        parent::__construct($crop);
        $this->model = $crop;
    }

    /**
     * List all the employees
     *
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return \Illuminate\Support\Collection
     */
    public function listCrops(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Support
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * Create the crop
     *
     * @param array $params
     * @return Crop
     * @throws CreateCropInvalidArgumentException
     */
    public function createCrop(array $params) : Crop
    {
        try {
            $data = collect($params)->all();
            $crop = new Crop($data);
            $crop->save();

            return $crop;
        } catch (QueryException $e) {
            throw new CreateCropInvalidArgumentException($e->getMessage(), 500, $e);
        }
    }

    /**
     * Update the crop
     *
     * @param array $params
     *
     * @return bool
     * @throws UpdateCropInvalidArgumentException
     */
    public function updateCrop(array $params) : bool
    {
        try {
            return $this->model->update($params);
        } catch (QueryException $e) {
            throw new UpdateCropInvalidArgumentException($e);
        }
    }

    /**
     * Find the crop or fail
     *
     * @param int $id
     *
     * @return Crop
     * @throws CropNotFoundException
     */
    public function findCropById(int $id) : Crop
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CropNotFoundException($e);
        }
    }

    /**
     * Delete a crop
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteCrop() : bool
    {
        return $this->delete();
    }

    /**
     * @param string $text
     * @return mixed
     */
    public function searchCrop(string $text = null) : Collection
    {
        if (is_null($text)) {
            return $this->all();
        }
        return $this->model->searchCrop($text)->get();
    }

}
