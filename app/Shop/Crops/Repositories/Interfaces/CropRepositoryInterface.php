<?php

namespace App\Shop\Crops\Repositories\Interfaces;

use App\Shop\Addresses\Address;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Crops\Crop;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Support;

interface CropRepositoryInterface extends BaseRepositoryInterface
{
    public function listCrops(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Support;

    public function createCrop(array $params) : Crop;

    public function updateCrop(array $params) : bool;

    public function findCropById(int $id) : Crop;

    public function deleteCrop() : bool;

    public function searchCrop(string $text) : Collection;
}
