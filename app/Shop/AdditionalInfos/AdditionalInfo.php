<?php

namespace App\Shop\AdditionalInfos;

use Illuminate\Database\Eloquent\Model;

class AdditionalInfo extends Model
{
    public $table = "additional_infos";
    protected $fillable = [
        'name'
    ];
}
