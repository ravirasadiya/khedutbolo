<?php

namespace App\Shop\OrderProducts;

use App\Shop\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = "order_product";
    protected $fillable = [
        'order_id',
        'product_id',
        'attribute_id',
        'quantity',
        'product_name',
        'product_sku',
        'product_description',
        'product_price',
        'shipping_price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
