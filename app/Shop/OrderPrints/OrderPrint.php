<?php

namespace App\Shop\OrderPrints;

use Illuminate\Database\Eloquent\Model;

class OrderPrint extends Model
{
    public $table = 'order_print';
    public $timestamps = false;
    protected $fillable = [
        'order_id'
    ];
}
