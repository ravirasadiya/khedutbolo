<?php

namespace App\Shop\Customers\Transformations;

use App\Shop\Customers\Customer;

trait CustomerTransformable
{
    protected function transformCustomer(Customer $customer)
    {
        $prop = new Customer;
        $prop->id = (int) $customer->id;
        $prop->name = $customer->name;
        $prop->email = $customer->email;
        $prop->mobile = $customer->mobile;
        $prop->status = (int) $customer->status;
        $prop->state_id = (int) $customer->states;
        $prop->city_id = (int) $customer->cities;
        $prop->tehsil_id = (int) $customer->tehsils;
        $prop->village_id = (int) $customer->villages;
        $prop->address = $customer->address;

        return $prop;
    }
}
