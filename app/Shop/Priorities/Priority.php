<?php

namespace App\Shop\Priorities;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $fillable = [
        'priority',
        'name',
        'category_id',
        'subcategory_id'
    ];
}
