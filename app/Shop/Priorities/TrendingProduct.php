<?php

namespace App\Shop\Priorities;

use Illuminate\Database\Eloquent\Model;

class TrendingProduct extends Model
{
	public $table = "trending_category";
    protected $fillable = [
        'priority',
        'product_id'
    ];
}
