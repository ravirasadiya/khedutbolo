<?php

namespace App\Shop\Offers\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateOfferRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer' => ['required'],
            'offer_type' => ['required'],
            'expired_date' => ['required'],
            'expired_time' => ['required'],
            'cover' => ['required', 'file', 'image:png,jpeg,jpg,gif']
        ];
    }
}
