<?php

namespace App\Shop\Offers\Repositories\Interfaces;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Offers\Offer;
use Illuminate\Support\Collection;
use Illuminate\Http\UploadedFile;

interface OfferRepositoryInterface extends BaseRepositoryInterface
{
    public function listOffers(string $order = 'id', string $sort = 'desc') : Collection;

    public function createOffer(array $params) : Offer;

    public function findOfferById(int $id) : Offer;

    public function updateOffer(array $params): bool;

    public function saveCoverImage(UploadedFile $file) : string;
    
    public function deleteFile(array $file, $disk = null) : bool;

    public function deleteOffer() : bool;
}
