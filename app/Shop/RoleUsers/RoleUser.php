<?php

namespace App\Shop\RoleUsers;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
	public $table = "role_user";
    protected $fillable = [
        'role_id',
        'user_id',
        'user_type'
    ];
    public $timestamps = false;

}
