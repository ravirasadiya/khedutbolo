<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Invoice</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <style type="text/css">
        table { border-collapse: collapse;}
    </style>
</head>
<body>
    <!-- <section class="row"> -->
 <!--        <div style="margin-top:3%; text-align: center">
            <img class="logofull" src="{{ 'http://khedutbolo.shineinfosoft.in/khedutBolo_logo_40.png' }}" width="auto" height="100px">
        </div>
    </section> -->
    <section class="row">
        <div class="col-md-12">
            <p style="margin-left: 22px; font-size: 16px!important;">Order details are here</p>
            <table class="table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td style="width: 10%;margin-left: 50px;"><strong> Name</strong></td>
                        <td style="width: 90%;margin-left: 5px;">{{$data['customer']['name']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 10%;margin-left: 50px;"><strong> Email</strong></td>
                        <td style="width: 90%;margin-left: 5px;">{{$data['email']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 10%;margin-left: 50px;"><strong> Mobile</strong></td>
                        <td style="width: 90%;margin-left: 5px;">{{$data['customer']['mobile']}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <h3 style="text-align: center;">Product Details</h3>
            <table class="table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">HSN Code</th>
                        <th style="width: 15%; border: 1px solid black; border-collapse: collapse;text-align: center;">Name</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Unit</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Quantity</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Price</th>
                        <th style="width: 15%; border: 1px solid black; border-collapse: collapse;text-align: center;">Shipping Price</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Tax</th>
                        <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Total</th>
                        @if($data['offer_status']==1)
                            <th style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">Offer Product</th>
                        @endif
                        
                    </tr>
                </thead>
                <tbody>
                @php
                    $tq=0;
                    $grand_total=0;
                @endphp

                @foreach($data['products'] as $product)
                    @php 
                        $tq=$tq+$product->quantity;
                        $product->product_price = str_replace(",","",$product->product_price);
                        $product_price = number_format($product->product_price, 2);
                        $product_price = str_replace(",","",$product_price);
                        $shipping_price = number_format($product->shipping_price, 2);
                        $gst = ($product_price*$product->quantity*$product->gst_price)/100;
                        $total = ($product_price*$product->quantity)+$product->shipping_price+$gst;
                        $gst = number_format($gst, 2);
                        $total = number_format($total, 2);
                        $total = str_replace(",","",$total);
                        $grand_total = $grand_total+$total;
                    @endphp
                    <tr>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->hsn_code}}</td>
                        <td style="width: 15%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->product_name}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse; margin-left:10px;text-align: center;">{{$product->unit}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{$product->quantity}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$product_price}}</td>
                        <td style="width: 15%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$shipping_price}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$gst}}</td>
                        <td style="width: 10%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$total}}</td>
                        @if($data['offer_status']==1)
                            <td style="width: 10%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->offer_product}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box">
            <div class="box-body">
                <h3 style="text-align: center;">Order Information</h3>
                <table class="table  table-bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Date</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Customer</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Quantity</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Payment</strong></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;"><strong>Status</strong></td>
                    </thead>
                    <tbody>
                        <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ date('M d, Y', strtotime($data['order']['created_at'])) }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $data['customer']['name'] }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ $tq }}</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;"><strong>{{ $data['order']['payment'] }}</strong></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{ucfirst($data['statuses'])}}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Subtotal:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{number_format($data['order']['total_products'],2)}}</td>
                    </tr>
                    @if($data['order']['wallet_point']!=0.0 && $data['order']['wallet_point']!="")
                        @php
                            $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                            $wallet_amount = number_format($wallet_amount, 2);
                        @endphp
                        <tr>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Discounts:</td>
                            <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{number_format($wallet_amount,2)}}</td>
                        </tr>
                    @endif
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Tax:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{number_format($data['order']['tax'],2)}}</td>
                    </tr>
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">Shipping Price:</td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{number_format($data['order']['total_shipping'],2)}}</td>
                    </tr>
                    @php 
                        $wallet_amount = 0;
                        if($data['order']['wallet_point']!=""){
                            $wallet_amount = $data['order']['wallet_point'] * env('DISCOUNT_POINT');
                        }
                        if (isset($wallet_amount)) {
                            $total1 = $data['order']['total'] + $data['order']['total_shipping'] + $data['order']['tax'] - $wallet_amount;
                            $total1 = number_format($total1, 2);
                        } else{
                            $total1 = $data['order']['total'] + $data['order']['total_shipping'] +$data['order']['tax'] ;
                            $total1 = number_format($total1, 2);
                        }
                    @endphp
                    <tr>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;"></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse; margin-left:10px;"><strong>Total:</strong></td>
                        <td style="width: 20%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;"><strong>Rs {{ $total1 }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </section>
</body>
</html>
                    