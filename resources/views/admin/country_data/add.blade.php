@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        @if(isset($tehsil))
                        <h4 class="page-title header-color"> Edit Tehsile</h4>
                        @else
                        <h4 class="page-title header-color"> Add Tehsile</h4>
                        @endif
                        
                    </div>
                </div>
                @if(isset($tehsil))
                <form action="{{ route('admin.country_data.update_tehsil') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                @else
                <form action="{{ route('admin.country_data.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                @endif
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state_id">State <span class="text-danger">*</span></label>
                                            <select name="state_id" id="state_id" class="form-control">
                                                @if($list)   
                                                    <option value="">Select State</option>
                                                    @foreach($list as $state)
                                                        <option value="{{ $state->STCode }}" @if(isset($tehsil->STCode)) @if($tehsil->STCode==$state->STCode) selected @endif @endif>{{ $state->name }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                            <input type="hidden" name="state_selected" id="state_selected" value="@if(isset($tehsil)) {{$tehsil->STCode}} @endif">
                                            <input type="hidden" name="disritct_selected" id="disritct_selected" value="@if(isset($tehsil)){{$tehsil->DTCode}}@endif">
                                            <input type="hidden" name="tehsil_id" id="tehsil_id" value="@if(isset($tehsil)){{$tehsil->SDTCode}}@endif">
                                            <input type="hidden" name="village_id" id="village_id" value="@if(isset($tehsil)){{$tehsil->TVCode}}@endif">
                                            <input type="hidden" name="tehsil" id="tehsil" value="@if(isset($tehsil)) 1 @else 0 @endif">
                                          
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="city_id">Disritct <span class="text-danger">*</span></label>
                                            <select name="city_id" id="city_id" class="form-control">
                                                <option value="">Select Disritct </option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">Tehsil <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Tehsil name" class="form-control"  maxlength="25" value="@if(isset($tehsil)) {{$tehsil->name}} @endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                            <span id="uname_response"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">तहसील <span class="text-danger">*</span></label>
                                            <input type="text" name="name_hi" id="name_hi" class="form-control"  maxlength="25" value="@if(isset($tehsil)) {{$tehsil->SDTName_hi}} @endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">તહસીલ <span class="text-danger">*</span></label>
                                            <input type="text" name="name_gu" id="name_gu" class="form-control"  maxlength="25" value="@if(isset($tehsil)) {{$tehsil->SDTName_gu}} @endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.country_data.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            @if(isset($tehsil))
                            <button type="submit" id="add_submit" class="btn btn-primary">Update </button>
                            @else
                            <button type="submit" id="add_submit" class="btn btn-primary">Add </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
  $("#name").keyup(function(){
                  var username = $(this).val().trim();
                   var state_selected =$('#state_id').val();
                   var disritct_selected=$('#city_id').val();
                   var tehsil_id=$('#tehsil_id').val();
                    var tehsil =$('#tehsil').val();
                   if(username != '' && tehsil!=''){
                       console.log('erer');
                        $.ajax({
                        url: "{{route('admin.country_data.checkEditTehsilname')}}",
                        type: 'get',
                        data: {name: username,STCode:state_selected,DTCode:disritct_selected,SDTCode:tehsil_id},
                        success: function(response){
                            
                            if(response=='true'){
                               $('#uname_response').html("<span style='color: red;'>Tehsile already exist.</span>");
                               $("#name").focus();
                               $('#add_submit').attr('disabled', true);
                               return false;
                            }
                             else{
                                $("#uname_response").empty();
                              $('#add_submit').removeAttr('disabled');
                            }
            
                         }
                     });
                   }
                  if(username != '' && tehsil!=1){
                     console.log('add');
                     $.ajax({
                        url: "{{route('admin.country_data.checkTehsilname')}}",
                        type: 'get',
                        data: {name: username,STCode:state_selected,DTCode:disritct_selected},
                        success: function(response){
                            
                            if(response=='true'){
                               $('#uname_response').html("<span style='color: red;'>Tehsile already exist.</span>");
                               $("#name").focus();
                               $('#add_submit').attr('disabled', true);
                               return false;
                            }
                             else{
                                $("#uname_response").empty();
                              $('#add_submit').removeAttr('disabled');
                            }
            
                         }
                     });
                  }else{
                   $("#uname_response").empty();
                    $('#add-submit').removeAttr('disabled');
                  }

             });
    $("#add_technical").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            state_id: {
                required: true,
            },
            city_id:{
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter tehsil name",
                maxlength: "Please enter max 25 character",
            },
            "state_id":{
                required: "Please select state",
            },
            "city_id":{
                required: "Please select disritct",
            }
        },
        submitHandler:  function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    $("#translate").on('click',function(){
        $("#add_submit").css('display','block');
        var name = $("#name").val();
        var tempArray = {};
        tempArray["name"] = name;
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data:  tempArray,
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                for (myObj in jsonObj) {
                    var tempName = String(myObj); 
                    $("."+tempName+"_hi").val(jsonObj[myObj].hi);
                    $("."+tempName+"_gu").val(jsonObj[myObj].gu);
                } 
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
    
    $('#state_id').change(function(){
        $("#city_id option").remove();
        var id = $(this).val();
        getCity(id);
    }); 
    if($('#state_selected').val()!=""){
        getCity($('#state_selected').val());
        
    }
    function getCity(id){ 
        var disritct_selected = $("#disritct_selected").val();
        console.log(disritct_selected);
        $.ajax({
            url : "{{route('admin.country_data.getCity')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            // dataType: 'json',
            success: function( result )
            {
                console.log(result);
                $('#city_id').empty();
                $('#city_id').append($('<option>', {value:'', text:'Select Disritct'}));
                
                $.each( result, function(k, v) {
                    console.log(disritct_selected==k);
                    if(disritct_selected==k){
                        console.log(k);
                        $('#city_id').append('<option value="'+ k +'" selected="true">' + v + '</option>');
                    } else{
                        $('#city_id').append($('<option>', {value:k, text:v}));
                    }
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    }
</script>
@endsection
