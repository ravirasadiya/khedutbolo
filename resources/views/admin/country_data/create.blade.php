@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        @if(isset($village))
                        <h4 class="page-title header-color"> Update Village</h4>
                        @else
                        <h4 class="page-title header-color"> Add Village</h4>
                        @endif
                        
                    </div>
                </div>
                @if(isset($village))
                <form action="{{ route('admin.country_data.village_update') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                @else
                <form action="{{ route('admin.country_data.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                @endif
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state_id">State <span class="text-danger">*</span></label>
                                            <select name="state_id" id="state_id" class="form-control">
                                                @if($list)   
                                                    <option value="">Select State</option>
                                                    @foreach($list as $state)
                                                        <option value="{{ $state->STCode }}" @if(isset($village->STCode)) @if($village->STCode==$state->STCode) selected @endif @endif>{{ $state->name }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="state_selected" id="state_selected" value="@if(isset($village)) {{$village->STCode}} @endif">
                                <input type="hidden" name="disritct_selected" id="disritct_selected" value="@if(isset($village)){{$village->DTCode}}@endif">
                                <input type="hidden" name="tehsil_id_selected" id="tehsil_id_selected" value="@if(isset($village)){{$village->SDTCode}}@endif">
                                <input type="hidden" name="village_id" id="village_id" value="@if(isset($village)){{$village->TVCode}}@endif">
                                <input type="hidden" name="village" id="village" value="@if(isset($village)) 1 @else 0 @endif">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="city_id">Disritct <span class="text-danger">*</span></label>
                                            <select name="city_id" id="city_id" class="form-control">
                                                <option value="">Select Disritct</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="tehsil_id">Tehsil <span class="text-danger">*</span></label>
                                            <select name="tehsil_id" id="tehsil_id" class="form-control">
                                                <option value="">Select Tehsil</option>
                                            </select>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">Village <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Village" class="form-control"  maxlength="25" value="@if(isset($village)){{$village->Name}}@endif">
                                            
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                            
                                            <span id="uname_response"></span>
                                        </div>
                                    </div>
                                     <div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">गाँव <span class="text-danger">*</span></label>
                                            <input type="text" name="name_hi" id="name_hi" placeholder="Village" class="form-control"  maxlength="25" value="@if(isset($village)){{$village->Name_hi}}@endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                     <div class="col-sm-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">ગામ <span class="text-danger">*</span></label>
                                            <input type="text" name="name_gu" id="name_gu" placeholder="Village" class="form-control"  maxlength="25" value="@if(isset($village)){{$village->Name_gu}}@endif">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.country_data.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            @if(isset($village))
                            <button type="submit" class="btn btn-primary">Update </button>
                            @else
                            <button type="submit" class="btn btn-primary">Add </button>
                            @endif
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#add_technical").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            state_id: {
                required: true,
            },
            city_id:{
                required: true,
            },
            tehsil_id:{
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter tehsil name",
                maxlength: "Please enter max 25 character",
            },
            "state_id":{
                required: "Please select state",
            },
            "city_id":{
                required: "Please select disritct",
            },
            "tehsil_id":{
                required: "Please select tehsil",
            }
        },
        submitHandler:  function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    $("#translate").on('click',function(){
        $("#add_submit").css('display','block');
        var name = $("#name").val();
        var tempArray = {};
        tempArray["name"] = name;
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data:  tempArray,
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                for (myObj in jsonObj) {
                    var tempName = String(myObj); 
                    $("."+tempName+"_hi").val(jsonObj[myObj].hi);
                    $("."+tempName+"_gu").val(jsonObj[myObj].gu);
                } 
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
    $('#state_id').change(function(){
        $("#city_id option").remove();
        var id = $(this).val();
        getCity(id);
    }); 
    if($('#state_selected').val()!=""){
        getCity($('#state_selected').val());
        
    }
    function getCity(id){ 
        var disritct_selected = $("#disritct_selected").val();
        console.log(disritct_selected);
        $.ajax({
            url : "{{route('admin.country_data.getCity')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            // dataType: 'json',
            success: function( result )
            {
                console.log(result);
                $('#city_id').empty();
                $('#city_id').append($('<option>', {value:'', text:'Select Disritct'}));
                
                $.each( result, function(k, v) {
                    console.log(disritct_selected==k);
                    if(disritct_selected==k){
                        $('#city_id').append('<option value="'+ k +'" selected="true">' + v + '</option>');
                    } else{
                        $('#city_id').append($('<option>', {value:k, text:v}));
                    }
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    }
    
    $('#city_id').change(function(){
        $("#tehsil_id option").remove();
        var id = $(this).val();
        getTehsil(id);
    });
    if($('#disritct_selected').val()!=""){
        getTehsil($('#disritct_selected').val());
    }
    function getTehsil(id){
        var tehsil_id_selected = $('#tehsil_id_selected').val();
        $.ajax({
            url : "{{route('admin.country_data.getTehsil')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            // dataType: 'json',
            success: function( result )
            {
                $('#tehsil_id').append($('<option>', {value:'', text:'Select Tehsil'}));
                $.each( result, function(k, v) {
                    if(tehsil_id_selected==k){
                        $('#tehsil_id').append('<option value="'+ k +'" selected="true">' + v + '</option>');
                    }else{
                        $('#tehsil_id').append($('<option>', {value:k, text:v}));    
                    }
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    }
     $("#name").keyup(function(){
         
                  var username = $(this).val().trim();
                   var state_selected =$('#state_id').val();
                   var disritct_selected=$('#city_id').val();
                     var tehsil_selected=$('#tehsil_id').val();
                     var village= $('#village').val();
                     var village_id = $('#village_id').val();
                    console.log(village);
                      if(username != '' && village==1){
              console.log('if');
                     $.ajax({
                        url: "{{route('admin.country_data.checkEditVillagename')}}",
                        type: 'get',
                        data: {name: username,STCode:state_selected,DTCode:disritct_selected,SDTCode:tehsil_selected,TVCode:village_id},
                        success: function(response){
                            
                            if(response=='true'){
                               $('#uname_response').html("<span style='color: red;'>Village already exist.</span>");
                               $("#name").focus();
                               $('.btn-primary').attr('disabled', true);
                               return false;
                                
                            }
                             else{
                                $("#uname_response").empty();
                                $('.btn-primary').removeAttr('disabled');
                            }
            
                         }
                     });
                  }
                  if(username != '' && village==0){
            
                     $.ajax({
                        url: "{{route('admin.country_data.checkVillagename')}}",
                        type: 'get',
                        data: {name: username,STCode:state_selected,DTCode:disritct_selected,SDTCode:tehsil_selected},
                        success: function(response){
                            
                            if(response=='true'){
                               $('#uname_response').html("<span style='color: red;'>Village already exist.</span>");
                               $("#name").focus();
                               $('.btn-primary').attr('disabled', true);
                               return false;
                            }
                             else{
                                $("#uname_response").empty();
                                $('.btn-primary').removeAttr('disabled');
                            }
            
                         }
                     });
                  }else{
                   $("#uname_response").empty();
                   $('.btn-primary').removeAttr('disabled');
                  }

             });
</script>
@endsection
