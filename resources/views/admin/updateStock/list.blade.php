@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$products->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Update Stock</h4>
                        </div>
                    </div>
                    @if(!$products->isEmpty())
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Technical</th>
                                <th>Category</th>
                                <th>Company</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $product->name }} </td>
                                    <td>{{ $product->technical_name }}</td>
                                    <td>{{ $product->category }}</td>
                                    <td>{{ $product->company }}</td>
                                    <td>@include('layouts.status', ['status' => $product->status])</td>
                                    <td>
                                        @if($permission->edit==1)
                                            <div class="btn-group">
                                                <a href="{{ route('admin.updateStock.edit', $product->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                    <div class="box-footer">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No products found.</p></div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')

<script type="text/javascript">
    $("#sort_change").on('change',function(){
        var sort = $("#sort_change").val();
        window.location.href = "{{ request()->url() }}?sort="+sort;
        var url = window.location.href;
        if(url == "{{ request()->url() }}"){
            window.location.href = "{{ request()->url() }}?sort="+sort;
        }
        if(url.includes("{{ request()->url() }}?product_no=")) {
            var product_no = "{{request()->input('product_no')}}";
            window.location.href = "{{ request()->url() }}?product_no="+product_no+"&&sort="+sort;
        }
        if(url.includes("{{ request()->url() }}?sort=")) {
            var product_no = "{{request()->input('product_no')}}";
            window.location.href = "{{ request()->url() }}?sort="+sort+"&&product_no="+product_no;
        }
    });

    $("#page_change").on('change',function(){
        var product_no = $("#page_change").val();
        var url = window.location.href;
        if(url == "{{ request()->url() }}"){
            window.location.href = "{{ request()->url() }}?product_no="+product_no;
            console.log(window.location.href);
        }
        if(url.includes("{{ request()->url() }}?sort=")) {
            var sort = "{{request()->input('sort')}}";
            window.location.href = "{{ request()->url() }}?sort="+sort+"&&product_no="+product_no;
            console.log(window.location.href);
        }
        if(url.includes("{{ request()->url() }}?product_no=")) {
            var sort = "{{request()->input('sort')}}";
            window.location.href = "{{ request()->url() }}?product_no="+product_no+"&&sort="+sort;
            console.log(window.location.href);
        }
    });
    $('#table-id').DataTable({});
</script>

@endsection
