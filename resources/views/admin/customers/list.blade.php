@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($customers)
            <div class="box">

                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Customer</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px"  id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-1">ID</th>
                                    <th class="col-md-2">Name</th>
                                    <th class="col-md-2">Email</th>
                                    <th class="col-md-2">Mobile</th>
                                    <th class="col-md-2">Status</th>
                                    <th class="col-md-3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($customers as $customer)
                                <tr>
                                    <td>{{ $customer['id'] }}</td>
                                    <td>{{ $customer['name'] }}</td>
                                    <td>{{ $customer['email'] }}</td>
                                    <td>{{ $customer['mobile'] }}</td>
                                    <td>@include('layouts.status', ['status' => $customer['status']])</td>
                                    <td>
                                        <form action="{{ route('admin.customers.destroy', $customer['id']) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.customers.show', $customer['id']) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                                @if($permission->edit==1)
                                                    <a href="{{ route('admin.customers.edit', $customer['id']) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                @endif
                                                @if($permission->delete==1)
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                                @endif
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $customers->links() }}
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <!-- /.box -->
            @else
                <div class="box">
                    <div class="box-body"><p class="alert alert-warning">No customers found.</p></div>
                    <div class="box-footer">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            @endif

    </section>
    <!-- /.content -->
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection