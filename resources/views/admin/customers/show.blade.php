@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Customer</h4>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table border-b1px">
                        <thead>
                        <tr>
                            <th class="col-md-3">ID</th>
                            <th class="col-md-3">Name</th>
                            <th class="col-md-3">Email</th>
                            <th class="col-md-3">Mobile Number</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $customer->id }}</td>
                            <td>{{ $customer->name }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->mobile }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div
            </div>
            <div class="box-body">
                <h4>Addresses</h4>
                <table class="table border-b1px">
                    <thead>
                    <tr>
                        <th>Alias</th>
                        <th>Address 1</th>
                        <th>Village</th>
                        <th>Tehsil</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Country</th>
                        <!-- <td class="col-md-2">Actions</td> -->
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $addresses->alias }}</td>
                            <td>{{ $addresses->address_1 }}</td>
                            <td>{{ $addresses->village_id }}</td>
                            <td>{{ $addresses->tehsil_id }}</td>
                            <td>{{ $addresses->city }}</td>
                            <td>{{ $addresses->state_code }}</td>
                            <td>{{ $addresses->country->name }}</td>
                            <!--<td>@include('layouts.status', ['status' => $addresses->status])</td>-->
                            <!-- <td>
                                <form action="{{ route('admin.addresses.destroy', $addresses->id) }}" method="post" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <div class="btn-group">
                                        <a href="{{ route('admin.customers.addresses.show', [$customer->id, $addresses->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> Show</a>
                                        <a href="{{ route('admin.customers.addresses.edit', [$customer->id, $addresses->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                                    </div>
                                </form>
                            </td> -->
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.customers.index') }}" class="btn btn-default btn-sm">Back</a>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
