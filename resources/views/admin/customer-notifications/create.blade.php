@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Send Notification </h4>
                </div>
            </div>
           
            <div class="box-body">
                 @include('layouts.errors-and-messages')
                <form action="{{ route('admin.notifications.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_notification">
                <div class="box-body">
                    <div class="row">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <input type="hidden" name="id" value="@if(isset($notification->id)) {{$notification->id}} @endif">
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="name">Customer <span class="text-danger">*</span></label>
                                        <select name="customer_type" id="customer_type" class="form-control">
                                            <option value="">Select Notification Type</option>
                                            <option value="single_customer">Single Customer</option>
                                            <option value="all_customer">All Customer</option>
                                            <option value="state_customer">State Customer</option>
                                            <option value="city_customer">City Customer</option>
                                            <option value="tehsil_customer">Tehsil Customer</option>
                                            <option value="village_customer">Village Customer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group" style="display: none;" id="state">
                                        <label for="states" id="state_customers"> </label>
                                        <select name="states" id="states" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group" id="city" style="display: none;">
                                        <label for="cities">District </label>
                                        <select name="cities" id="cities" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group" id="tehsils" style="display: none;">
                                        <label for="tehsil">Tehsil </label>
                                        <select name="tehsil" id="tehsil" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group" id="village" style="display: none;">
                                        <label for="villages">Village </label>
                                        <select name="villages" id="villages" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="msg">Message <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="msg" id="msg" rows="5" placeholder="Message" maxlength="200">@if(isset($notification)) {!! $notification->msg  !!} @else {{ old('msg') }} @endif</textarea>
                                        <span class="text-danger" id="en_error" style="color: red"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6" id="hindiarea" style="display: none;">
                                    <div class="form-group">
                                        <label for="msg_hi">संदेश <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="msg_hi" id="msg_hi" rows="5" placeholder="संदेश" maxlength="200">@if(isset($notification)) {!! $notification->msg_hi  !!} @else {{ old('msg_hi') }} @endif</textarea>
                                        <span class="text-danger" id="hi_error" style="color: red"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6" id="gujaratiarea" style="display: none;">
                                    <div class="form-group">
                                        <label for="msg_gu">સંદેશ <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="msg_gu" id="msg_gu" rows="5" placeholder="સંદેશ" maxlength="200">@if(isset($notification)) {!! $notification->msg_gu  !!} @else {{ old('msg_gu') }} @endif</textarea>
                                        <span class="text-danger" id="gu_error" style="color: red"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="radio" name="lang" value="English" id="englishid" checked>
                                    <label for="englishid">English</label>
                                    <input type="radio" name="lang" value="Hindi" id="hindiid">
                                    <label for="hindiid">Hindi</label>
                                    <input type="radio" name="lang" value="Gujarati" id="gujaratiid">
                                    <label for="gujaratiid">Gujarati</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.dashboard') }}" class="btn btn-default">Cancel</a>
                        <input type="button" class="btn btn-info" value="Translate" id="translate">
                        <button type="submit" id="submit" class="btn btn-primary">@if(!isset($notification)) Send @else Update @endif</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#add_notification").validate({
        validClass: "validation-valid-label",
        rules: {
            company_id: {
                required: true,
            },
            customer_type: {
                required: true,
            },
            status: {
                required: true,
            },
            msg: {
                required: true,
            }
        },
        messages: {
            "company_id":{
                required: "Please select company.",
            },
            "customer_type":{
                required: "Please select notification type.",
            },
            "status":{
                required: "Please select status.",
            },
            "msg":{
                required: "Please enter message.",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

    $('input[name=lang]').on('change',function(){
        var lang = $(this).val();
        if(lang=="Hindi"){
            $("#gujaratiarea").css('display','none');
            $("#hindiarea").css('display','block');
        } else if(lang=="Gujarati"){
            $("#hindiarea").css('display','none');
            $("#gujaratiarea").css('display','block');
        } else{
            $("#hindiarea").css('display','none');
            $("#gujaratiarea").css('display','none');
        }
    });

    $("#submit").on('click',function(e){
        $("#en_error").html('');
        $("#hi_error").html('');
        $("#gu_error").html('');
        var langvalue = $('input[name=lang]:checked').val();
        var areaMessage = null;
        if(langvalue=="English"){
            areaMessage = $("#msg").val();
        }
        if(langvalue=="Hindi"){
            areaMessage = $("#msg_hi").val();
        }   
        if(langvalue=="Gujarati"){
            areaMessage = $("#msg_gu").val();
        }     
        if(areaMessage.trim()==""){
            if(langvalue=="English"){
                $("#msg").focus();
                $("#en_error").html('Please enter message');
            }
            if(langvalue=="Hindi"){
                $("#msg_hi").focus();
                $("#hi_error").html('Please enter message');
            }   
            if(langvalue=="Gujarati"){
                $("#msg_gu").focus();
                $("#gu_error").html('Please enter message');
            }  
            e.preventDefault();
        }else{
            console.log("Reaady");
        }
    });

    $('#customer_type').change(function(){
        var value=$(this).val();
        $('#village').hide();
        $('#city').hide();
        $('#tehsils').hide();
        $('#state').hide();
        $('#state-customer').hide();
        if(value!="all_customer"){
            if(value=="single_customer"){   
                document.getElementById('state_customers').innerHTML="Customer Name";
                value= $(this).val();
            } else {
                document.getElementById('state_customers').innerHTML="State";
                value="state_customer";
            }
            $.ajax({
                url:"{{route('admin.notifications.getValue')}}",
                data:{
                        "_token": "{{ csrf_token() }}",
                        "value": value,
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $("#states option").remove();
                    $('#states').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#states').append($('<option>', {value:k, text:v}));
                    });
                    $('#state').show();
                },
                error: function()
                {
                    alert('error...');
                }
            });
        }
    });

    $('#states').change(function(){
        var values = $('#customer_type').val();
        var state = $('#states').val();
        console.log(state);
        var value = "city_customer";
        console.log(value);
        if(!(values=="state_customer" || values=="single_customer")){
            $.ajax({
                url:"{{route('admin.notifications.getValue')}}",
                data:{
                        "_token": "{{ csrf_token() }}",
                        "value": value,
                        "state": state,
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $("#cities option").remove();
                    $('#cities').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#cities').append($('<option>', {value:k, text:v}));
                    });
                    $('#city').show();
                },
                error: function()
                {
                    alert('error...');
                }
            });
        }
    });
    $('#cities').change(function(){
        var value = "tehsil_customer";
        var city = $('#cities').val();
        var values = $('#customer_type').val();
        if(values!="city_customer"){
            $.ajax({
                url:"{{route('admin.notifications.getValue')}}",
                data:{
                        "_token": "{{ csrf_token() }}",
                        "value": value,
                        "city": city,
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $("#tehsil option").remove();
                    $('#tehsil').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#tehsil').append($('<option>', {value:k, text:v}));
                    });
                    $('#tehsils').show();
                },
                error: function()
                {
                    alert('error...');
                }
            });
        }
    });
    $('#tehsils').change(function(){
        var value = $('#customer_type').val();
        var tehsil = $('#tehsil').val();
        var values = $('#customer_type').val();
        if(values!="tehsil_customer"){
            $.ajax({
                url:"{{route('admin.notifications.getValue')}}",
                data:{
                        "_token": "{{ csrf_token() }}",
                        "value": value,
                        "tehsil": tehsil,
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $("#villages option").remove();
                    $('#villages').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#villages').append($('<option>', {value:k, text:v}));
                    });
                    $('#village').show();
                },
                error: function()
                {
                    alert('error...');
                }
            });
        }
    });
    $("#translate").on('click',function(){
        $("#submit").css('display','block');
        var msg = $("#msg").val();
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "msg": msg },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#msg_hi").val(jsonObj.msg.hi);
                $("#msg_gu").val(jsonObj.msg.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
</script>
@endsection
