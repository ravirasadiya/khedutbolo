@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($category)
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Categories</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="col-md-4">Category Name</th>
                                <th class="col-md-4">Description</th>
                                <th class="col-md-4">Cover</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td>{!! $category->description  !!}</td>
                                    <td>
                                        @if(isset($category->cover))
                                            <img  style="height:100px !important;width:100px !important" src="{{ $category->cover }}" alt="category image" class="img-thumbnail">
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                <!--    </div>-->
                <!--</div>-->
                @if(!$categories->isEmpty())
                <hr>
                    <!--<div class="box-body">-->
                    <!--    <div class="box-body">-->
                            <table class="table border-b1px" id="table-id">
                                <thead>
                                <tr>
                                    <th class="col-md-3">Sub-Category Name</th>
                                    <th class="col-md-3">Description</th>
                                    <th class="col-md-3">Cover</th>
                                    <th class="col-md-3">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $cat)
                                        <tr>
                                            <td>{{ $cat->name }}</a></td>
                                            <td>{!! $cat->description !!}</td>
                                            <td>@if(isset($cat->cover))<img style="height:100px !important;width:100px !important"  src="{{$cat->cover}}" alt="category image" class="img-thumbnail">@endif</td>
                                            <td>
                                                <form action="{{ route('admin.categories.destroy', $cat->id) }}" method="post" class="form-horizontal">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                                    <div class="btn-group">
                                                        @if($permission->edit==1)
                                                            <a href="{{ route('admin.categories.edit', $cat->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                                                        @endif
                                                        @if($permission->delete==1)
                                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button>
                                                        @endif
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                @if(!$products->isEmpty())
                    <div class="box-body">
                        <h2>Products</h2>
                        @include('admin.shared.products', ['products' => $products])
                    </div>
                @endif
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="box-body">
                        <div class="btn-group">
                            <a href="{{ route('admin.categories.index') }}" class="btn btn-default btn-sm">Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection


@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable({
        'info' : false,
        'paging' : true,
        'searching' : false,
        'orderable': false,
        "lengthChange": false,
        'sorting' : []
        });
    } );
</script>
@endsection