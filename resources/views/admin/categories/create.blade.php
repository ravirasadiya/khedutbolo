@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Add Category</h4>
                    </div>
                </div>
                <form action="{{ route('admin.categories.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_category">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <!-- <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="company_id">Company Name <span class="text-danger">*</span></label>
                                    <select name="company_id" id="company_id" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($companies as $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="parent">Parent Category</label>
                                    <select name="parent" id="parent" class="form-control select2">
                                        <option value="">Select</option>
                                        @foreach($categories as $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="{{ old('name') }}"  maxlength="25">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name_hi"> <span class="text-danger">*</span></label>
                                    <input type="text" name="name_hi" id="name_hi" placeholder="नाम" class="form-control" value="{{ old('name_hi') }}"  maxlength="25">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name_gu"> <span class="text-danger">*</span></label>
                                    <input type="text" name="name_gu" id="name_gu" placeholder="નામ" class="form-control" value="{{ old('name_gu') }}"  maxlength="25">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="status">Status <span class="text-danger">*</span></label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="0">Disable</option>
                                        <option value="1">Enable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="description">Description </label>
                                    <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="description_hi">विवरण </label>
                                    <textarea class="form-control ckeditor" name="description_hi" id="description_hi" rows="5" placeholder="विवरण">{{ old('description_hi') }}</textarea>

                                    @if ($errors->has('description_hi'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description_hi') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="description_gu">વર્ણન </label>
                                    <textarea class="form-control ckeditor" name="description_gu" id="description_gu" rows="5" placeholder="વર્ણન">{{ old('description_gu') }}</textarea>
                                    @if ($errors->has('description_gu'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('description_gu') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                           <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="cover">Cover <span class="text-danger">*</span></label>
                                    <input type="file" name="cover" id="cover" class="form-control" accept="image/*" onchange="GetFileSize()">
                                    <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                    <label id="image" class="image">Please select cover image</label>
                                    <label id="image_error" class="image_error">Image size is large.</label>
                                    <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" class="btn btn-primary" style="display: none;" id="add_submit">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')

<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>

<script type="text/javascript">
    $('#company_id').change(function(){
        $("#parent option").remove();
        $("#subcategory_id option").remove();
        var id = $(this).val();
        console.log('id',id);
    
        // $.ajax({
        //     url : "{{route('admin.products.getCategories')}}",
        //     data: {
        //         "_token": "{{ csrf_token() }}",
        //         "id": id
        //         },
        //     type: 'post',
        //     dataType: 'json',
        //     success: function( result )
        //     {
        //         $('#parent').append($('<option>', {value:'', text:'Select Category'}));
        //         $.each( result, function(k, v) {
        //             $('#parent').append($('<option>', {value:k, text:v}));
        //         });
        //     },
        //     error: function()
        //     {
        //         alert('error...');
        //     }
        // });
    });

    $('#parent').change(function(){
        $("#subcategory_id option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{route('admin.products.getSubCategories')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                $('#subcategory_id').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#subcategory_id').append($('<option>', {value:k, text:v}));
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    });

    $("#add_category").validate({
        ignore: [],
        errorClass: 'error text-change',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            
            status : {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name",
                maxlength:"Please enter no more than 25 character",
            },
            "company_id":{
                required: "Select company name",
            },
            "cover":{
                required: "Select cover image",
            },
            "status":{
                required: "Select status",
            }
        },
            
        submitHandler: function (form) {
            error=0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image').style.display="none";
            var logo = $('#cover').val();
            console.log(logo);
            if(logo==""){
                document.getElementById('cover').style.display="block";
                error=1;
            } else {
                var fi = document.getElementById('cover');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;   
                        console.log('fsize'+fsize+' type '+type+' size '+size);
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error = 1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error = 1;
                        }
                    }
                } 
            }
            if (error == 1)  {
                return false;
            }
            $('#add_submit').attr('disabled', true);
            form.submit();
        },
    });
    function GetFileSize() {
        var fi = document.getElementById('cover');
        document.getElementById('image').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {

                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;  
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });
    $("#translate").on('click',function(){
        $("#add_submit").css('display','block');
        var name = $("#name").val();
        var description = CKEDITOR.instances['description'].getData();
        console.log(description);
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "name": name,"description":description },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                CKEDITOR.instances['description_hi'].setData(jsonObj.description.hi);
                CKEDITOR.instances['description_gu'].setData(jsonObj.description.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
</script>
@endsection

