@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($categories)
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Categories</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-3">Category Name</th>
                                    <th class="col-md-3">Cover</th>
                                    <th class="col-md-3">Status</th>
                                    <th class="col-md-3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <td>
                                        {{ $category->name }}
                                    </td>
                                    <td>
                                        @if(isset($category->cover))
                                            <img  style="height:100px !important;width:100px !important" src="{{$category->cover}}" alt="category image" class="img-thumbnail">
                                        @endif
                                    </td>
                                    <td>@include('layouts.status', ['status' => $category->status])</td>
                                    <td>
                                        <form action="{{ route('admin.categories.destroy', $category->id) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">
                                                <a href="{{ route('admin.categories.show', $category->id) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                                @if($permission->edit==1)
                                                    <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                @endif
                                                @if($permission->delete==1)
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button>
                                                @endif
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>    
                    </div>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body">
                    <p class="alert alert-warning">No categories found.</p>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
