@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        @include('layouts.errors-and-messages')
        <!-- Default box -->
        @if($orderStatuses)
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Manage Order Status</h4>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table border-b1px" id="table-id">
                    <thead>
                        <tr>
                            <th class="col-md-4">Name</th>
                            <th class="col-md-4">Color</th>
                            <th class="col-md-4">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($orderStatuses as $status)
                        <tr>
                            <td>{{ ucfirst($status->name) }}</td>
                            <td><button class="btn" style="background-color: {{ $status->color }}"><i class="fa fa-check" style="color: #ffffff"></i></button></td>
                            <td>
                                <form action="{{ route('admin.order-statuses.destroy', $status->id) }}" method="post" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <div class="btn-group">
                                        @if($permission->edit==1)
                                            <a href="{{ route('admin.order-statuses.edit', $status->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                                        @endif
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="btn-group">
                </div>
            </div>
        </div>
        <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No order status found.</p></div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
