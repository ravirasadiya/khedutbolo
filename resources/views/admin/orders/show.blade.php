@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                          @if($order->address!=null)
                        <h2>
                            <a href="#">{{$order->address->alias}}</a> <br />
                            <small>{{$order->email}}</small> <br />
                            <small>{{$order->address->phone}}</small> <br />
                            @endif
                            <small>reference: <strong>{{$order->reference}}</strong></small>
                        </h2>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <h2><a href="@if($order->invoice_path) {{asset($order->invoice_path)}} @else {{route('admin.orders.invoice.generate', $order['id'])}} @endif" class="btn btn-primary btn-block" target="_blank">Download Invoice</a></h2>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <h4> <i class="fa fa-shopping-bag"></i> Order Information</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="col-md-2">Date</th>
                            <th class="col-md-2">Customer</th>
                            <th class="col-md-2">Phone</th>
                            <th class="col-md-2">Payment</th>
                            <th class="col-md-2">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</td>
                          @if($order->address!=null)
                        <td><a href="{{ route('admin.customers.show', $customer->id) }}">{{ $order->address->alias }}</a></td>
                        @else
                         <td><a href="{{ route('admin.customers.show', $customer->id) }}"></a></td>
                        @endif
                         @if($order->address!=null)
                        <td>
                            {{ $order->address->phone }}</td>
                            @else
                            <td></td>
                            @endif
                        <td><strong>{{ $order['payment'] }}</strong></td>
                        <td>
                            <button type="button" class="btn btn-info btn-block">{{ ucfirst($currentStatus->name) }}</button>
                        </td>
                    </tr>
                    <!-- </tbody>
                    <tbody> -->
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Sub Total</td>
                        <td class="bg-warning">Rs {{ number_format(($order['total_products']) - ($order['tax']),2) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Tax</td>
                        <td class="bg-warning">{{ $order['tax'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Shipping</td>
                        <td class="bg-warning">{{ $order['total_shipping'] }}</td>
                    </tr>
                    @if($order['wallet_point']!=0.0 && $order['wallet_point']!="")
                        @php
                            $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                            $wallet_amount = number_format($wallet_amount,2);
                        @endphp
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="bg-warning">- Discount</td>
                            <td class="bg-warning">{{ $wallet_amount }}</td>
                        </tr>
                    @endif
                    <!--<tr>-->
                    <!--    <td></td>-->
                    <!--    <td></td>-->
                    <!--    <td></td>-->
                    <!--    <td class="bg-success text-bold">Order Total</td>-->
                    <!--    <td class="bg-success text-bold">Rs {{ $order['total'] }}</td>-->
                    <!--</tr>-->
                    @php
                        if (isset($wallet_amount)) {
                            $total = $order['total'] + $order['total_shipping'] - $wallet_amount;
                        } else{
                            
                            $total = $order['total'] + $order['total_shipping'];
                        }
                    @endphp
                    @if($order['total_paid'] != $order['total'])
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="bg-danger text-bold">Total paid</td>
                            <td class="bg-danger text-bold">Rs {{ number_format($total,2) }}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            @if($order)
                @if(!$items->isEmpty())
                    <div class="box-body">
                        <h4> <i class="fa fa-gift"></i> Items</h4>
                        <table class="table table-striped">
                            <thead>
                           
                            <th class="col-md-2">Name</th>
                              @if($offer_status==1)
                                <th class="col-md-2">Offer Product</th>
                            @endif
                             <th class="col-md-2">HSN Code</th>
                            <th class="col-md-1">Unit</th>
                            <th class="col-md-1">Quantity</th>
                            <th class="col-md-2">Price</th>
                           <th class="col-md-2">Shipping Price</th>
                            <th class="col-md-1">GST %</th>
                            <th class="col-md-1">CGST</th>
                            <th class="col-md-1">SGST</th>
                            <th class="col-md-2">Total</th>
                          
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                            @php
                                $item->product_price = str_replace(",","",$item->product_price);
                                $gst = ($item->product_price*$item->quantity*$item->gst_price)/100;
                                $total = ($item->product_price*$item->quantity)+$item->shipping_price;
                                
                                $total = number_format($total ,2);
                                $total = str_replace(",","",$total);
                            @endphp
                                <tr>
                                   
                                    <td>{{ $item->product_name }}</td>
                                     @if($offer_status==1)
                                        <td>{{ $item->offer_product }}</td>
                                    @endif
                                     <td>{{ $item->hsn_code }}</td>
                                   
                                    <td>{{ $item->unit }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <?php /*?>
                                    <td>{{ str_replace(",","",$item->product_price-$gst) }}</td>
                                     <?php */?>
                                       <td>{{ number_format(($item->product_price)-($gst),2) }}</td>
                                   <td>{{ number_format($item->shipping_price,2) }}</td>
                                   <?php /*?> <td>{{ $gst }}%</td><?php */ ?>
                                   <td>{{$item->gst_price}}%</td>
                                     <td>{{ number_format(($gst/2),2) }} </td>
                                      <td>{{ number_format(($gst/2),2) }}</td>
                                    <td>{{ $total }}</td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
              
                @if($order->address!=null)
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-map-marker"></i> Address</h4>
                            <table class="table table-striped">
                                <thead>
                                    <th class="col-md-2">Address</th>
                                    <th class="col-md-2">Village</th>
                                    <th class="col-md-2">Tehsil</th>
                                    <th class="col-md-2">City</th>
                                    <th class="col-md-2">State</th>
                                    <th class="col-md-2">Country</th>
                                    <th class="col-md-2">Zip</th>
                                    <!-- <th class="col-md-2">Province</th> -->
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order->address->address_1 }}</td>
                                    <td>{{ $order->address->village_id }}</td>
                                    <td>{{ $order->address->tehsil_id }}</td>
                                    <td>{{ $order->address->city }}</td>
                                    <!-- <td>
                                        @if(isset($order->address->province))
                                            {{ $order->address->province->name }}
                                        @endif
                                    </td> -->
                                    <td>{{ $order->address->state_code }}</td>
                                    <td>{{ $order->address->country->name }}</td>
                                    <td>{{ $order->address->zip }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
            <!-- /.box -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.orders.index') }}" class="btn btn-default">Back</a>
                    @if($order->order_status_id!=1 && $order->order_status_id!=4)
                        @if($permission->edit==1)<a href="{{ route('admin.orders.edit', $order->id) }}" class="btn btn-primary">Edit</a>
                        @endif
                    @endif
                </div>
            </div>
        @endif
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection