@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        <div class="box">
            <div class="box-header">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h2>
                            <a href="#">{{$order->address->alias}}</a> <br />
                            <small>{{$order->email}}</small> <br />
                            <small>{{$order->address->phone}}</small> <br />
                            <small>reference: <strong>{{$order->reference}}</strong></small>
                        </h2>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <h2><a href="
                        @if($order->invoice_path)
                         {{asset($order->invoice_path)}}
                        @else
                         {{route('admin.orders.invoice.generate', $order['id'])}}
                        @endif" class="btn btn-primary btn-block" target="_blank">Download Invoice</a></h2>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <h4> <i class="fa fa-shopping-bag"></i> Order Information</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="col-md-2">Date</th>
                            <th class="col-md-2">Customer</th>
                            <th class="col-md-2">Phone</th>
                            <th class="col-md-2">Payment</th>
                            <th class="col-md-2">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</td>
                        <td><a href="{{ route('admin.customers.show', $customer->id) }}">{{ $order->address->alias }}</a></td>
                        <td>{{ $order->address->phone }}</td>
                        <td><strong>{{ $order['payment'] }}</strong></td>
                        <td>
                            <form action="{{ route('admin.orders.update', $order->id) }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                <!-- <label for="order_status_id" class="hidden">Update status</label> -->
                                <!-- <input type="text" name="total_paid" class="form-control" placeholder="Total paid" style="margin-bottom: 5px; display: none" value="{{ old('total_paid') ?? $order->total_paid }}" /> -->
                                <div class="input-group">
                                    <select name="order_status_id" id="order_status_id" class="form-control select2">
                                        @foreach($statuses as $status)
                                            <option @if($currentStatus->id == $status->id) selected="selected" @endif value="{{ $status->id }}">{{ ucfirst($status->name) }}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn" style="width: 0"><button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-primary">Update</button></span>
                                </div>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Subtotal</td>
                        <td class="bg-warning">Rs {{ $order['total_products'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Tax</td>
                        <td class="bg-warning">{{ $order['tax'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">+ Shipping</td>
                        <td class="bg-warning">{{ $order['total_shipping'] }}</td>
                    </tr>
                      @if($order['wallet_point']!=0.0 && $order['wallet_point']!="")
                        @php
                            $wallet_amount = $order['wallet_point'] * env('DISCOUNT_POINT');
                            $wallet_amount = number_format($wallet_amount,2);
                        @endphp
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="bg-warning">- Discount</td>
                            <td class="bg-warning">{{ $wallet_amount }}</td>
                        </tr>
                    @endif
                    <!--<tr>-->
                    <!--    <td></td>-->
                    <!--    <td></td>-->
                    <!--    <td></td>-->
                    <!--    <td class="bg-success text-bold">Order Total</td>-->
                    <!--    <td class="bg-success text-bold">Rs {{ $order['total'] }}</td>-->
                    <!--</tr>-->
                    @php
                        if (isset($wallet_amount)) {
                            $total = $order['total'] + $order['total_shipping'] + $order['tax'] - $wallet_amount;
                        } else{
                            $total = $order['total'] + $order['total_shipping'] + $order['tax'];
                        }
                    @endphp
                    @if($order['total_paid'] != $order['total'])
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="bg-danger text-bold">Total paid</td>
                            <td class="bg-danger text-bold">Rs {{ number_format($total,2) }}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            @if($order)
                @if(!$items->isEmpty())
                    <div class="box-body">
                        <h4> <i class="fa fa-gift"></i> Items</h4>
                        <table class="table table-striped">
                            <thead>
                            <th class="col-md-2">HSN Code</th>
                            <th class="col-md-2">Name</th>
                            <th class="col-md-1">Unit</th>
                            <th class="col-md-1">Quantity</th>
                            <th class="col-md-2">Price</th>
                            <th class="col-md-2">Shipping Price</th>
                            <th class="col-md-1">Tax</th>
                            <th class="col-md-2">Total</th>
                            @if($offer_status==1)
                                <th class="col-md-2">Offer Product</th>
                            @endif
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                            @php
                                $item->product_price = str_replace(",","",$item->product_price);
                                $gst = ($item->product_price*$item->quantity*$item->gst_price)/100;
                                $total = ($item->product_price*$item->quantity)+$item->shipping_price+$gst;
                            @endphp
                                <tr>
                                    <td>{{ $item->hsn_code }}</td>
                                    <td>{{ $item->product_name }}</td>
                                    <td>{{ $item->unit }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ number_format($item->product_price,2) }}</td>
                                    <td>{{ number_format($item->shipping_price,2) }}</td>
                                    <td>{{ number_format($gst, 2) }}</td>
                                    <td>{{ number_format($total, 2) }}</td>
                                    @if($offer_status==1)
                                        <td>{{ $item->offer_product }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-map-marker"></i> Address</h4>
                            <table class="table table-striped">
                                <thead>
                                    <th class="col-md-2">Address</th>
                                    <th class="col-md-2">Village</th>
                                    <th class="col-md-2">Tehsil</th>
                                    <th class="col-md-2">City</th>
                                    <th class="col-md-2">State</th>
                                    <th class="col-md-2">Country</th>
                                    <th class="col-md-2">Zip</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order->address->address_1 }}</td>
                                    <td>{{ $order->address->village_id }}</td>
                                    <td>{{ $order->address->tehsil_id }}</td>
                                    <td>
                                        @if(isset($order->address->city))
                                            {{ $order->address->city }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(isset($order->address->state_code))
                                            {{ $order->address->state_code }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(isset($order->address->country_id))
                                            {{ $order->address->country_id }}
                                        @endif
                                    </td>
                                    <td>{{ $order->address->zip }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-truck"></i> Courier</h4>
                            <table class="table table-striped">
                                <thead>
                                    <th class="col-md-2">Name</th>
                                    <th class="col-md-2">Link</th>
                                    <th class="col-md-6">Description</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order->courier->name }}</td>
                                    <td>{{ $order->courier->url }}</td>
                                    <td>{{ $order->courier->description }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.orders.show', $order->id) }}" class="btn btn-default">Back</a>
                    </div>
                </div>
            @endif
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            let osElement = $('#order_status_id');
            osElement.change(function () {
                if (+$(this).val() === 1) {
                    $('input[name="total_paid"]').fadeIn();
                } else {
                    $('input[name="total_paid"]').fadeOut();
                }
            });
        })
    </script>
@endsection
