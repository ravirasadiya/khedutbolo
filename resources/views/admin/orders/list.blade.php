@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->

        @if($orders)
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                                <h4 class="page-title header-color">Manage Orders</h4>
                            </div>
                            <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                                <div  style="text-align:right;">
                                    <a href="{{url('admin/orders/printorder')}}" target="_blank"><i class="fa fa-print" style="font-size:24px;"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="page-filter">
                            <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="sort-product-lable" style="margin-left: -30px;">
                                    <div class="form-group text-left sort-product-lable">
                                        <div class="col-sm-1 lable_product"><label>Show</label></div>
                                        <div class="col-sm-3">
                                            <select class="form-control w-auto d_inline" name="product_no" id="page_change">
                                                <option value="10" selected="true" @if(request()->input('product_no')=="10") selected="true" @endif>10</option>
                                                <option value="25" @if(request()->input('product_no')=="25") selected="true" @endif>25</option>
                                                <option value="50" @if(request()->input('product_no')=="50") selected="true" @endif>50</option>
                                                <option value="100" @if(request()->input('product_no')=="100") selected="true" @endif>100</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 lable_product product_margin"><label>Entries</label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group text-right sort-product sortter">
                                     {{-- <label>Sort by:</label>  --}}
                                    <select class="form-control w-auto d_inline"  name="sort" id="sort_change">
                                        <option value="" @if(request()->input('sort')=="") selected="true" @endif>Select Sort by</option>
                                        <option value="customer" @if(request()->input('sort')=="customer") selected="true" @endif>Customer</option>
                                        <option value="price_low_to_high" @if(request()->input('sort')=="price_low_to_high") selected="true" @endif>Price: Low to High</option>
                                        <option value="price_high_to_low" @if(request()->input('sort')=="price_high_to_low") selected="true" @endif>Price: High to Low</option>
                                        <option value="date" @if(request()->input('sort')=="date") selected="true" @endif>Order Date</option>
                                    </select>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="orderAll" class="checkbox" name="orderAll" onClick="orderAll(this)"></th>
                                    <th >Order Id</th>
                                    <th >Date</th>
                                    <th >E-mail</th>
                                    <th >Mobile</th>
                                    <th >Customer</th>
                                    <th >Total</th>
                                    <th >Status</th>
                                    <th >View</th>
                                </tr>
                            </thead>
                            <tbody>
                         
                            @foreach ($orders as $order)
                                <tr>
                                    <td><input type="checkbox" id="order" value="{{$order->id}}" class="checkbox" name="order[]" value="order"></td>
                                    <td>{{$order->id}}</td> 
                                    <td>{{date('M d, Y h:i a', strtotime($order->created_at))}}</td>
                                    <td>{{$order->email }}</td>
                                    <td>{{$order->mobile }}</td>
                                    <td>{{$order->customer_name}}  </td>
                                   @php
                                    if($order['wallet_point']!=0){
                                    
                                     $wallet_amount = floatval($order['wallet_point']) * floatval(env('DISCOUNT_POINT'));
                                     }
                                     else{
                                      $wallet_amount = 0;
                                     }
                                     @endphp
                                      
                                      @php
                                        if($order['wallet_point']!=0){
                                            if (is_numeric($wallet_amount)) {
                                             $total = floatval($order['total']) + floatval($order['total_shipping']) - floatval($wallet_amount);
                                            }
                                        }
                                         else{
                                            $total = floatval($order['total']) + floatval($order['total_shipping']);
                                        }
                                    @endphp
                                   <?php /* ?>
                                    @php
                                    
                                        $wallet_amount = is_numeric($order['wallet_point']) * is_numeric(env('DISCOUNT_POINT'));
                                    @endphp
                                    @php
                                     
                                        if (is_numeric($wallet_amount)) {
                                            $total = is_numeric($order['total']) + is_numeric($order['total_shipping']) - is_numeric($wallet_amount);
                                        } else{
                                            $total = is_numeric($order['total']) + is_numeric($order['total_shipping']);
                                        }
                                    @endphp
                                  
                                    <td>
                                        <span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif">Rs </span>
                                    </td>
                                     <?php */ /*?><?php */ ?>
                                    <td>
                                        <span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif">Rs {{ $total }}</span>
                                    </td>
                                    <td><p class="label" style=" color: black; background-color: {{ $order->color }}">{{ ucfirst($order->status) }}</p></td>
                                    <td>
                                        <a title="Show order" href="{{ route('admin.orders.show', $order->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{$fullscreen_theme->links()}}
                </div>
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No orders found.</p></div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection
@section('js')

<script type="text/javascript">
    $("#sort_change").on('change',function(){
        var sort = $("#sort_change").val();
        window.location.href = "{{ request()->url() }}?sort="+sort;
        var url = window.location.href;
        if(url == "{{ request()->url() }}"){
            window.location.href = "{{ request()->url() }}?sort="+sort;
        }
        if(url.includes("{{ request()->url() }}?product_no=")) {
            var product_no = "{{request()->input('product_no')}}";
            window.location.href = "{{ request()->url() }}?product_no="+product_no+"&&sort="+sort;
        }
        if(url.includes("{{ request()->url() }}?sort=")) {
            var product_no = "{{request()->input('product_no')}}";
            window.location.href = "{{ request()->url() }}?sort="+sort+"&&product_no="+product_no;
        }
    });

    $("#page_change").on('change',function(){
        var product_no = $("#page_change").val();
        var url = window.location.href;
        if(url == "{{ request()->url() }}"){
            window.location.href = "{{ request()->url() }}?product_no="+product_no;
            console.log(window.location.href);
        }
        if(url.includes("{{ request()->url() }}?sort=")) {
            var sort = "{{request()->input('sort')}}";
            window.location.href = "{{ request()->url() }}?sort="+sort+"&&product_no="+product_no;
            console.log(window.location.href);
        }
        if(url.includes("{{ request()->url() }}?product_no=")) {
            var sort = "{{request()->input('sort')}}";
            window.location.href = "{{ request()->url() }}?product_no="+product_no+"&&sort="+sort;
            console.log(window.location.href);
        }
    });
    
    $('.checkbox').on('change', function(){
        var checkboxes = document.getElementsByName('order[]');
        var a=0;
        var order = [];
        counts=0;
        var count = checkboxes.length;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                var order_value = checkboxes[i].value;
                order[i] = order_value;
                counts = counts+1;
                a=a+1;
                if(a==count){
                    document.getElementById('orderAll').checked=true;
                }else {
                    document.getElementById('orderAll').checked=false;
                }
            }
        }
        ajaxfunction(order, counts);
    });
    
    function ajaxfunction(order, counts) {
        var count = order.length;
        console.log(order);
        $.ajax({
            url : "{{route('admin.orders.print_order')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "order": order,
                "count": counts
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
            },
            error: function()
            {
                alert('error...');
            }
        });
    }
    
    function orderAll(source) {
        checkboxes = document.getElementsByName('order[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
</script>

@endsection
