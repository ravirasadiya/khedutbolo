@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Role</h4>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('admin.roles.update', $role->id) }}" id="edit_role" method="post" class="form">
                        <div class="box-body">
                            {{ csrf_field() }}
                            <input type="hidden" value="put" name="_method">
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="name">Name  <span class="text text-danger">*</span></label>
                                        <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="{{ old('name') ?: $role->name }}" maxlength="25">
                                        @if ($errors->has('name'))
                                            <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="display_name">Display Name <span class="text text-danger">*</span></label>
                                        <input type="text" name="display_name" id="display_name" placeholder="Display name" class="form-control character" value="{{ old('display_name') ?: $role->display_name }}" maxlength="25">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="description">Description </label>
                                        <textarea name="description" id="description" class="form-control ckeditor" placeholder="Description"> {!! old('description') ?: $role->description !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <table  class="table table-striped">
                                            <thead>
                                                <th>No</th>
                                                <th>Modules</th>
                                                <th>View <input type="checkbox" id="view" class="role-add view" class="role-margin" onClick="viewAll(this)" @if($view==14) checked @endif></th>
                                                <th>Add <input type="checkbox" id="add" class="role-add add" onClick="addAll(this)" @if($add==11) checked @endif></th>
                                                <th>Edit <input type="checkbox" id="edit" class="role-add edit" onClick="editAll(this)" @if($edit==13) checked @endif></th>
                                                <th>Delete <input type="checkbox" id="delete" class="role-delete delete" onClick="deleteAll(this)" @if($delete==10) checked @endif></th>
                                            </thead>
                                            <tbody>
                                                @foreach($modules as $module)
                                                    <tr>
                                                        <td>{{ $module->id }} </td>
                                                        <td>{{ $module->display_name }} </td>
                                                        @foreach($permissions as $permission)
                                                            @if($module->id == $permission->module_id)
                                                                <td align="center">
                                                                    @if($module->name!='notification' )
                                                                        <input type="checkbox" class="checkbox" name="view[]" value="1-{{$module->name}}" @if($module->id == $permission->module_id && $permission->view == 1) checked @endif>
                                                                    @else - @endif    
                                                                </td>
                                                                <td align="center">
                                                                    @if($module->name!='update_stock' && $module->name!='order_status' && $module->name!='priority' && $module->name!='wallet')
                                                                        <input type="checkbox" class="checkbox" name="add[]" value="1-{{$module->name}}" @if($module->id == $permission->module_id && $permission->add == 1) checked @endif>
                                                                    @else - @endif
                                                                </td>
                                                                <td align="center">
                                                                    @if($module->name!='wallet' && $module->name!='notification')
                                                                        <input type="checkbox" class="checkbox" name="edit[]" value="1-{{$module->name}}" @if($module->id == $permission->module_id && $permission->edit == 1) checked @endif>
                                                                    @else - @endif
                                                                </td>
                                                                <td align="center">
                                                                    @if($module->name!='update_stock' && $module->name!='order_status' && $module->name!='priority' && $module->name!='notification' && $module->name!='wallet')
                                                                        <input type="checkbox" class="checkbox" name="delete[]" value="1-{{$module->name}}" @if($module->id == $permission->module_id && $permission->delete == 1) checked @endif>
                                                                    @else - @endif
                                                                </td>
                                                                
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                @endforeach  
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="btn-group">
                                <div class="btn-group">
                                    <a href="{{ route('admin.roles.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-primary" style="background-color: rgba(76, 175, 80, 1);">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">

    function viewAll(source) {
        checkboxes = document.getElementsByName('view[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function addAll(source) {
        checkboxes = document.getElementsByName('add[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function editAll(source) {
        checkboxes = document.getElementsByName('edit[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function deleteAll(source) {
        checkboxes = document.getElementsByName('delete[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    
    $('.checkbox').change(function() {
        checkboxes = document.getElementsByName('view[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==14){
                    document.getElementById('view').checked=true;
                }else {
                    document.getElementById('view').checked=false;
                }
            }
        }
        checkboxes = document.getElementsByName('add[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==11){
                    document.getElementById('add').checked=true;
                }else {
                    document.getElementById('add').checked=false;
                }
            }
        }
        checkboxes = document.getElementsByName('edit[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==13){
                    document.getElementById('edit').checked=true;
                }else {
                    document.getElementById('edit').checked=false;
                }
            }
        }
        checkboxes = document.getElementsByName('delete[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==10){
                    document.getElementById('delete').checked=true;
                }else {
                    document.getElementById('delete').checked=false;
                }
            }
        }
    });            


    $("#edit_role").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                // lettersonly: true,
            },
            display_name: {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name.",
                // lettersonly: "Please enter only character.",
            },
            "display_name":{
                required: "Please enter display name.",
            },
        },
    errorPlacement: function(error, element) {
        var elem = $(element);
        if (elem.hasClass("select2-hidden-accessible")) {
           element = $("#select2-" + elem.attr("id") + "-container").parent();
           error.insertAfter(element);
        } else {
           error.insertAfter(element);
        }
    },
    submitHandler: function(form) {
        
        $('button[type="submit"]').attr('disabled', true);
        form.submit();
    },
});

    
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    }); 


</script>

@endsection

