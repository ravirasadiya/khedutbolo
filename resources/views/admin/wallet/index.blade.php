@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$customers->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Customer List</h4>
                        </div>
                    </div>
                    @if(!$customers->isEmpty())
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                            <tr>
                                <th class="col-md-2">Name</th>
                                <th class="col-md-2">Mobile</th>
                                <th class="col-md-2">Total Points</th>
                                <th class="col-md-2">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($customers as $customer)
                            
                                @php
                                $customer_points = App\Shop\Customers\Wallet::where('customer_id',$customer->id)->get();
                                $totalcreditamount = array();
                                $totalcreditpoint = array();
                                $totaldebitamount = array();
                                $totaldebitpoint = array();
                                foreach ($customer_points as $customer_point) {
                                    if ($customer_point->credit_or_debit == "credit") {
                                        $customer_point['title'] = "Added to wallet";
                                        array_push($totalcreditamount, $customer_point->amount);
                                        array_push($totalcreditpoint, $customer_point->points);
                                    } else{
                                        array_push($totaldebitamount, $customer_point->amount);
                                        array_push($totaldebitpoint, $customer_point->points);
                                        $customer_point['title'] = "Paid from wallet";
                                    }
                                }

                                $totalpoint = array_sum($totalcreditpoint) - array_sum($totaldebitpoint);
                                $totalamount = array_sum($totalcreditamount) - array_sum($totaldebitamount);
                                $totalamount = number_format($totalamount, 2);
                                @endphp
                                @if($totalpoint!=0)
                                <tr>
                                    <td>{{ $customer->name }} </td>
                                    <td>{{ $customer->mobile }} </td>
                                    <td>{{ $totalpoint }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('admin.wallet.show', $customer->id) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        {{-- <nav>
                            {{ $fullscreen_theme->links() }}
                        </nav> --}}
                    </div>
                    @endif
                    <div class="box-footer">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No products found.</p></div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')

<script type="text/javascript">


    $(document).ready(function() {
        $('#table-id').DataTable({});
    });

</script>

@endsection