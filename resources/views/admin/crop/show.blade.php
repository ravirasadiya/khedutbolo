@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Solution</h4>
                </div>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Solution Name</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $crop->name }}</td>
                            <td><img src="@if($crop['image']){{ $crop->image }} @else {{asset('images/crop/default_crop.jpg')}} @endif" alt="Image not found" class="img-responsive img-thumbnail" height="100" width="100"></td>
                        </tr>
                    </tbody>
                </table>
            <!--</div>-->
            @if(!$values->isEmpty())
            <!--<div class="box-body">    -->
                <!-- <table class="table border-b1px" style="margin-left: 35px"> -->
                <table class="table border-b1px" id="table-id">
                    <thead>
                        <tr>
                            <th>Sub-Solution Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($values as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td><img src="@if($item['image']){{ $item['image'] }}@else {{ asset('images/crop/default_crop.jpg') }} @endif" alt="Image not found" class="img-responsive img-thumbnail" height="100" width="100"></td>
                                <td>
                                    <form action="{{ route('admin.crops.destroy', $item['id']) }}" class="form-horizontal" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.crops.show_sub', [$item['id'], $crop->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                            @if($permission->edit==1)
                                                <a href="{{ route('admin.crops.edit', [$item['id'], $crop->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if($permission->delete==1)
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                            @endif
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="box-body">
                <div class="btn-group">
                    <a href="{{ route('admin.crops.create', $crop->id) }}" class="btn btn-primary btn-sm">Add Sub-Solution</a>
                    <a href="{{ route('admin.crops.index') }}" class="btn btn-default btn-sm">Back</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable({
        'info' : false,
        'paging' : true,
        'searching' : false,
        'orderable': false,
        "lengthChange": false,
        'sorting' : []
        });
    } );
</script>
@endsection