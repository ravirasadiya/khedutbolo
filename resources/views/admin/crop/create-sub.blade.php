@extends('layouts.admin.app')
@section('content')

    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">@if(isset($subsubid)) Edit @else Add @endif Sub-Sub Solution </h4>
                    </div>
                </div>
                <input type="hidden"  id="crop" name="crop-type" value="@if(!$crop)Solution @else Sub-Solution  @endif">
                <form action="{{ route('admin.crops.subcatstore') }}" method="post" class="form" enctype="multipart/form-data" id="add_crop">
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="core_category">Solution Category <span class="text-danger">*</span></label>
                                                    <select name="core_category" id="core_category" class="form-control core_scroll"  >
                                                        <option value="">Select Category</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category['id'] }}" @if(isset($crop)) @if($category->id == $crop->id) selected="selected" @endif  @endif </option>{{ $category['name'] }}</option>
                                                            @endforeach
                                                    </select>
                                                    <input type="hidden" name="core_category_selected" id="core_category_selected" value="@if(isset($crop)){{$crop->id}}@endif">
                                                    <input type="hidden" name="core_subcategory_selected" id="core_subcategory_selected" value="@if(isset($subid)){{$subid}}@endif">
                                                    <input type="hidden" name="id" id="id" value="@if(isset($subsubid)){{$subsubid->id}}@endif">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="core_subcategory">Sub-Solution Category <span class="text-danger">*</span></label>
                                                    <select name="core_subcategory" id="core_subcategory" class="form-control core_scroll"  >
                                                        <option value="">Select Sub Category</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name">Name <span class="text-danger">*</span></label>
                                                    <input type="text" name="name" id="name" placeholder="Sub-Sub Solution" class="form-control character" value="@if(isset($subsubid)){{$subsubid->name}}@endif" maxlength="25">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name"><span class="text-danger">*</span></label>
                                                    <input type="text" name="name_hi" id="name_hi" placeholder="उप-उप समाधान" class="form-control" value="@if(isset($subsubid)){{$subsubid->name_hi}}@endif" maxlength="25">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label for="name"><span class="text-danger">*</span></label>
                                                    <input type="text" name="name_gu" id="name_gu" placeholder="પેટા-સબ સોલ્યુશન" class="form-control" value="@if(isset($subsubid)){{$subsubid->name_gu}}@endif" maxlength="25">
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="image">Image <span class="text-danger">*</span></label>
                                            <input type="file" name="image" id="image" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                            <label id="image1" class="image_error">Please select image</label>
                                            <label id="image_error" class="image_error">Image size is large.</label>
                                            <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="technical_name">Technical Name <span class="text-danger">*</span></label>
                                            <select name="technical_name" id="technical_name" class="form-control">
                                                <option value="">Select Technical Name</option>
                                                @foreach($technical as $tech_name)
                                                    <option value="{{$tech_name->name}}">{{$tech_name->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                                <div class="row">
                                    <table class="table border-b1px" id="product-sub-sub-solution">
                                        <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>Product Name</th>
                                                <th>Technical Name</th>
                                            </tr>
                                        </thead>
                                        <tbody id="product_tbl">
                                            @if(isset($subsubid))
                                                @php
                                                    $pro = json_decode($subsubid->product_id);
                                                   
                                                @endphp
                                                @if(count(explode(",",$subsubid->product_id))!=1)
                                                @foreach($products as $product)
                                                <tr>
                                                    <td><input type="checkbox" name="product[]" value="{{$product->id}}" 
                                                           @if(isset($pro)) {{in_array($product->id, $pro) ? 'checked' : ''}} @endif
                                                        ></td>
                                                    <td>{{$product->name}}</td>
                                                    <td>{{$product->technical_name}}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                                 @if(count(explode(",",$subsubid->product_id))==1)
                                                @foreach($products as $product)
                                                <tr>
                                                    <td><input type="checkbox" name="product[]" value="{{$product->id}}" 
                                                           @if(isset($pro)) {{($product->id==$subsubid->product_id) ? 'checked' : ''}} @endif
                                                        ></td>
                                                    <td>{{$product->name}}</td>
                                                    <td>{{$product->technical_name}}</td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            @else
                                                @php
                                                    $pro = json_decode($crop->product_id);
                                                @endphp
                                                
                                                @foreach($products as $product)
                                                <tr>
                                                    <td><input type="checkbox" name="product[]" value="{{$product->id}}" 
                                                           @if(isset($pro)) {{in_array($product->id, $pro) ? 'checked' : ''}} @endif
                                                        ></td>
                                                    <td>{{$product->name}}</td>
                                                    <td>{{$product->technical_name}}</td>
                                                </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if(isset($subsubid))
                                <div class="form-group">
                                    <img src="{{ $subsubid['image'] }}" alt="Image not found" class="img-responsive img-thumbnail" height="100" width="100">
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.crops.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            @if(isset($subsubid))
                            <button type="submit" id="submit"  class="btn btn-primary button-color">Update</button>
                            @else
                            <button type="submit" id="submit"  class="btn btn-primary button-color">Add</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    var cropType=document.getElementById('crop').value;
    cropType=cropType.replace(/\s+/, "");
    if(cropType=="Solution") {
        $("#add_crop").validate({
            ignore: [],
            errorClass: 'error',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            rules: {
                name: {
                    required: true,
                    // lettersonly: true,
                    maxlength: 25,
                }
            },
            messages: {
                "name":{
                    required: "Please enter solution name",
                    maxlength: "Please enter max 25 character",
                },
                "image":{
                    required: "Please choose solution image",
                }
            },
            submitHandler: function(form) {
                console.log('enter');
                $('button[type="submit"]').attr('disabled', true);
                form.submit();
            },
        });
    } else {
        $("#add_crop").validate({
            ignore: [],
            errorClass: 'error',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            rules: {
                name: {
                    required: true,
                    // lettersonly: true,
                    maxlength: 25,
                },
                core_category: {
                    required: true,
                },
                core_subcategory: {
                    required: true,
                }
            },
            messages: {
                "name":{
                    required: "Please enter sub-sub solution name",
                    maxlength: "Please enter max 25 character",
                },
                "core_category":{
                    required: "Please select solution",
                },
                "core_subcategory":{
                    required: "Please select sub category",
                }
            },
            submitHandler: function(form) {
                error=0;
                document.getElementById('image_error').style.display="none";
                document.getElementById('image_type_error').style.display="none";
                document.getElementById('image1').style.display="none";
                var logo = $('#image').val();
                var id = $('#id').val();
                
                if(logo=="" && id==""){
                    console.log('e');
                    document.getElementById('image1').style.display="block";
                    error=1;
                } else if(logo!="") {
                    var fi = document.getElementById('image');
                    document.getElementById('image_error').style.display="none";
                    document.getElementById('image_type_error').style.display="none";
                    if (fi.files.length > 0) {
                        for (var i = 0; i <= fi.files.length - 1; i++) {
                            var fsize = fi.files.item(i).size;
                            var type = fi.files.item(i).type;
                            var size = fsize/1024;  
                            if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                                document.getElementById('image_type_error').style.display="block";
                                error=1;
                            } else if(size>1024){
                                document.getElementById('image_error').style.display="block";
                                error=1;
                            }
                        }
                    }
                }
                
                if (error == 1)  {
                    return false;
                }
                $('button[type="submit"]').attr('disabled', true);
                form.submit();
            },
        });
    }
    
    function GetFileSize() {
        var fi = document.getElementById('image');
        document.getElementById('image1').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;  
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }

    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });
    if($("#core_category_selected").val()!=""){
        var id = $("#core_category_selected").val();
        getSub(id);
    }
    $('#core_category').change(function () {
        var id = $("#core_category").val();
        getSub(id);
    });
    function getSub(id){
        var core_subcategory_selected = $("#core_subcategory_selected").val();
        $.ajax({
            url : "{{url('admin/crops/getsubcat/')}}/"+id,
            
            type: 'get',
            success: function (data) {
                console.log(data);
                $('#core_subcategory').empty();
                $('#core_subcategory').append($('<option>', {value:'', text:'Sub-Solution Category'}));
                
                $.each( data, function(key, value) {
                    if(core_subcategory_selected==value){
                        $('#core_subcategory').append('<option value="'+  value +'"  selected="true">' + key + '</option>');
                    } else{
                        $('#core_subcategory').append('<option value="'+ value +'">' + key + '</option>');
                    }
                });
            },
            errors: function (data) {
                console.log(data);
            }
        });
    }
    $("#translate").on('click',function(){
        $("#submit").css('display','block');
        var name = $("#name").val();
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "name": name,"name2":"name2 value" },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
    $('#technical_name').change(function(){
        // $("#product_tbl").remove();
        var t_name = $(this).val();
        // console.log(t_name);
        $.ajax({
            url : "{{route('admin.products.getTechnicalName')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "technical_nm": t_name
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                 var res='';
                $.each (result, function (key, value) {
                    console.log(value.name);
                res +=
                '<tr>'+
                    '<td><input type="checkbox" name="product[]" value="@if(isset($product->id)) {{$product->id}}@endif" @if(isset($product->id))  @if($product->id == $product->id) checked @endif @endif></td>'+
                    '<td>'+value.name+'</td>'+
                    '<td>'+value.technical_name+'</td>'+
                '</tr>';
                });
                $('#product_tbl').html(res);
                
            },
            error: function()
            {
                alert('error...');
            }
        });
    });
    $(document).ready(function() {
        $('#product-sub-sub-solution').DataTable({
             "paging": false,
             "ordering":false,
             "info": false
        });
    } );
</script>
@endsection
