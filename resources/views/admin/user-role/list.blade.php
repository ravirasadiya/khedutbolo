@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        @include('layouts.errors-and-messages')
        <!-- Default box -->
        @if($employees)
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Manage Users</h4>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table border-b1px" id="table-id">
                        <thead>
                            <tr>
                                <th class="col-md-1">ID</th>
                                <th class="col-md-3">Name</th>
                                <th class="col-md-3">Email</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-3">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $i=1 @endphp
                        @foreach ($employees as $employee)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $employee->name }}</td>
                                <td>{{ $employee->email }}</td>
                                <td>@include('layouts.status', ['status' => $employee->status])</td>
                                <td>
                                    <form action="{{ route('admin.users.destroy', $employee->id) }}" method="post" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="post">
                                        <div class="btn-group">
                                            @if($permission->edit==1)
                                                <a href="{{ route('admin.users.edit', $employee->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                            @endif
                                            @if($permission->delete==1)
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button>
                                            @endif
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
            <div class="box-body">
            </div>
        </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No Users found.</p></div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
