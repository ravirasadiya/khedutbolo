@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!--@include('layouts.errors-and-messages')-->
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">@if(isset($user->id)) Edit @else Add @endif  User</h4>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('admin.users.store') }}" method="post" id="create_company" class="form" enctype="multipart/form-data">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="@if(old('name')!=''){{old('name')}}@elseif(isset($user->name)){{$user->name}}@else{{old('name')}}@endif" maxlength="25">
                                    @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name_hi"><span class="text-danger">*</span></label>
                                    <input type="text" name="name_hi" id="name_hi" placeholder="नाम" class="form-control" value="@if(old('name_hi')!=''){{old('name_hi')}}@elseif(isset($user->name_hi)){{$user->name_hi}}@else{{old('name_hi')}}@endif" maxlength="25" tabindex="-1">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name_gu"><span class="text-danger">*</span></label>
                                    <input type="text" name="name_gu" id="name_gu" placeholder="નામ" class="form-control" value="@if(old('name_gu')!=''){{old('name_gu')}}@elseif(isset($user->name_gu)){{$user->name_gu}}@else{{old('name_gu')}}@endif" maxlength="25" tabindex="-1">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="mobile">Mobile <span class="text-danger">*</span></label>
                                    <input type="text" name="mobile" id="mobile" placeholder="Mobile" class="form-control numeric" value="@if(old('mobile')!=''){{old('mobile')}}@elseif(isset($user->mobile)){{$user->mobile}}@else{{old('mobile')}}@endif" maxlength="10">
                                    @if ($errors->has('mobile'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('mobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <div class="form-group">
                                       <!--  <span class="input-group-addon">@</span> -->
                                        <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="@if(old('email')!=''){{old('email')}}@elseif(isset($user->email)){{$user->email}}@else{{old('email')}}@endif">
                                        @if ($errors->has('email'))
                                            <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="password">Password <span class="text-danger">*</span></label>
                                    <input type="password" name="password" id="password" placeholder="xxxxx" class="form-control" maxlength="15">
                                    @if ($errors->has('password'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="role">Role <span class="text-danger">*</span></label>
                                    <select name="role" id="role" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if(old("role")==$role->id) selected="selected" @endif>{{ ucfirst($role->display_name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="status">Status <span class="text-danger">*</span></label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="0" @if(isset($user->status) && $user->status=="0")  selected="selected" @endif>Disable</option>
                                        <option value="1" @if(isset($user->status) && $user->status=="1") selected="selected" @endif>Enable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="logo">Logo<span class="text-danger">*</span></label>
                                    <input type="file" name="logo" id="logo" class="form-control" accept="image/*" onchange="GetFileSize()">
                                    <small class="text-warning">Note :- Select Logo file which have extensions .jpg, .jpeg and .png upto 1 MB only.</small>
                                    <label id="image" class="image_error">Please select logo image</label>
                                    <label id="image_error" class="image_error">Image size is large.</label>
                                    <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <div class="btn-group">
                                <a href="{{ route('admin.users.index') }}" class="btn btn-default">Cancel</a>
                                <input type="button" class="btn btn-info" value="Translate" id="translate">
                                <button type="submit" id="submit" style="display: none;" class="btn btn-primary">@if(isset($user->id)) Edit @else Add @endif</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#create_company").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                // lettersonly: true,
                maxlength: 25,
            },
            mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15,
            },
            role: {
                required: true,
            },
            status : {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name.",
                // lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "mobile":{
                required: "Mobile Number is required.",
                digits: "Enter digits for Mobile Number.",
                minlength: "Enter 10 digits for Mobile Number.",
                maxlength: "Enter 10 digits for Mobile Number.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
            "password":{
                required: "Please enter password.",
                minlength: "Please enter min 8 character.",
                maxlength: "Please enter max 15 character.",
            },
            "role":{
                required: "Please select role.",
            },
            "status":{
                required: "Please select status.",
            }
        },
        submitHandler: function(form) {
            error=0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image').style.display="none";
            var logo = $('#logo').val();
            console.log(logo);
            if(logo==""){
                console.log('e');
                document.getElementById('image').style.display="block";
                error=1;
            } else {
                var fi = document.getElementById('logo');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;   
                        console.log('fsize'+fsize+' type '+type+' size '+size);
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error = 1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error = 1;
                        }
                    }
                } 
            }
            if (error == 1)  {
                return false;
            }
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    function GetFileSize() {
        var fi = document.getElementById('logo');
        document.getElementById('image').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        console.log(fi);
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;   
                console.log('fsize'+fsize+' type '+type+' size '+size);
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    }); 

    $("#translate").on('click',function(){
        $("#submit").css('display','block');
        var name = $("#name").val();
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "name": name,"name2":"name2 value" },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
</script>
@endsection
