@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Edit Company</h4>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('admin.companies.update', $employee->id) }}" id="edit_company" method="post" class="form" enctype="multipart/form-data">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="{!! $employee->name ?: old('name')  !!}" maxlength="25">
                                    <span id="uname_response"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name_hi"> <span class="text-danger">*</span></label>
                                    <input type="text" name="name_hi" id="name_hi" placeholder="नाम" class="form-control" value="{!! $employee->name_hi ?: old('name_hi')  !!}" maxlength="25">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name_gu"> <span class="text-danger">*</span></label>
                                    <input type="text" name="name_gu" id="name_gu" placeholder="નામ" class="form-control" value="{!! $employee->name_gu ?: old('name_gu')  !!}" maxlength="25">
                                </div>
                            </div>
                        </div>

                       <!--  <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="mobile">Mobile <span class="text-danger">*</span></label>
                                    <input type="text" name="mobile" id="mobile" placeholder="Mobile" class="form-control numeric" value="{!! $employee->mobile ?: old('mobile')  !!}" maxlength="10">
                                    @if ($errors->has('mobile'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('mobile') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="{!! $employee->email ?: old('email')  !!}">
                                        @if ($errors->has('email'))
                                            <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="password">Password </label>
                                    <input type="password" name="password" id="password" placeholder="xxxxx" class="form-control" maxlength="15">
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="roles">Role  <span class="text text-danger">*</span></label>
                                    <select name="roles" id="roles" class="form-control select2">
                                        @foreach($roles as $role)
                                            <option @if(in_array($role->id, $selectedIds))selected="selected" @endif value="{{ $role->id }}">{{ ucfirst($role->display_name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div> -->

                         <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    @include('admin.shared.status-select', ['status' => $employee->status])
                                </div>  
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="logo">Logo  <span class="text text-danger">*</span></label>
                                    <input type="file" name="logo" id="logo" class="form-control" accept="image/*" onchange="GetFileSize()">
                                    <small class="text-warning">Note :- Select Logo file which have extensions .jpg, .jpeg and .png upto 1 MB only.</small>
                                    <label id="image_error" class="image_error">Image size is large.</label>
                                    <label id="image" class="image_error">Please select logo image</label>
                                    <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        
                        @if(isset($employee->logo))
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <img style="height:100px !important" src="{{ $employee->logo }}" alt="image not found" class="img-responsive" height="200px"/> <br/>
                                    </div>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        @endif
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.companies.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
     $("#name").keyup(function(){
                  var username = $(this).val().trim();
                     var productid= "{{$employee->id}}";
                  if(username != ''){
            
                     $.ajax({
                        url: "{{route('admin.companies.checkEditCompanyname')}}",
                        type: 'get',
                        data: {name: username,productid:productid},
                        success: function(response){
                            
                            if(response=='true'){
                               $('#uname_response').html("<span style='color: red;'>Company already exist.</span>");
                               $("#name").focus();
                               $('.btn-primary').attr('disabled', true);
                               return false;
                            }
                            else{
                                 $("#uname_response").empty();
                   $('.btn-primary').removeAttr('disabled');
                            }
            
                         }
                     });
                  }else{
                   $("#uname_response").empty();
                   $('.btn-primary').removeAttr('disabled');
                  }

             });
    $("#edit_company").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                // lettersonly: true,
                maxlength: 25,
            }
            // mobile: {
            //     required: true,
            //     digits: true,
            //     minlength: 10,
            //     maxlength: 10,
            // },
            // email: {
            //     required: true,
            //     email: true,
            // },
            // role: {
            //     required: true,
            // }
        },
        messages: {
            "name":{
                required: "Please enter name.",
                // lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "mobile":{
                required: "Please enter mobile.",
                digits : "Please enter number only.",
                minlength: "Please enter min 10 digit.",
                maxlength: "Please enter max 10 digit.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
            "role":{
                required: "Please select role.",
            }
        },
        submitHandler: function(form) {
            error = 0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image').style.display="none";
            var logo = $('#logo').val();
            if(logo!=""){
                var fi = document.getElementById('logo');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;   
                        console.log('fsize'+fsize+' type '+type+' size '+size);
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error = 1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error = 1;
                        }
                    }
                } 
            }
            if (error == 1)  {
                return false;
            }
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    function GetFileSize() {
        var fi = document.getElementById('logo');
        document.getElementById('image').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {

                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;    
                console.log(size);
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });

     $("#translate").on('click',function(){
        $("#submit").css('display','block');
        var name = $("#name").val();
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "name": name,"name2":"name2 value" },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#name_hi").val(jsonObj.name.hi);
                $("#name_gu").val(jsonObj.name.gu);
                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });
</script>
@endsection
