@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Offer</h4>
                    </div>
                </div>
                <form action="{{ route('admin.offers.update', $offer->id) }}" method="post" class="form" enctype="multipart/form-data" id="edit_offer">
                    <div class="box-body">
                        {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="offer">Offer <span class="text-danger">*</span></label>
                                        <input type="text" name="offer" id="offer" placeholder="Offer" class="form-control " value="{!! $offer->offer ?: old('offer')  !!}" maxlength="25">
                                    </div>
                                </div> 
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="offer_hi">प्रस्ताव <span class="text-danger">*</span></label>
                                        <input type="text" name="offer_hi" id="offer_hi" placeholder="प्रस्ताव" class="form-control" value="{!! $offer->offer_hi ?: old('offer_hi')  !!}" maxlength="25">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="offer_gu">ઓફર <span class="text-danger">*</span></label>
                                        <input type="text" name="offer_gu" id="offer_gu" placeholder="ઓફર" class="form-control" value="{!! $offer->offer_gu ?: old('offer_gu')  !!}" maxlength="25">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="offer_type">Offer Type <span class="text-danger">*</span></label>
                                        <select name="offer_type" id="offer_type" class="form-control">
                                            <option value="single_offer" @if($offer->offer_type=="single_offer") selected="selected" @endif>Single Offer</option>
                                            <option value="combo_offer" @if($offer->offer_type=="combo_offer") selected="selected" @endif>Combo Offer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="expired_date">Expired Date<span class="text-danger">*</span></label>
                                        <input type="text" name="expired_date" id="expired_date" class="form-control" value="{!! $offer->expired_date ?: old('expired_date') !!}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="expired_time">Expired Time<span class="text-danger">*</span></label>
                                        <input type="time" name="expired_time" id="expired_time" placeholder="Time" class="form-control" value="{!! $offer->expired_time ?: old('expired_time') !!}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="cover">Image </label>
                                        <input type="file" name="cover" id="cover" class="form-control" accept="image/*" onchange="GetFileSize()">
                                        <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                        <label id="image" class="image_error">Please select offer image</label>
                                        <label id="image_error" class="image_error">Image size is large</label>
                                        <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="status">Status </label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="0" @if($offer->status == 0) selected="selected" @endif>Disable</option>
                                            <option value="1" @if($offer->status == 1) selected="selected" @endif>Enable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        @if(isset($offer->cover))
                                            <div class="form-group" style="margin-top: 10px;">
                                                <img style="height:100px !important" src="{{ $offer->cover }}" alt="" class="img-responsive"> <br/>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="description">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description">{{ $offer->description ? $offer->description : old('description') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="description_hi">विवरण <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="description_hi" id="description_hi" rows="5" placeholder="विवरण">{{ $offer->description_hi ? $offer->description_hi : old('description_hi') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="description_gu">વર્ણન <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="description_gu" id="description_gu" rows="5" placeholder="વર્ણન">{{ $offer->description_gu ? $offer->description_gu : old('description_gu') }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-10 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <table  class="table table-striped" id="offer_table">
                                            <thead>
                                                <th><input type="checkbox" id="allOffer" onClick="offerAll(this)" @if(count($offer_products)>0) @if(count($offer_products) == count($products)) checked @endif @endif></th>
                                                <th>Product</th>
                                                <th>Technical</th>
                                                <th>Category</th>
                                                <th>Company</th>
                                                <th>Unit Key</th>
                                                <th>Price</th>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i=1;
                                                @endphp
                                                @foreach ($products as $product)
                                                    <tr>
                                                        <td>
                                                                <input type="checkbox" class="checkbox product" name="product[]" id="item_product_{{$i}}" value="{{$product->items[0]->id}}" @foreach($offer_products as $offer) @if($product->id==$offer->product_id) checked @endif @endforeach>
                                                        </td>
                                                        <td>{{ $product->name }} </td>
                                                        <td>{{ $product->technical_name }}</td>
                                                        <td>{{ $product->category }}</td>
                                                        <td>{{ $product->company }}</td>
                                                        <td>
                                                            <select name="item_key[]" disabled id="item_key_{{$i}}" class="form-control item_key">
                                                                @foreach($product->items as $item)
                                                                    <option value="{{$item->id}}">{{$item->key}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td><input type="text" placeholder="Price"  name="item_price[]" id="item_price_{{$i++}}" value="{{$product->items[0]->sale_price}}" readonly></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.offers.index') }}" class="btn btn-default">Cancel</a>
                            <input type="button" class="btn btn-info" value="Translate" id="translate">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript">
         $('#offer_table').DataTable();
        $("#edit_offer").validate({
        ignore: [],
        errorClass: 'error text-change',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            offer : {
                required: true,
                maxlength: 25,
            },
            offer_type: {
                required: true,
            },
            expired_date: {
                required: true,
            },
            expired_time: {
                required: true,
            },
            status: {
                required: true,
            },
        },
        messages: {
            "offer":{
                required: "Please enter offer",
                maxlength:"Please enter no more than 25 character",
            },
            "offer_type":{
                required: "Please select offer type.",
            },
            "expired_date":{
                required: "Please enter expired date.",
            },
            "expired_time":{
                required: "Please enter expired time.",
            },
            "status":{
                required: "Please select status",
            },
        },
        submitHandler: function(form) {
            error=0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image').style.display="none";
            var logo = $('#cover').val();
            console.log(logo);
            if(logo!=""){
                var fi = document.getElementById('cover');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;   
                        console.log('fsize'+fsize+' type '+type+' size '+size);
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error = 1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error = 1;
                        }
                    }
                } 
            }
            if (error == 1)  {
                return false;
            }
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
            
    });
    function GetFileSize() {
        var fi = document.getElementById('cover');
        document.getElementById('image').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {

                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024; 
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });
    
    function offerAll(source) {
        checkboxes = document.getElementsByName('product[]');
        for(var i=0; i<checkboxes.length; i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    
    $('.checkbox').change(function() {
        checkboxes = document.getElementsByName('product[]');
        var a=0;
        for(var i=0; i<checkboxes.length ;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==checkboxes.length){
                    document.getElementById('allOffer').checked=true;
                }else {
                    document.getElementById('allOffer').checked=false;
                }
            }
        }
    });
   

    $('.item_key').change(function(event){
        var id = $(this).attr('id');
        var value = $(this).val();
        var res = id.replace("item_key_", "");
        console.log($("#"+"item_product_"+res).val());
        $("#"+"item_product_"+res).val(value);
        // $('.product').val(value);
        console.log(product);
        product(value,res);
    });
    
    function product(value, item_id){
        console.log(item_id);
        $.ajax({
            url : "{{route('admin.offers.product.attr')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": value
                },
            type: 'get',
            // dataType: 'json',
            success: function( result )
            {
                name='item_price_'+item_id;
                $('#'+name).val(result['sale_price']);
                // document.getElementById(name).value=result['sale_price'];
            },
            error: function()
            {
                alert('error...');
            }
        });
    }
    
    $('#expired_date').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:'TRUE',
        autoclose: true,
        startDate: new Date(),
    });


    $("#translate").on('click',function(){
        $("#submit").css('display','block');
        var offer = $("#offer").val();
        var description = $("#description").val();
        $(".box").css({'opacity':'0.5','pointer-events': 'none'});
        $.ajax({
            url : "{{route('admin.translate')}}",
            data: { "offer": offer,"description":description },
            type: 'get',
            dataType: 'json',
            success: function( result )
            {
                var jsonObj = result;
                $("#offer_hi").val(jsonObj.offer.hi);
                $("#offer_gu").val(jsonObj.offer.gu);
                $("#description_hi").val(jsonObj.description.hi);
                $("#description_gu").val(jsonObj.description.gu);

                // console.log(result);
                $(".box").css({'opacity':'1','pointer-events': 'auto'});
            },
            error: function(error)
            {
                console.log(error);
            }
        });
    });

    </script>
@endsection
