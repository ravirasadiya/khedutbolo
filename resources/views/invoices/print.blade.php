<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Invoice</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <style type="text/css">
        table { border-collapse: collapse;}
    </style>
</head>
<body>
    <section class="row">
        <div style="margin-top:3%; text-align: center">
            <img class="logofull" src="{{ 'http://khedutbolo.com/New Project.jpg' }}" width="auto" height="50px">
        </div>
    </section>
    <section class="row">
        @if(count($orders)>0)
            @foreach($orders as $order)
            <div class="col-md-12" style="margin-top: 8px!important; ">
                <p style="margin-bottom: 2px!important;">Order Id: {{$order->reference}}</p>
                <p style="margin-top: 0px!important; margin-bottom: 2px!important;">Customer: {{ ucfirst($order->customer_name) }}</p>
                <p style="margin-top: 0px!important; margin-bottom: 2px!important;">Status : {{ ucfirst($order->status_name) }}</p>
                <p style="margin-top: 0px!important; margin-bottom: 2px!important;">Address : {{ ucfirst($order->address_1) }}</p>
                <!--<p style="margin-top: 0px!important; margin-bottom: 2px!important;">Village : {{ ucfirst($order->village_id) }}</p>-->
                <p style="margin-top: 0px!important; margin-bottom: 2px!important;">City : {{ ucfirst($order->city_name) }}</p>
                <p style="margin-top: 0px!important; margin-bottom: 2px!important;">State : {{ ucfirst($order->state_name) }}</p>
                <p style="margin-top: 0px!important; margin-bottom: 2px!important;">Country : {{ ucfirst($order->country_name) }}</p>
                
                <table class="table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Name</th>
                            <!--<th style="width: 15%; border: 1px solid black; border-collapse: collapse;">Description</th>-->
                            <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">HSN Code</th>
                            <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Quantity</th>
                            <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Price</th>
                            <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Shipping Price</th>
                            <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Discount</th>
                            <th style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $tq=0;
                    @endphp
                    
                    @foreach($order['items'] as $product)
                        @php
                            $tq=$tq+$product->quantity; 
                            $product->product_price = str_replace(",","",$product->product_price);
                            if (strpos($product->product_price, '.') !== false) {
                                $product_price = $product->product_price;
                                $shipping_price = $product->shipping_price;
                               
                                $total = ($product->product_price*$product->quantity)+$product->shipping_price;
                            } else {
                                $product_price = $product->product_price;
                                $shipping_price = $product->shipping_price;
                                $total = ($product->product_price*$product->quantity)+$product->shipping_price;
                            }
                        @endphp
                        <tr>
                            <td style="width: 14%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{ucfirst($product->product_name)}}</td>
                            <td style="width: 14%; border: 1px solid black; border-collapse: collapse; margin-left:10px;">{{$product->hsn_code}}</td>
                            <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: center;">{{$product->quantity}}</td>
                            <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$product_price}}</td>
                            <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$shipping_price}}</td>
                            <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{$product->discount}}</td>
                            <td style="width: 14%; border: 1px solid black; border-collapse: collapse;text-align: right; margin-right:10px;">{{ $total - $product->discount}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endforeach
        @else
            <div class="col-md-12" style="margin-top: 10px!important; text-align:center; ">
                <p style="margin-bottom: 2px!important;">Data not found </p>
            </div>
        @endif

    </section>
</body>
</html>