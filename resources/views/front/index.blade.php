@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection

@section('content')
    @include('layouts.front.home-slider')
    
    <div id="about">
        <div style="text-align: center;padding: 100px 0;">
            <h2 style="font-weight: 800">{{ config('app.name') }}</h2>
            <div class="kb_txt">
                <p>Khedutbolo is India’s foremost AgTech start-up working on the mission of #HelpingFarmersWin by providing a complete range of agri solutions at the fingertips of farmers. khedutbolo’s tech platform provides a combination of agronomy advice coupled with service and agri input products that enable farmers to significantly improve their productivity and income.</p>
            <p>We use an extensive amount of data, technology and agronomy knowledge to give the right solutions (advice+products) to Indian farmers.</p>
            <p>Khedutbolo currently operates in the states of Gujarat, Maharashtra, and Rajasthan and has over 5 Lakh farmers on its digital platform. Farmers in these states can avail agri solutions for their entire crop life-cycle with a simple “missed call” or through its Android app, which is amongst the highest rated farming focused apps in the country.</p>
            <p>Over the next 5 years, we plan to continue to provide incremental value to farmers in terms of wide range of inputs delivered at their doorsteps, world-class agronomy guidance and also access to market linkages and credit.</p>
            </div>
        </div>    
    </div>
    
    <div id="product" style="background: lightblue;">
        <div style="padding: 40px;">
            <div style="margin: 75px 0px;">
                <div style="text-align: center;line-height: 55px;">
                    <div style="font-size: 50px;">Product & Service</div>
                    <div style="font-size: 22px;">Customized Solutions to Improve Farm Productivity</div>
                </div>
                <div class="container" style="margin-top: 80px;">
                <div class="row">
                    <div class="col-sm-6 col-md-3 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-pagelines spc_ico" style="padding: 40px 45px!important;" aria-hidden="true"></i></div>
                                <div class="spc_title">Agronomy Advice</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-cogs spc_ico" aria-hidden="true"></i></div>
                                <div class="spc_title">Technology</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-pie-chart spc_ico" aria-hidden="true"></i></div>
                                <div class="spc_title">Data Analytics</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-cloud spc_ico" aria-hidden="true"></i></div>
                                <div class="spc_title">Agri Inputs</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>

    <section class="service_product_container">
        <div class="spc_lay">
            <div>
                <h1 style="font-weight: 600;color: #449D44">Our value propositions to farmers</h1>
            </div>
            <div class="container" style="margin-top: 80px;">
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-stethoscope spc_ico2" style="padding: 40px 45px" aria-hidden="true"></i></div>
                                <div class="spc_title">EXPERT ADVICE</div>
                                <div>Real time agronomy solutions to farmers to better manage their farms.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-thumbs-o-up  spc_ico2" style="padding: 40px 45px" aria-hidden="true"></i></div>
                                <div class="spc_title">QUALITY</div>
                                <div>Provide authentic and high quality products manufactured by renowned companies.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-user-o spc_ico2" style="padding: 40px 45px" aria-hidden="true"></i></div>
                                <div class="spc_title">CUSTOMER SERVICE</div>
                                <div>Provide timely and efficient customer service when the farmer needs it</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-male spc_ico2" aria-hidden="true"></i></div>
                                <div class="spc_title">CONVENIENCE</div>
                                <div>Zero travel for the farmers with products delivery at their doorstep in villages.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-inr spc_ico2" aria-hidden="true"></i></div>
                                <div class="spc_title">FAIR PRICE</div>
                                <div>Strive to make products available to the farmer at a fair price.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="text-center" style="margin: 20px;">
                            <div>
                                <div><i class="fa fa-mobile spc_ico2" aria-hidden="true"></i></div>
                                <div class="spc_title">EASY ACCESSIBILITY</div>
                                <div>Information available on the AgroStar app or by giving a missed call on our 1800 number</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="khedutbolo_app">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <img src="{{ asset('/images/mobile1.png') }}" style="width: 400px;">
                    </div>    
                </div>
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div style="font-size: 25px;">
                        <span style="color: #449D44;font-weight: 800">{{ config('app.name') }}</span>
                    </div>
                    <div style="margin-top: 20px;">
                        {{ config('app.name') }} App is a one-stop solution for all agri-needs.
Farmers can access personalized, agri focused content, tips and articles focused on helping farmers grow better produce and solve common problems occurring on the farm. They can also browse through a wide variety of quality products like seeds, crop protection, nutrition and farm implements belonging to best agri-brands in the country.
                    </div>
                    <div class="play-btn">
                        <a href="#k"><img src="{{ asset('/images/playbutton.png') }}"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="partnership" id="partnership">
        <div style="font-size: 40px;font-weight: 700;color: #ffffffd9;">
            Partnership
        </div>
        <div style="font-size: 20px;margin-top: 20px;color: #ffffffd9;">
            Do you make  great quality and cutting edge Agri Products?
        </div>
    </section>

    <section class="partnership-dis">
        <div style="width: 65%;margin: auto;">
            <div style="font-size: 30px;font-weight: 600;color: #449d44">We make your products easily available to farmers!</div>
            <div style="font-size: 17px;margin: 20px 0;">If you manufacture or source great quality and innovative agri-inputs like seeds, crop nutrition products, crop protection products and farm implements, then reach out to on</div>
            <div><a href="mailto:khedutbolo@gmail.com">khedutbolo@gmail.com</a></div>
        </div>
    </section>

    <section class="our-team" id="ourteam">
        <div class="our-team_lay">
            <div style="color: #ffffffd9;font-size: 40px;font-weight:700;padding-bottom: 20px;">
                Our Team, Our Culture
            </div>
            <div style="color: #ffffffd9;font-size: 18px;">Be a part of our journey to make a meaningful impact to the indian farmer through technology!</div>
        </div>
    </section>

    <section class="partnership-dis">
        <div style="width: 56%;margin: auto;">
            <div style="font-size: 17px;margin: 20px 0;">
                <div style="font-weight: 700;font-size: 25px;">Make an impact for Indian farmers and join us in #HelpingFarmersWin!<br><br></div>
                <div>500+ passionate and hardworking team members are working tirelessly to make a difference to the lives of farmers.<br><br></div>
                <div>If you believe that the right agronomy advice powered by data and technology can make a positive difference to the life of farmers and your aspirations meet ours, if you are someone who will work as hard as us, if you have an entrepreneur in you and are a risk taker, wait no longer and come join us.<br><br></div>
                <div>We are actively hiring across key functions like technology, data science, agronomy, design, operations, marketing, supply chain operations, finance and customer service.</div>
            </div>
            <div style="margin: 45px 0 35px 0">
                <button class="btn_p">View Open Position</button>
            </div>
            <div style="font-size: 17px;letter-spacing: 1px;"><span>Or reach us at </span><a href="mailto:khedutbolo@gmail.com">khedutbolo@gmail.com</a></div>
        </div>
    </section>

    <section class="khedutbolo_app" id="contact" style="background: #F7F7F7 !important;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <div style="font-size: 25px;"><span style="color: #449d44">Contact</span><span style="color: #808080"> Us</span></div>
                        <div style="font-size: 16px;font-weight: 700;margin-bottom: 20px;">{{ config('app.name') }} Pvt. Ltd.,</div>
                        <div style="font-size: 16px;margin-bottom: 20px;">B-89, Marketing Yard( Bedii), Morbi Road, Rajkot- 360003</div>
                        
                        <div style="font-size: 16px;margin-bottom: 20px;">Email: <a href="mailto:info@khedutbolo.in">khedutbolo@gmail.com</a></div>
                        <div style="display: flex;line-height: 5px;margin-bottom: 50px;">
                            <div class="app_ico"><a href="#x"><i style="color: black;" class="fa fa-facebook" aria-hidden="true"></i></a></div>
                            <div class="app_ico"><a href="#x"><i style="color: black;" class="fa fa-twitter" aria-hidden="true"></i></a></div>
                            <div class="app_ico"><a href="#x"><i style="color: black;" class="fa fa-instagram" aria-hidden="true"></i></a></div>
                            <div class="app_ico"><a href="#x"><i style="color: black;" class="fa fa-linkedin" aria-hidden="true"></i></a></div>
                        </div>
                    </div>    
                </div>
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2012.013812218417!2d70.80062468073065!3d22.351755884921193!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xeb1f416f2e70086a!2sKhedut%20Beej%20Bhandar!5e0!3m2!1sen!2sin!4v1591189530603!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;width: 100%;height: 450px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid" style="background: #F7F7F7;font-family: 'Open Sans', sans-serif;padding-bottom: 100px;">
        <div class="row">
            <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
            </div>
            <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12" style="padding: 20px 0;">
                <h1 style="font-family: 'Open Sans', sans-serif;text-align: center;font-weight: 800"><span style="color: #449d44"> Contact</span><span style="color: #808080;"> Us</span></h1>
            </div>
            <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
            </div>
        </div>
        <div class="row">
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    @if(session()->has('message'))
                        <div class="box no-border">
                            <div class="box-tools">
                                <p class="alert alert-success alert-dismissible">
                                    {{ session()->get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
            </div>
        <form action="{{ route('contact.contactFormSubmit') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label>Full name <span class="text-danger">*</span></label>
                        <input type="text" name="fullname" id="fullname" placeholder="Fullname" class="form-control contact-input contact-placeholder" value="" maxlength="35">
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label>Email <span class="text-danger">*</span></label>
                        <input type="text" name="email" id="email" placeholder="Email" class="form-control contact-input contact-placeholder" value="" maxlength="100">
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label>Message  <span class="text-danger">*</span></label>
                        <textarea class="form-control contact-placeholder contact-input" name="message" id="message" rows="5" placeholder="Message"  maxlength="300"></textarea>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
                <div class="col-sm-6 col-md-6 col-sm-6 col-xs-12">
                    <div style="text-align: center;">
                        <button type="submit" class="btn_p">Submit</button>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-sm-3 col-xs-12">
                </div>
            </div>
        </form>
    </section>

    </div>
   
    <hr />
    @include('mailchimp::mailchimp')
@endsection