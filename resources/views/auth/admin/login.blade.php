<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>
    <link rel="icon" type="image/png" sizes="25x25" href="{{ asset('khedutBolo_icon_1.png') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.min.css') }}">
</head>
<body class="hold-transition skin-purple login-page logo_bgs">
    <div class="login-box">
        <!--<div class="login-logo">-->
        <!--    <a href="{{ url('admin') }}" class="text_white">{{ config('app.name') }}</a>-->
        <!--</div>-->
        <!-- /.login-logo -->
        
        <div class="login-box-body login-margin">
            <div class="login-logo">
                <a href="{{ url('admin') }}" style="color: green;">{{ config('app.name') }}</a>
            </div>
            @include('layouts.errors-and-messages')
            <form action="{{ route('admin.login') }}" method="post" id="login">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <input name="email" type="email" id="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                    <span class="fa fa-envelope form-control-feedback"></span>
                    <label class="text-danger errorValidation" id="email_error">
                        Email is Required
                    </label>
                    <label class="text-danger errorValidation" id="email_valid_error" style="display: none">
                        Entered Email address is invalid
                    </label>
                    @if ($errors->has('email'))
                        <span class="text-danger errorValidation">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input name="password" type="password" id="password" class="form-control" placeholder="Password">
                    <span class="fa fa-lock form-control-feedback"></span>
                    <label class="text-danger errorValidation" id="password_error">
                        Password is Required
                    </label>
                    <label class="text-danger errorValidation" id="password_error1">
                        Please enter no more than 15 and less than 6 character
                    </label>
                    @if ($errors->has('password'))
                        <span class="text-danger errorValidation">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <!--@if(Session::has('error'))-->
                <!--  <div class="text-danger errorValidation invalid-data" id="session">{{ Session::get('error') }}</div>-->
                <!--@endif-->
                <div class="row">
                    <div class="col-xs-8">

                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4 log-button">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" id="login_btn">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <script src="{{ asset('js/admin.min.js') }}"></script>
    <script type="text/javascript">
        $('#login_btn').click(function(){
            var email = document.getElementById('email').value;
            var password = document.getElementById('password').value;
            var n, m;
            document.getElementById('email_error').style.display = "none";
            document.getElementById('password_error').style.display = "none";
            document.getElementById('email_valid_error').style.display = "none";
            document.getElementById('password_error1').style.display = "none";
            if(password!=""){
                var length = password.length;
            }
            if(email=="" && password==""){
                document.getElementById('email_error').style.display = "block";
                document.getElementById('password_error').style.display = "block";
            }
            if(email!="" && password==""){
                n = email.includes("@");
                m = email.includes(".");
                if(n==false || m==false){
                console.log('e');
                    document.getElementById('email_valid_error').style.display = "block";
                }
                document.getElementById('password_error').style.display = "block";
            }
            if(email=="" && password!=""){
                document.getElementById('email_error').style.display = "block";
                if(length>15 || length<6){
                    document.getElementById('password_error1').style.display = "block";
                }
            }
            if(email!="" && password!=""){ 
                n = email.includes("@");
                m = email.includes(".");
                if(n==false || m==false){
                    document.getElementById('email_valid_error').style.display = "block";
                }
                if(length>15 || length<6){
                    document.getElementById('password_error1').style.display = "block";
                }
            }
            if(email=="" && password=="" || email!="" && password=="" || email=="" && password!="" || n==false || m==false || length>15 || length<6 ) {
                event.preventDefault();
            }
        });
    </script>
</body>
</html>