<header class="main-header">
    <!-- Logo -->
    <a href="{{route('admin.dashboard')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="{{ asset('khedutBolo_icon.png') }}" width="40"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg loginIcon"><img src="{{ asset('khedutBolo_logo_40.png') }}" width="40"> {{ config('app.name') }}</span>

    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
               
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ url($user->logo) }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ "$user->name" }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            
                            <img src="{{ url($user->logo) }}" class="img-circle" alt="User Image">

                            <p>
                                {{ $user->name }}
                                <small>Member since {{ date('m Y', strtotime($user->created_at)) }}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                       
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('admin.companies.profile', $user->id) }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('admin.logout') }}"onclick="return confirm('Are you sure, you want to logout?')" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
               
            </ul>
        </div>
    </nav>
</header>