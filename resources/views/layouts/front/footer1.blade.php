  <!-- Site footer -->
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-9">
            <h6>About</h6>
            <p class="text-justify">Khedutbolo is India’s foremost AgTech start-up working on the mission of #HelpingFarmersWin by providing a complete range of agri solutions at the fingertips of farmers. khedutbolo’s tech platform provides a combination of agronomy advice coupled with service and agri input products that enable farmers to significantly improve their productivity and income.

We use an extensive amount of data, technology and agronomy knowledge to give the right solutions (advice+products) to Indian farmers.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="#about">About</a></li>
              <li><a href="#product">Product & Service</a></li>
              <li><a href="#partnership">Partnership</a></li>
              <li><a href="#ourteam">Our Team</a></li>
              <li><a href="#contact">Contact US</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2020 All Rights Reserved by 
         <a href="{{ config('app.url') }}">{{ config('app.name') }}</a>.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
</footer>