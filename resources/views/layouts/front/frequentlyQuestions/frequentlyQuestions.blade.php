<!--Partnerships Start-->
<section class="section" id="Partnership">
<title align="center"> Partnership</title>
<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-strech-content="true" class="vc_row wpb_row" style="position: relative; left:-78px; box-sizing: border-box; width: 596px;">
    ::before
    <div class="wpb_column bdy vc_column_container">
        <div class="vc_column-inner">
            ::before
            <div class="wpb_wrapper">
                <div class "wpb_single_image wpb_content_element vc_align_left">
                    <figure class="vc_single vc_figure">
                        <div class "vc_single_image_wrapper vc_box_border_grey">
                            <img width="1390" height="498" src="{{ asset('front_assets/images/product_supplier4.png') }}" class="vc_single_image-img" alt="">
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Partnership Ends-->

    <!-- ***** Frequently Question Start ***** -->
    <section class="section" id="frequently-question">
        <div class="container">
            <!-- ***** Section Title Start ***** -->
            <!--<div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>What is Lorem Ipsum?</h2>
                    </div>
                </div>
                <div class="offset-lg-3 col-lg-6">
                    <div class="section-heading">
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                    </div>
                </div>
            </div>-->
            <!-- ***** Section Title End ***** -->

            <div class="row">
                <div class="left-text col-lg-6 col-md-6 col-sm-12">
                    <h5>{{ config('app.name') }} Pvt. Ltd.,</h5>
                    <div class="accordion-text">
                        <p>B-89, Marketing Yard( Bedii), Morbi Road, Rajkot- 360003</p>
                        <p>Tel: <a href="tel:7211192222" class="text-275540">7211192222</a></p>
                        <span>Email: <a href="mailto:khedutbolo@gmail.com" class="text-275540">khedutbolo@gmail.com</a><br></span>
                        <a href="#contact-us" class="main-button">Contact Us</a>
                    </div> 
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="accordions is-first-expanded">
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is KhedutBolo App ? </span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>KhedutBolo is android E-commerce app for agriculture items.
                                    <br><br>
                                    </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>How We can order?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>You can go to our App name KhedutBolo and Provide Address, Name, Product Details and Order it.
                                    <br><br>
                                    </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Is there any Charges for Delivery? </span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p> It is depends on your Address and  Pin Code
                                    <br><br>
                                    </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Which Company Products do you supply?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>We provide many companies product like Bayer, Syngenta, FMC, Swal, UPL, PI, Corteva, Indofil, Gharda, NACL,Godrej, Nidhi Seeds, Rasi Seeds And many more also can check in our App
                                    <br><br>
                                    </p>
                                </div>
                            </div>
                        </article>
                        <!--<article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.
                                    <br><br>
                                    From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage. </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>What is Lorem Ipsum?</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                                </div>
                            </div>
                        </article>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Frequently Question End ***** -->