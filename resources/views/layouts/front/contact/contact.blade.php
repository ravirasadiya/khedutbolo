<meta name="cybersiara-verification" content="I4b9IJQj1F082wY6D2W1hPIaB65Vf99u" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://embedcdn.mycybersiara.com/CaptchaFormate/CaptchaResources.js"></script>
    
    
    <script type="text/javascript">
        $(function () {
    			InitCaptcha('I4b9IJQj1F082wY6D2W1hPIaB65Vf99u');
    			 $('.CaptchaSubmit').click(function () {
    				  if (CheckCaptcha() != true) {
    					return false;
    				} else { 
    				// Please write your submit button click event function here //
    				} 
    			});
    		});			
    </script>
    <!-- ***** Contact Us Start ***** -->
    <section class="section" id="contact-us">
        <div class="container-fluid">
            <div class="row">
                <!-- ***** Contact Map Start ***** -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div id="map">
                      <!-- How to change your own map point
                           1. Go to Google Maps
                           2. Click on your location point
                           3. Click "Share" and choose "Embed map" tab
                           4. Copy only URL and paste it within the src="" field below
                    -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2012.013812218417!2d70.80062468073065!3d22.351755884921193!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xeb1f416f2e70086a!2sKhedut%20Beej%20Bhandar!5e0!3m2!1sen!2sin!4v1591189530603!5m2!1sen!2sin" width="100%" height="585px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                   <!--  <iframe src="https://maps.google.com/maps?width=100%25&amp;hl=en&amp;q=B-89,%20Marketing%20Yard,%20Juno%20Morbi%20Rd,%20Bedi,%20Rajkot,%20Gujarat%20360003+(KhedutBolo%20Beej%20Bhandar)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed" width="100%" height="585px" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                    </div>
                </div>
                <!-- ***** Contact Map End ***** -->

                <!-- ***** Contact Form Start ***** -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="contact-form">
                        <form id="contact" action="{{ route('contact.contactFormSubmit') }}" method="post">
                          @csrf
                          <div class="row">
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="firstName" type="text" id="name" placeholder="Full Name" required="" class="contact-field">
                              </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="lastName" type="text" id="LastName" placeholder="Last Name" required="" class="contact-field">
                              </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="email" type="text" id="email" placeholder="E-mail" required="" class="contact-field">
                              </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="mobile" type="text" id="mobile" placeholder="Mobile"  required="" class="contact-field">
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <textarea name="message" rows="6" id="message" placeholder="Your Message" required="" class="contact-field"></textarea>
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                  <div class="SiaraShield"></div>
                                  
                                 </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" id="form-submit" class="main-button CaptchaSubmit">Send It</button>
                              </fieldset>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
                <!-- ***** Contact Form End ***** -->
            </div>
        </div>
    </section>
    <!-- ***** Contact Us End ***** -->