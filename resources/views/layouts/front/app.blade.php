<!DOCTYPE html>
<html lang="en">

  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    @yield('css')
    <meta property="og:url" content="{{ request()->url() }}"/>
    @yield('og')
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="25x25" href="{{ asset('front_assets/images/khedutBolo_icon.png')}}">
    <title>{{ config('app.name') }}</title>
    <title>KhedutBolo</title>
<!--

ART FACTORY

https://templatemo.com/tm-537-art-factory

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/templatemo-art-factory.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/css/owl-carousel.css') }}">

    </head>
    
    <body>
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
     @include('layouts.front.header.header') 
    <!-- ***** Header Area End ***** -->


    <!-- ***** Welcome Area Start ***** -->
     @include('layouts.front.welcome.welcome') 
    <!-- ***** Welcome Area End ***** -->


    <!-- ***** Features Big Item Start ***** -->
     @include('layouts.front.about.about') 
    <!-- ***** Features Big Item End ***** -->

    <!-- ***** Features Small Start ***** -->
    @include('layouts.front.services.services') 
    <!-- ***** Features Small End ***** -->


    <!-- ***** Frequently Question Start ***** -->
    @include('layouts.front.frequentlyQuestions.frequentlyQuestions')
    <!-- ***** Frequently Question End ***** -->
    <!-- ***** Contact Us Start ***** -->
     @include('layouts.front.contact.contact') 
    <!-- ***** Contact Us End ***** -->
    
    <!-- ***** Footer Start ***** -->
   @include('layouts.front.footer') 
    <!-- jQuery -->
    <script src="{{ asset('front_assets/js/jquery-2.1.0.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('front_assets/js/popper.js') }}"></script>
    <script src="{{ asset('front_assets/js/bootstrap.min.js') }}"></script>

    <!-- Plugins -->
    <script src="{{ asset('front_assets/js/owl-carousel.js') }}"></script>
    <script src="{{ asset('front_assets/js/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('front_assets/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('front_assets/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('front_assets/js/imgfix.min.js') }}"></script> 
    
    <!-- Global Init -->
    <script src="{{ asset('front_assets/js/custom.js') }}"></script>
    @yield('js')
  </body>
</html>