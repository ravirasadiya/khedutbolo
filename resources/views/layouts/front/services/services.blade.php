       <!-- ***** Features Small Start ***** -->
    <section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/services_quality.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Quality</h5>
                        <p>Provide You Best Quality                             </p>
                        <a href="#" class="main-button">Read More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/Fst_dlvry.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Fast Delivery </h5>
                        <p>Provide you deliver in few days.                     </p>
                        <a href="#" class="main-button">Discover More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/lw-prc.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Cheap Rate</h5>
                        <p>Cheap Rate in Online Market                          </p>
                        <a href="#" class="main-button">More Detail</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/language.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Languages </h5>
                        <p>We have our app in language Gujarati, Hindi, English  </p>
                        <a href="#" class="main-button">Read More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/best_compan1y.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Best Company </h5>
                        <p>Provide You Best Company Product                    </p>
                        <a href="#" class="main-button">Read More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/pymnt-mthd.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Payment Method</h5>
                        <p>Various payment Method Like Cash on Delivery And Online Payment </p>
                        <a href="#" class="main-button">Detail</a>
                    </div>
                    <!--<div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('front_assets/images/service-icon-01.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">What is Lorem Ipsum?</h5>
                        <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                        <a href="#" class="main-button">Read More</a>
                    </div>-->
                </div>
            </div>
        </div>
    </section>
  <!-- ***** Features Small Ends ***** --> 