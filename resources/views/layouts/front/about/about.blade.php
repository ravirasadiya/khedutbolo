    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="{{ asset('front_assets/images/About_us-1234.jpg') }}" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5>We are Agriculture Product Provide Shop. We have very Hard Working and Enthusiastic Team  to Provide You Best Service.</h5>
                    </div>
                    <div class="left-text">
                        <p>Our goal is to provide you best service in Lower and Cheaper Rate. We provide you best agriculture product like Insecticide, Nutrition, Seeds, Herbicides, Hardware, Combo kit and all other farming related things. <br><br>
                          We have our App on app store there you can visit and order our products. We can provide you all over globe company products. There are company like Bayer, BASF, UPL, Godrej, Avani seeds etc. </p>
                        <!--<a href="#about2" class="main-button">Discover More</a>-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->
    
    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about2">
        <div class="container">
            <div class="row">
                <div class="left-text col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix">
                    <div class="left-heading">
                        <h5>We have our App in App store name KHEDUTBOLO</h5>
                    </div>
                    <p></p>
                    <ul>
                        <li>
                            <img src="{{ asset('front_assets/images/Wht_is_khedut_app.png') }}" alt="">
                            <div class="text">
                                <h6>What is KhedutBolo App ?</h6>
                                <p>KhedutBolo App is a App where you can buy online agriculture Products in Cheaper Price and Best Quality.</p>
                            </div>
                        </li>
                        <li>
                            <img src="{{ asset('front_assets/images/how_it_works.png') }}" alt="">
                            <div class="text">
                                <h6>How it Works ?</h6>
                                <p>You can download our App and fill your details like Address and what Products do you want to buy and we will supply that product to you in minimun time. </p>
                            </div>
                        </li>
                        
                        
                        <a href="https://play.google.com/store/apps/details?id=com.khedutbolo" target="_blank" class="appbtn">
                            <img src="{{ asset('front_assets/images/google.png') }}" alt="">
                        </a>
                        
                        
                        <!--<li>
                            <img src="{{ asset('front_assets/images/about-icon-03.png') }}" alt="">
                            <div class="text">
                                <h6>What is Lorem Ipsum?</h6>
                                <p>From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.</p>
                            </div>
                        </li>-->
                    </ul>
                </div>
                <div class="right-image col-lg-7 col-md-12 col-sm-12 mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="{{ asset('front_assets/images/AppImg.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->