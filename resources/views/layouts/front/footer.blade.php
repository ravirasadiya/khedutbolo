    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12">
                    <p class="copyright">Copyright &copy; 2020 KhedutBolo 
                    </p>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12">
                    <ul class="social">
                        <li><a href="https://www.facebook.com/khedut.bolo"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/khedutbolo?s=09"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCm8QvQ3bVOEppKks-TcFCuQ"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.instagram.com/khedutbolo"><i class="fa fa-youtube"></i></a></li>
                        <!--<li><a href="#"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </footer>