      <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area" id="welcomebg">
        <img  src="{{ asset('front_assets/images/1087107586_preview_324.jpg') }}" class="bg_img" id="welcomebg" alt="">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <h1>KhedutBolo <!--is free <strong>for YOU</strong>--></h1>
                        
                        <a href="#about" class="main-button-slider">About Us</a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <img src="{{ asset('front_assets/images/crtn.png') }}" class="rounded img-fluid d-block mx-auto" alt="First Vector Graphic"><!--front_assets/images/slider-icon.png-->
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->